<?php
/*
Plugin Name: Mailman integration
Description: A list management plugin for Mailman 2
Version: 0.9
Author: Erika "Masala" Nilsson
Author URI: http://www.erikanilsson.eu
License: MIT
*/

if(!class_exists('Mailman_Integration'))
{
	class Mailman_Integration
	{
		private static $ajaxAPI = false;

		/**
		 * Construct the plugin object
		 */
		public function __construct()
		{
			// Initialize Settings
			require_once(sprintf("%s/settings.php", dirname(__FILE__)));
			$Mailman_Integration_Settings = new Mailman_Integration_Settings();
			$plugin = plugin_basename(__FILE__);
			add_filter("plugin_action_links_$plugin", array( $this, 'plugin_settings_link' ));

			add_action('after_setup_theme', function(){
				// FYI, functions.php is loaded just before after_setup_theme, so AjaxAPI exists and can be instantiated.
				self::ajaxAPI();
			});
		}

		private static function ajaxAPI(){
			if( false === self::$ajaxAPI && class_exists('AjaxAPI') ) {
				self::$ajaxAPI = new AjaxAPI();
			}

			return self::$ajaxAPI;
		}

		/**
		 * Activate the plugin
		 */
		public static function activate()
		{
			// Do nothing
		} 

		/**
		 * Deactivate the plugin
		 */
		public static function deactivate()
		{
			// Do nothing
		}

		// Add the settings link to the plugins page
		function plugin_settings_link($links)
		{
			$settings_link = '<a href="admin.php?page=mailman.php">Settings</a>';
			array_unshift($links, $settings_link);
			return $links;
		}


		/**
		 * Helper to fetch and parse the list settings field from DB
		 * @param $public What lists to fetch. Permitted values: 'all', 'public', 'private'.
		 *
		 * Followed by two helpers that fetch private and public lists' settings, respectively.
		 */
		public static function getListSettings($public = 'all'){
			switch ($public) {
				case 'public': 
					return self::getPublicListSettings();
				case 'private': 
					return self::getPrivateListSettings();
				case 'all': 
				default:
					$mmi_public = (array) self::getPublicListSettings();
					$mmi_private = (array) self::getPrivateListSettings();
					return (object) array_merge($mmi_private, $mmi_public);
			}
		}
		private static function getPublicListSettings() {
			$mmi_setting = get_option('mmi_mailing_lists');

			if(null != trim($mmi_setting) && "{}" != trim($mmi_setting))
				$mmi_setting = json_decode($mmi_setting);
			else
				$mmi_setting = (object)array();

			return $mmi_setting;
		}
		private static function getPrivateListSettings() {
			$mmi_setting = get_option('mmi_mailing_lists_nonpublic');

			if(null != trim($mmi_setting)){
				$setting_temp = preg_split("/\R/", $mmi_setting);
				$mmi_setting = array();

				if(is_array($setting_temp)){
					foreach($setting_temp as $s){
						$s = explode('|', $s);
						$mmi_setting[trim($s[1])] = (object)array('name' => trim($s[1]), 'title'=>trim($s[0]), 'password'=>$s[2]);
					}
				}
				else{
					$s = explode('|', $setting_temp);
					$mmi_setting[trim($s[1])] = (object)array('name' => trim($s[1]), 'title'=>trim($s[0]), 'password'=>$s[2]);
				}

				$mmi_setting = (object)$mmi_setting;
			}
			else
				$mmi_setting = (object)array();

			return $mmi_setting;
		}


		/**
		 * Ajax functions
		 */
		public static function ajaxRefreshMailingLists(){
			$ajax = self::ajaxAPI();

			if(!isset($_REQUEST['listurl']))
				$ajax->ReturnError("Parameter missing. Request must contain 'listurl'.");

			require_once('Services/Mailman.php');
			$mailman = new Services_Mailman($_REQUEST['listurl']);
			$lists = $mailman->lists();

			if(isset($lists))
				$ajax->ReturnJSON(true, $lists, count($lists).' mailing lists found');
			else
				$ajax->ReturnError("No mailing lists were fetched or something went wrong.");
		}

		public static function unsubscribeMember($email, $list){
			$mmi_url = get_option('mmi_admin_list_url');
			$mmi_setting = self::getListSettings();
			$list_pw = $mmi_setting->$list->password;

			require_once('Services/Mailman.php');
			$mailman = new Services_Mailman($mmi_url, $list, $list_pw);

			if( ! $mailman->unsubscribe($email) )
				return false;
			else
				return true;
		}

		public static function ajaxUnsubscribe(){
			$ajax = self::ajaxAPI();

			if(!isset($_REQUEST['email']) || !isset($_REQUEST['listname']))
				$ajax->ReturnError("Parameter missing. Request must contain 'email' and 'listname'.");

			if( ! self::unsubscribeMember($_REQUEST['email'], $_REQUEST['listname']) )
				$ajax->ReturnError("An error occurred when trying to unsubscribe the member {$_REQUEST['email']}.");
			else
				$ajax->ReturnJSON(true, "Member {$_REQUEST['email']} unsubscribed from {$_REQUEST['listname']}.");
		}

		public static function ajaxSubscribe(){
			$ajax = self::ajaxAPI();

			if(!isset($_REQUEST['email']) || !isset($_REQUEST['listname']))
				$ajax->ReturnError("Parameter missing. Request must contain 'email' and 'listname'.");

			$mmi_url = get_option('mmi_admin_list_url');
			$mmi_setting = self::getListSettings();
			$list = $_REQUEST['listname'];
			$list_pw = $mmi_setting->$list->password;
			$email = filter_var ($_REQUEST['email'], FILTER_SANITIZE_EMAIL);

			require_once('Services/Mailman.php');
			$mailman = new Services_Mailman($mmi_url, $list, $list_pw);

			if( ! $mailman->subscribe($email) ) // Todo: Option to invite more than one email
				$ajax->ReturnError("An error occurred when trying to subscribe the email {$email}.");
			else
				$ajax->ReturnJSON(true, "{$email} has been subscribed to list {$list}.");
		}

		public static function ajaxSubmitModerationData() {
			$ajax = self::ajaxAPI();

			if(!isset($_REQUEST['listname']) || !isset($_REQUEST['formdata']) || !isset($_REQUEST['formaction']))
				$ajax->ReturnError("Parameter missing. Requires 'listname', 'formdata' and 'formaction'.");

			$mmi_url = get_option('mmi_admin_list_url');
			$mmi_setting = self::getListSettings();
			$list = $_REQUEST['listname'];
			$list_pw = $mmi_setting->$list->password;
			require_once("HTTP/Request2.php");

			// Submit the form data
	        $url = $_REQUEST['formaction'] . '?adminpw=' . $list_pw . '&' . $_REQUEST['formdata'];
	        //$ajax->ReturnJSON(true, $url);

	        $request = new HTTP_Request2();

	        try {
	            $request->setUrl($url);
	            $request->setMethod(HTTP_Request2::METHOD_POST);
	            $html = $request->send()->getBody();
	        } catch (Exception $e) {
	            trigger_error($e->getMessage());
	        }

	        libxml_use_internal_errors(true);
	        $doc = new DOMDocument();
	        $doc->preserveWhiteSpace = false;
	        $doc->loadHTML($html);
	        $xpath = new DOMXPath($doc);
	        $h2 = $xpath->query("/html/body/h2[1]")->item(0)->nodeValue;
	        libxml_clear_errors();

	        if ($h2 == 'Database Updated...')
	        	$ajax->ReturnJSON(true, "Moderation settings saved.");
	        else
	        	$ajax->ReturnError("The moderation settings were not saved and I don't know why. Feel free to try again. Sorry.");
		}

	} // END class Mailman_Integration
} // END if(!class_exists('Mailman_Integration'))

if ( class_exists('Mailman_Integration') && false !== stream_resolve_include_path('Services/Mailman.php') ) {
	// Installation and uninstallation hooks
	register_activation_hook(__FILE__, array('Mailman_Integration', 'activate'));
	register_deactivation_hook(__FILE__, array('Mailman_Integration', 'deactivate'));
	// instantiate the plugin class
	$mailman_integration = new Mailman_Integration();
}
else if (class_exists('Mailman_Integration')) {
	function mmi_admin_notice() {
	    ?>
	    <div class="notice notice-warning is-dismissible">
	        <p><strong>PEAR package Services_Mailman missing!</strong><br>The Mailman integration plugin requires the PEAR package Services_Mailman to be present on the server, but it seems to be missing. Since the plugin can't work without it, it has turned itself off.</p>
	    </div>
	    <?php
	}
	add_action( 'admin_notices', 'mmi_admin_notice' );
}