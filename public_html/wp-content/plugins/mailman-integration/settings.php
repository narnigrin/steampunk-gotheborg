<?php
if(!class_exists('Mailman_Integration_Settings'))
{
	class Mailman_Integration_Settings
	{
		/**
		 * Construct the plugin object
		 */
		public function __construct()
		{
			// register actions
            add_action('admin_init', array(&$this, 'admin_init'));
        	add_action('admin_menu', array(&$this, 'add_menu'));
		} // END public function __construct
		
        /**
         * hook into WP's admin_init action hook
         */
        public function admin_init()
        {
        	$this->setting_fields();
        } // END public function admin_init


        public function setting_fields(){
            // Register all settings
            register_setting('mailman_integration-group', 'mmi_admin_list_url');
            register_setting('mailman_integration-group', 'mmi_mailing_lists');
            register_setting('mailman_integration-group', 'mmi_mailing_lists_nonpublic');

            // Settings sections
            add_settings_section(
                'mmi-list-settings-section', 
                'Mailing list settings', 
                array(&$this, 'settings_section_mmi_list_settings'), 
                'mailman_integration'
            );
            
            // Settings fields - Mailing list settings section
            add_settings_field(
                'mailman_integration-mmi_admin_list_url', 
                'Admin list URL', 
                array(&$this, 'settings_field_input_text'), 
                'mailman_integration', 
                'mmi-list-settings-section',
                array(
                    'field' => 'mmi_admin_list_url',
                    'desc' => '<br><button id="refresh-lists-button" class="button button-primary" type="button">Refresh mailing lists</button>',
                    'placeholder' => 'http://example.com/mailman/admin'
                )
            );

            add_settings_field(
                'mailman_integration-mmi_mailing_lists', 
                'Public mailing lists', 
                array(&$this, 'settings_field_textarea'), 
                'mailman_integration', 
                'mmi-list-settings-section',
                array(
                    'field' => 'mmi_mailing_lists',
                    'desc' => '<div id="mailing-lists-list" style="font-style:normal;"><img src="images/wpspin_light.gif" alt="Loading..."></div>',
                    'style' => 'display:none'
                )
            );

            add_settings_field(
                'mailman_integration-mmi_mailing_lists_nonpublic', 
                'Add non-public mailing lists manually', 
                array(&$this, 'settings_field_textarea'), 
                'mailman_integration', 
                'mmi-list-settings-section',
                array(
                    'field' => 'mmi_mailing_lists_nonpublic',
                    'desc' => 'To handle a mailing list which is not publically listed, please enter it and its password here manually. The format should be </em><strong>title|listname|password</strong><em>, one list per line. The list name is usually its e-mail address with the @-sign replaced by an underscore.',
                    'placeholder' => 'List Title|listname_domain.tld|password',
                )
            );
        } 
        

        public function settings_section_mmi_list_settings()
        {
            echo 'To make this plugin work, you need to enter the URL to the public Mailman list page on the server where your mailing lists reside (will usually be something like http://your-domain.com/mailman/admin), choose which mailing lists you want to manage, and specify their admin passwords. If you can\'t see any selectable mailing lists below, please double-check that your admin URL is correct and hit "Refresh mailing lists".</p><p>Only <strong>publically listed</strong> mailing lists will show up automatically, so either make public any lists you want to manage in Mailman, or add them manually below. Please note that list passwords are stored in <strong>plaintext,</strong> unencrypted.';
        }
        
        
        public function settings_field_input_text($args)
        {
            // Get the field name from the $args array
            $field = $args['field'];
            // Get the value of this setting
            $value = get_option($field);

            $ph = isset($args['placeholder']) ? $args['placeholder'] : '';
            $desc = isset($args['desc']) ? $args['desc'] : '';
            $css = isset($args['style']) ? $args['style'] : '';

            echo '<input type="text" name="'.$field.'" id="'.$field.'" value="'.$value.'" style="width:99%;display:block;'.$css.'" placeholder="'.$ph.'"><em>'.$desc.'</em>';
        }


        public function settings_field_textarea($args)
        {
            // Get the field name from the $args array
            $field = $args['field'];
            // Get the value of this setting
            $value = get_option($field);

            $desc = isset($args['desc']) ? $args['desc'] : '';
            $rows = isset($args['rows']) ? $args['rows'] : 3;
            $ph = isset($args['placeholder']) ? $args['placeholder'] : '';
            $css = isset($args['style']) ? $args['style'] : '';

            echo '<textarea name="'.$field.'" id="'.$field.'" style="width:99%; display:block; '.$css.'" rows="'.$rows.'" placeholder="'.$ph.'">'.$value.'</textarea><em>'.$desc.'</em>';
        }
        
        /**
         * add a menu
         */		
        public function add_menu()
        {
            add_menu_page(
                'Mailman list handling', 
                'Mailing lists', 
                'publish_posts', 
                'mailman.php', 
                array(&$this, 'plugin_settings_page_check_lists'),
                'dashicons-email-alt',
                71
            );

            add_submenu_page(
                'mailman.php',
                'Mailman moderation queue', 
                'Moderation queues', 
                'publish_posts', 
                'mailman_queue', 
                array(&$this, 'plugin_settings_page_check_queue')
            );

            add_submenu_page(
                'mailman.php',
                'Mailman list settings',
                'Settings',
                'manage_options',
                'mailman_settings',
                array(&$this, 'plugin_settings_page_check')
            );
        } 
    
        /**
         * Menu Callbacks
         */		
        public function plugin_settings_page_check()
        {
        	if(!current_user_can('manage_options'))
        	{
        		wp_die(__('You do not have sufficient permissions to access this page.'));
        	}
	
        	// Render the settings template
        	include(sprintf("%s/templates/settings.php", dirname(__FILE__)));
        } 
        public function plugin_settings_page_check_lists()
        {
            if(!current_user_can('publish_posts'))
            {
                wp_die(__('You do not have sufficient permissions to access this page.'));
            }
    
            // Render the settings template
            include(sprintf("%s/templates/settings-lists.php", dirname(__FILE__)));
        }
        public function plugin_settings_page_check_queue()
        {
            if(!current_user_can('publish_posts'))
            {
                wp_die(__('You do not have sufficient permissions to access this page.'));
            }
    
            // Render the settings template
            include(sprintf("%s/templates/settings-queue.php", dirname(__FILE__)));
        }

    } // END class Mailman_Integration_Settings
} // END if(!class_exists('Mailman_Integration_Settings'))