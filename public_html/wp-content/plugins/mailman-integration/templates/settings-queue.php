<div class="wrap">
    <h2>Handle Mailman moderation queues</h2>

    <p>Handle the moderation queues of your mailing lists here. 
    This page shows any pending messages for each of your mailing lists, along with the reason why that message is being held in the queue. 
    Select what action to take for each message and then click "Submit all data for [list name]" to save the settings for that list.</p>

    <h4 style="margin-bottom:0"><a href="javascript:void(0)" class="mmi-toggle-info" style="text-decoration:none">Meaning of the moderation actions... ▼</a></h4>
    <ul style="margin-top:6px; margin-left:1em; display:none" class="mmi-info-body">
    	<li><strong>Defer:</strong> Don't do anything with this message now; deal with it later.
    	<li><strong>Approve:</strong> Allow this message on the list. The message will be sent to the whole mailing list unchanged.
    	<li><strong>Reject:</strong> Don't allow this message on the list. The sender will receive a message with the reason for rejection.
    	<li><strong>Discard:</strong> Like Reject, but the sender will <em>not</em> be told the reason for rejection. The message is simply deleted.
    </ul>
    <br>

    <?php
    	require_once('Services/Mailman.php');

    	class Mailman_With_Moderation extends Services_Mailman {
    		public function getModerationQueue(){
    			$path  = 'db/' . $this->list; // The path becomes .../admindb/...
		        $query = array(
		            'adminpw' => $this->adminPW, // Honest to God I have no idea why PW needs to be in caps, it's not defined in caps
		            'details' => 'all',
		        );

		        $query = http_build_query($query, '', '&');
		        $url   = $this->adminURL . $path . '?' . $query; // Same as above re caps
		        $html  = $this->fetch($url);

		        libxml_use_internal_errors(true);
		        $doc = new DOMDocument();
		        $doc->preserveWhiteSpace = false;
		        $doc->loadHTML($html);
		        $xpath = new DOMXPath($doc);
		        
		        $formaction = $xpath->query("/html/body/form/@action");

		        if($formaction->length > 0){
		        	$formaction = $formaction->item(0)->nodeValue;
			        $csrftoken = $xpath->query("/html/body/form/input[@name='csrf_token']/@value")->item(0)->nodeValue; // Need to use this or we might not be able to submit the form
			        $tables = $xpath->query("/html/body/form/table");
			        $response = array(
			        	'form_action' => $formaction,
			        	'csrf_token' => $csrftoken,
			        	'messages' => array(),
			        );

			        foreach($tables as $table){
		        		$msg = array(
		        			'from'          => $xpath->query('tr[1]/td[2]', $table)->item(0)->nodeValue,
		        			'subject'       => $xpath->query('tr[2]/td[2]', $table)->item(0)->nodeValue,
		        			'reason'        => $xpath->query('tr[3]/td[2]', $table)->item(0)->nodeValue,
		        			'received'      => $xpath->query('tr[4]/td[2]', $table)->item(0)->nodeValue,
		        			'headers'       => $xpath->query('tr[9]/td[2]/textarea', $table)->item(0)->nodeValue,
		        			'excerpt'       => $xpath->query('tr[10]/td[2]/textarea', $table)->item(0)->nodeValue,
		        			'actionsName'   => $xpath->query('tr[5]/td[2]/table/tr[2]/td/center/input/@name', $table)->item(0)->nodeValue
		        		);
		        		$response['messages'][] = $msg;
			        }
			    }

			    $response = array('messages' => array());

		        libxml_clear_errors();
		        return $response;
		    }
    	}

    	$mmi_url = get_option('mmi_admin_list_url');
    	$mailman = new Mailman_With_Moderation($mmi_url);

    	$lists = Mailman_Integration::getListSettings();

    	if ( null != $lists ) : ?>
			<h2 class="nav-tab-wrapper wp-clearfix">
				<?php 
				$i = 0;
				foreach ($lists as $ḱey => $list) {
					$i++;
					$current = $i==1?'nav-tab-active':'';
					echo "<a data-list='{$list->name}' href='#mmi-tab-{$list->name}' class='nav-tab {$current}' title='{$list->title} ({$list->name})'>{$list->title} ({$list->name})</a>";
				}
				?>
			</h2><?php

    		foreach ( $lists as $key => $list) : 
    			$mailman->setList($list->name);
    			$mailman->setAdminPw($list->password);
    			$queue = $mailman->getModerationQueue();
    		?>
    			<form action="<?php echo $queue['form_action'] ?>" method="post" data-list="<?php echo $list->name ?>" id="#mmi-tab-<?php echo $list->name ?>">
    			<input type="hidden" name="csrf_token" value="<?php echo $queue['csrf_token'] ?>">
    			<h3><?php echo count($queue['messages']) ?> messages held for approval</h3>

    			<?php if(count($queue['messages']) == 0) : ?>
    				<p>There are no pending requests for <?php echo $list->name ?>.</p>

    			<?php else: ?>
    			<p>For more extensive options (such as filtering/banning e-mail addresses), please go to <a href="<?php echo $mmi_url . 'db/' . $list->name . '?adminpw=' . $list->password ?>">this page.</a></p>
    			<p><button type="button" class="button button-primary mmi-mod-submit">Submit all data for <?php echo $list->name ?></button></p>

    			<table class="mmi-moderation-table" cellspacing="0" cellpadding="4" style="width:100%">
    				<thead style="text-align:left"><tr>
    					<th style="width:30px; border-bottom:1px solid #ccc">&nbsp;</th>
    					<th style="width:25%; border-bottom:1px solid #ccc">Actions</th>
    					<th style="border-bottom:1px solid #ccc">Message</th></tr></thead>
    				<tbody>

	    			<?php 
	    				$i=1; 
	    				foreach ($queue['messages'] as $message) : 
	    			?>
    				<tr>
    					<th style="border-bottom:1px solid #ccc; border-right:1px solid #ccc; vertical-align:top"><?php echo $i; ?></th>
    					<td style="border-bottom:1px solid #ccc; padding-left:10px; vertical-align:top">
    						<input type="radio" name="<?php echo $message['actionsName'] ?>" value="0" id="actions-<?php echo $list->name . '-' . $message['actionsName'] ?>-0" checked> 
    						<label for="actions-<?php echo $list->name . '-' . $message['actionsName'] ?>-0">Defer</label><br>
    						<input type="radio" name="<?php echo $message['actionsName'] ?>" value="1" id="actions-<?php echo $list->name . '-' . $message['actionsName'] ?>-1"> 
    						<label for="actions-<?php echo $list->name . '-' . $message['actionsName'] ?>-1">Approve</label><br>
    						<input type="radio" name="<?php echo $message['actionsName'] ?>" value="2" id="actions-<?php echo $list->name . '-' . $message['actionsName'] ?>-2"> 
    						<label for="actions-<?php echo $list->name . '-' . $message['actionsName'] ?>-2">Reject</label><br>
    						<input type="radio" name="<?php echo $message['actionsName'] ?>" value="3" id="actions-<?php echo $list->name . '-' . $message['actionsName'] ?>-3"> 
    						<label for="actions-<?php echo $list->name . '-' . $message['actionsName'] ?>-3">Discard</label>
    					</td>
    					<td style="border-bottom:1px solid #ccc; vertical-align:top">
    						<table border="0" cellspacing="1" style="text-align:left">
    							<tr>
    								<th>From:</th>
    								<td><strong><?php echo $message['from'] ?></strong></td>
    							</tr>
    							<tr>
    								<th>Subject:</th>
    								<td><?php echo $message['subject'] ?></td>
    							</tr>
    							<tr>
    								<th>Received:</th>
    								<td><?php echo $message['received'] ?></td>
    							</tr>
    							<tr>
    								<th>Reason:</th>
    								<td><em><?php echo $message['reason'] ?></em></td>
    							</tr>
    						</table>

    						<h4 style="margin-bottom:0"><a href="javascript:void(0)" class="mmi-toggle-headers" style="text-decoration:none">Headers... ▼</a></h4>
    						<pre style="white-space:pre-wrap; margin-top:0; display:none" class="mmi-headers-pre"><?php echo htmlspecialchars($message['headers']) ?></pre>
    						<h4 style="margin-bottom:0"><a href="javascript:void(0)" class="mmi-toggle-excerpt" style="text-decoration:none">Message... ▼</a></h4>
    						<pre style="white-space:pre-wrap; margin-top:0; display:none" class="mmi-excerpt-pre"><?php echo htmlspecialchars($message['excerpt']) ?></pre>

    						<br>
    					</td>
    				</tr>
    			<?php 
    				$i++;
    				endforeach; 
    			?>
    				</tbody>
    				<tfoot style="text-align:left"><tr><th>&nbsp;</th><th>Actions</th><th>Message</th></tr></tfoot>
    			</table>

    			<p><button type="button" class="button button-primary mmi-mod-submit">Submit all data for <?php echo $list->name ?></button></p>

    			<?php endif; ?>
    			</form>
    			<?php
    		endforeach;
    	endif;
    ?>
</div>

<script type="text/javascript">
(function($){
	$(document).ready(function(){

		// Switch tabs
		$('form[data-list]').hide();
		$('form[data-list="' + $('.nav-tab-active').data('list') + '"]').show();
		$('.nav-tab').on('click', function(ev){
			ev.preventDefault();
			$(this).siblings('.nav-tab').removeClass('nav-tab-active');
			$(this).addClass('nav-tab-active');

			$('form[data-list]').hide();
			$('form[data-list="' + $(this).data('list') + '"]').show();
		});

		// Open headers/excerpts
		$('.mmi-toggle-headers').on('click', function(){
			$(this).closest('h4').next('.mmi-headers-pre').slideToggle();
		});
		$('.mmi-toggle-excerpt').on('click', function(){
			$(this).closest('h4').next('.mmi-excerpt-pre').slideToggle();
		});
		$('.mmi-toggle-info').on('click', function(){
			$(this).closest('h4').next('.mmi-info-body').slideToggle();
		});

		// Submit forms
		$('.mmi-mod-submit').on('click', function(){
			var $form = $(this).closest('form');
			var formdata = $form.serialize();
			var formaction = $form.attr('action');
			var list = $form.data('list');

			$.ajax({
				url: window.wptoolkit.ajaxUrl,
				type: 'POST',
				data: {
					action:        'AjaxAPI',
					command_class: 'Mailman_Integration',
					command:       'ajaxSubmitModerationData',
					listname:      list,
					formdata:      formdata,
					formaction:    formaction,
				},
				success: function(response){
					//console.log('ajax success', response);

					if(response.status == true && response.data){
						alert(response.data);
						window.location.reload(true);
					}
					else {
						alert(response.message);
						window.location.reload(true);
					}
				},
				error: function(jqXHR, status, error){
					console.log(jqXHR);
					alert("There was a server error. Feel free to try again.");
				},
			});
		});

	});
})(jQuery);
</script>