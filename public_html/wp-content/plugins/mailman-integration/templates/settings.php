<div class="wrap">
    <h2>Mailman integration</h2>
    <form method="post" action="options.php"> 
        <?php @settings_fields('mailman_integration-group'); ?>
        <?php @do_settings_fields('mailman_integration-group'); ?>
        <?php do_settings_sections('mailman_integration'); ?>

        <?php @submit_button(); ?>
    </form>
</div>

<script type="text/javascript">
(function($){
	$(document).ready(function(){

		// Refresh lists
		function refreshLists(silenterror = false){
			$.ajax({
				url: window.wptoolkit.ajaxUrl,
				type: 'GET',
				data: {
					action:        'AjaxAPI',
					command_class: 'Mailman_Integration',
					command:       'ajaxRefreshMailingLists',
					listurl:       $('#mmi_admin_list_url').val(),
				},
				success: function(response){
					//console.log('ajax success', response);
					var $elt = $('#mailing-lists-list');

					function getCheckboxForList(listname, listtitle){
						var cssid = 'mmi-checkbox-' + listname,
						    $mlfield = $('#mmi_mailing_lists'),
						    lists = $mlfield.val();

						lists = (lists !== '' ? JSON.parse(lists) : {});
						var pw = (listname in lists) ? lists[listname].password : ''

						var string = '<p><label for="' + cssid + '">';
						    string += '<input type="checkbox" name="' + cssid + '" id="' + cssid + '" data-value="' + listname + '" data-title="' + listtitle + '"';
						    string += (listname in lists) ? ' checked' : '';
						    string += '><strong>' + listtitle + '</strong> (' + listname + ') &mdash; <span style="font-size:1.8em; vertical-align:middle">⚿</span>';
						    string += '<input type="text" placeholder="Enter list password" value="' + pw + '"' + ((listname in lists) ? '' : ' disabled') + ' style="vertical-align:middle"></label></p>';

						return string;
					}

					if(response.status == true && response.data && response.data.length){
						var i, l = response.data.length, output = '';
						for(i=0; i<l; i++){
							output += getCheckboxForList(response.data[i].path, response.data[i].name);
						}
						$elt.html(output).css('color', '');
					}
					else {
						if( ! silenterror ){
							$elt.html('There was an error when fetching the mailing lists.<br>Please check your URL and try again.').css('color','red');
						}
						console.log(response.message);
					}
				},
				error: function(jqXHR, status, error){
					if ( ! silenterror ) {
						$('#mailing-lists-list')
							.html('There was an error when fetching the mailing lists:<br>' + status + ' ' + error + '<br>You probably did nothing wrong. Feel free to try again.')
							.css('color','red');
					}
					console.log(status, error);
				},
			});
		}
		refreshLists(true);

		$('#refresh-lists-button').on('click', function(ev){
			ev.preventDefault();
			refreshLists();
		});

		// Check/uncheck list checkboxes
		$('#mailing-lists-list').on('change', 'input[type="checkbox"]', function(ev){
			var $mlfield = $('#mmi_mailing_lists')
				lists = $mlfield.val(),
				thisvalue = $(this).data('value'),
				$pwfield = $(this).siblings('input[type="text"]');

			if(lists.trim() != ''){
	   				lists = JSON.parse(lists);
			}
	   			else {
	   				lists = {};
	   			}

			if( $(this).is(':checked')) {
				lists[thisvalue] = {
					'name' : thisvalue,
					'title' : $(this).data('title'),
					'password' : $pwfield.val(),
				};
				$pwfield.prop('disabled', false);
			}
			else if( ! $(this).is(':checked')){
				delete lists[thisvalue];
				$pwfield.prop('disabled', true);
			}

			lists = JSON.stringify(lists);
			$mlfield.val(lists);
		});

		// Edit a password
		$('#mailing-lists-list').on('change', 'input[type="text"]', function(ev){
			var listname = $(this).siblings('input[type="checkbox"]').data('value'),
				$mlfield = $('#mmi_mailing_lists'),
				lists = $mlfield.val();

			if(lists.trim() != ''){
	   				lists = JSON.parse(lists);
			}
	   			else {
	   				lists = {};
	   			}

	   			if(listname in lists){
	   				lists[listname].password = $(this).val();
	   				lists = JSON.stringify(lists);
	   				$mlfield.val(lists);
	   			}
		});
	});
})(jQuery);
</script>