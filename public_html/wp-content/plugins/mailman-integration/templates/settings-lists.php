<div class="wrap">
    <h2>Handle Mailman list subscribers</h2>
    <p>View the membership rosters and subscribe/unsubscribe members of your mailing lists here.<br>
    Keep in mind that unsubscribing a member can't be undone except by adding their e-mail back on the list.</p>

	<?php
		$mmi_url = get_option('mmi_admin_list_url');
		require_once('Services/Mailman.php');
		//require_once(sprintf("%s/../Services/Mailman.php", dirname(__FILE__)));
		$mailman = new Services_Mailman($mmi_url);

		$lists = Mailman_Integration::getListSettings();

        if( null != $lists ) : ?>
        	<h2 class="nav-tab-wrapper wp-clearfix">
				<?php 
				$i = 0;
				foreach ($lists as $ḱey => $list) {
					$i++;
					$current = $i==1?'nav-tab-active':'';
					echo "<a data-list='{$list->name}' href='#mmi-tab-{$list->name}' class='nav-tab {$current}' title='{$list->title} ({$list->name})'>{$list->title} ({$list->name})</a>";
				}
				?>
			</h2>
        	
        	<?php foreach ($lists as $key => $list) :
	            $listname = $list->name;

	            ?>
			    <form data-list="<?php echo $listname ?>" id="#mmi-tab-<?php echo $listname ?>"> 
			    	<h3 style="margin-bottom:0">Add member/subscribe</h3>
			    	<p style="margin-top:1ex">
			    		<input id="mmi-<?php echo $listname ?>-subscribe" name="mmi-<?php echo $listname ?>-subscribe" type="text" style="width:80%; max-width:400px" placeholder="email@example.com">
			    		<button type="button" class="button button-primary mmi-subscribe-button">Add</button>
			    	</p>

		    		<?php
		    			$mailman->setList($listname);
		    			$mailman->setAdminPw($list->password);
		    			$members = $mailman->members();

		    			if (count($members) == 0) {
		    				?><p style="color:red">Something went wrong when fetching members. Please reload the page to try again.</p><?php
		    			}
		    			else if (count($members[0]) == 0) {
		    				?><p>No members for this list were found.<br>Either the list does not have any members, the admin password given in the <a href="options-general.php?page=mailman_integration">Mailman Integration settings</a> is wrong, or some unknown error occurred.</p><?php
		    			}
		    			else {
		    				echo "<h3 style='margin-bottom:.8ex'>Members of $listname</h3>";
		    				echo "<table id='mmi-{$listname}-members' style='vertical-align:middle'><thead style='text-align:left'><th>E-mail address</th><th>Unsubscribe</th></thead><tbody>";

		    				$m = array_flip(array_flip($members[0]));
		    				foreach ($m as $email){
		    					echo "<tr data-email='$email'><td>$email</td><td style='text-align:center'><button type='button' class='button mmi-unsubscribe'>✘</button></td></tr>";
		    				}

		    				echo "</tbody></table>";
		    			}
		    		?>
			    </form>
	            <?php
	        endforeach;
	    else :
	    	?><p style="color:red">There are no mailing lists to manage. Go to the <a href="options-general.php?page=mailman_integration">Mailman Integration settings page</a> and fill in your list details first.</p><?php
	    endif;
    ?>
</div>

<script type="text/javascript">
(function($){
	$(document).ready(function(){
		// Subscribe
		$('.mmi-subscribe-button').on('click', function(ev){
			var member = $(this).siblings('input[type="text"]').val(),
				list = $(this).closest('form').data('list');

			$.ajax({
				url: window.wptoolkit.ajaxUrl,
				type: 'GET',
				data: {
					action:        'AjaxAPI',
					command_class: 'Mailman_Integration',
					command:       'ajaxSubscribe',
					email:         member,
					listname:      list,
				},
				success: function(response){
					//console.log('ajax success', response);

					if(response.status == true && response.data){
						alert(response.data);
						window.location.reload(true);
					}
					else {
						alert(response.message);
					}
				},
				error: function(jqXHR, status, error){
					//console.log(jqXHR);
					alert("There was a server error when trying to add the e-mail address to the list. Make sure the e-mail isn't already subscribed.");
				},
			});
		});

		// Unsubscribe
		$('.mmi-unsubscribe').on('click', function(){
			var $row = $(this).closest('tr'),
				member = $row.data('email'),
				list = $(this).closest('form').data('list');

			$.ajax({
				url: window.wptoolkit.ajaxUrl,
				type: 'GET',
				data: {
					action:        'AjaxAPI',
					command_class: 'Mailman_Integration',
					command:       'ajaxUnsubscribe',
					email:         member,
					listname:      list,
				},
				success: function(response){
					//console.log('ajax success', response);

					if(response.status == true && response.data){
						alert(response.data);
						$row.remove();
					}
					else {
						alert(response.message);
					}
				},
				error: function(jqXHR, status, error){
					//console.log(jqXHR);
					alert("There was a server error when trying to unsubscribe the e-mail address. Make sure the e-mail is actually subscribed before removing it.");
				},
			});
		});

		// Switch tabs
		$('form[data-list]').hide();
		$('form[data-list="' + $('.nav-tab-active').data('list') + '"]').show();
		$('.nav-tab').on('click', function(ev){
			ev.preventDefault();
			$(this).siblings('.nav-tab').removeClass('nav-tab-active');
			$(this).addClass('nav-tab-active');

			$('form[data-list]').hide();
			$('form[data-list="' + $(this).data('list') + '"]').show();
		});
	});
})(jQuery);
</script>