(function($){
	$(document).ready(function(){
		// ### Adding member rows (not actually saving the new members)
		$('.add-member').click(function(){
			var $newRow = $('#sp-members-table tbody tr').eq(0).clone(),
				$newFields = $newRow.find('input, textarea, select'),
				rowIndex = $('#sp-add-members-table tbody tr').length; // NB old length = new index

			if($newRow.length < 1){
				$newRow = $(window.sp.membersAdminTableRow);
				$newFields = $newRow.find('input, textarea, select');
			}

			// Tidy away old row's MID everywhere, set the right widths and stuff
			$newFields.val('').attr('value', '').each(function(){
				$(this).attr('name', "new[" + rowIndex + "][" + $(this).data('name') + "]");
			});
			$newRow.find('[data-name=privileges]').attr('value', 'member');
			$newRow.attr('id', '').data('member-id', '');
			$newRow.find('td').eq(4).css('width', '18%');
			$newRow.find('td').eq(9).css('width', '4.5em');
			$newRow.css('display', 'table-row').find('.row-edited-check').remove();

			// Append the row to the "adding table"
			$('#sp-add-members-table tbody').append($newRow);
		});


		// ### Deleting members and rows (real deletion)
		$('body').on('click', '.delete-member', function(ev){
			ev.preventDefault();
			var go = window.confirm("Do you really want to delete the member?\nThis action can't be undone.");
			if(go){
				var $row = $(this).closest('tr'),
					tableId = $(this).closest('table').attr('id');

				if ("sp-add-members-table" == tableId && true == go) {
					$row.remove();
				}
				else if(true == go) {
					var mid = $row.data('member-id');
					$.ajax({
						url: window.wptoolkit.ajaxUrl,
						type: 'POST',
						data: {
							action:        'AjaxAPI',
							command_class: 'MembersAjax',
							command:       'ajaxDeleteMember',
							mid:   		   mid,
						},
						success: function(response){
							console.log('ajax success', response);

							if(response.status == true){
								$row.remove();
							}
							else {
								console.log(response);
							}
						},
						error: function(jqXHR, status, error){
							console.log(jqXHR);
						},
					});
				}
			}
		});


		$('#sp-members-table').find('input, textarea, select').on('change', function(){
			$(this).closest('tr').find('.row-edited-check').val('true');
		});
	});
})(jQuery);