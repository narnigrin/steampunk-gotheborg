<?php
/*
Plugin Name: Steampunk Götheborg Members
Description: Plugin for membership handling
Version: 0.9.1
Author: Erika "Masala" Nilsson
Author URI: http://www.erikanilsson.eu
License: MIT
*/

if(!class_exists('Members'))
{
	class Members
	{
		public $table = 'members';
		public static $table_static;
		public $privileges = "'member','committee'"; //,'admin'";
		public static $privileges_static;
		public $Admin;
		public $TemplateTags;

		public $columns = array(
			'mid' => array(
				'en' => 'Member ID',
				'sv' => 'Medlems-ID',
				'tag' => "input type='text'",
				'notnull' => '',
				'sortable' => 'true',
				'caps' => 'read_club_only_posts',
				'edit_caps' => 'list_users',
			),
			'firstname' => array(
				'en' => 'Given name',
				'sv' => 'Förnamn',
				'tag' => "input type='text'",
				'notnull' => true,
				'sortable' => "asc",
				'caps' => 'read_club_only_posts',
				'edit_caps' => 'publish_posts',
			),
			'surname' => array(
				'en' => 'Surname',
				'sv' => 'Efternamn',
				'tag' => "input type='text'",
				'notnull' => true,
				'sortable' => "asc",
				'caps' => 'read_club_only_posts',
				'edit_caps' => 'publish_posts',
			),
			'email' => array(
				'en' => 'E-mail',
				'sv' => 'E-post',
				'tag' => "input type='email'",
				'notnull' => false,
				'sortable' => false,
				'caps' => 'publish_posts',
				'edit_caps' => 'publish_posts',
			),
			'phone' => array(
				'en' => 'Phone',
				'sv' => 'Telefon',
				'tag' => "input type='text'",
				'notnull' => false,
				'sortable' => false,
				'caps' => 'publish_posts',
				'edit_caps' => 'publish_posts',
			),
			'comments' => array(
				'en' => 'Comments',
				'sv' => 'Kommentar',
				'tag' => 'textarea',
				'notnull' => false,
				'sortable' => false,
				'caps' => 'publish_posts',
				'edit_caps' => 'publish_posts',
			),
			'membersince' => array(
				'en' => 'Member since',
				'sv' => 'Blev medlem',
				'tag' => "input type='date'",
				'notnull' => false,
				'sortable' => "asc",
				'caps' => 'read_club_only_posts',
				'edit_caps' => 'publish_posts',
			),
			'paiduntil' => array(
				'en' => 'Paid until',
				'sv' => 'Betald till',
				'tag' => "input type='date'",
				'notnull' => true,
				'sortable' => "asc",
				'caps' => 'publish_posts',
				'edit_caps' => 'publish_posts',
			),
			'privileges' => array(
				'en' => 'Privileges',
				'sv' => 'Användargrupp',
				'tag' => '',
				'notnull' => false,
				'sortable' => 'desc',
				'caps' => 'publish_posts',
				'edit_caps' => 'publish_posts',
			),
			'uid' => array(
				'en' => 'User account',
				'sv' => 'Användarkonto',
				'tag' => '',
				'notnull' => false,
				'sortable' => 'desc',
				'caps' => 'manage_options',
				'edit_caps' => 'list_users',
			),
		);


		public function __construct()
		{
			// Add database prefix to the table name
			global $wpdb;
			$this->table = $wpdb->prefix . $this->table;
			self::$table_static = $this->table;
			self::$privileges_static = $this->privileges;

			// Add a new conditional for "If Menu" plugin, relating to Club Member users
			add_filter('if_menu_conditions', array($this, 'club_member_if'));

			// Scripts and styles
			//add_action('wp_enqueue_scripts', array($this, 'enqueue_scripts'), 9);
			add_action('admin_enqueue_scripts', array($this, 'admin_enqueue_scripts'), 9);

			// User demotion and such. The top two of these should be in a cron job eventually.
			add_action('after_setup_theme', array($this, 'demoteExpiredMembers')); 
			add_action('init', array($this, 'deleteOldMembers')); 
			add_action('init', array($this, 'demotion_message'));

			// Include other class files, initiate etc
			require_once(sprintf("%s/MembersAdmin.class.php", dirname(__FILE__)));
			$this->Admin = new MembersAdmin();

			require_once(sprintf("%s/MembersTemplateTags.class.php", dirname(__FILE__)));
			$this->TemplateTags = new MembersTemplateTags();

			require_once(sprintf("%s/MembersAjax.class.php", dirname(__FILE__)));

			// Custom column on users
			add_filter('manage_users_columns', array($this, 'user_columns')) ;
			add_action('manage_users_custom_column', array($this,'user_columns_content'), 10, 3);

			add_action( 'admin_notices', array($this, 'user_admin_notice') );
		}


		public static function activate()
		{
			// Create new user role and capabilities
			// Role: 'Club member'
			// Capabilities: read_club_only_posts, post_to_club
			if ( !function_exists('get_editable_roles') ) {
				require_once( ABSPATH . '/wp-admin/includes/user.php' );
			}

			$roles = get_editable_roles();
	        foreach ($GLOBALS['wp_roles']->role_objects as $key => $role){
	            if (isset($roles[$key]) && $role->has_cap('edit_posts')){ // Includes everyone but Subscribers
	                $role->add_cap('read_club_only_posts');
	                $role->add_cap('post_to_club');
	            }
	        }

			remove_role('member'); // To avoid confusion
			add_role('member', 'Club member', array(
				'read' => true,
				'read_club_only_posts' => true,
				'post_to_club' => true,
			));


			// Create or update the new db table
			global $wpdb;
			$charset_collate = $wpdb->get_charset_collate();

			$table = self::$table_static;
			$users = $wpdb->users;
			$enum = self::$privileges_static;

			$sql = "CREATE TABLE $table (
				mid mediumint(9) NOT NULL AUTO_INCREMENT,
				firstname varchar(128) NOT NULL,
				surname varchar(128) NOT NULL,
				email varchar(255),
				phone varchar(64),
				comments text,
				membersince date DEFAULT '0000-00-00',
				paiduntil date NOT NULL,
				privileges ENUM($enum),
				uid bigint(20) unsigned,
				PRIMARY KEY  (mid),
				KEY email (email),
				KEY members_fk (uid)
			) $charset_collate;";

			require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
			dbDelta($sql);

			// I want a proper foreign key constraint on the user-to-member relationship. Note that dbDelta can't do this.
			$fk = "ALTER TABLE $table ADD FOREIGN KEY (uid) REFERENCES $users(ID) ON DELETE SET NULL ON UPDATE CASCADE;";
			$wpdb->query($fk);
		} 


		public static function deactivate()
		{
			if ( !function_exists('get_editable_roles') ) {
				require_once( ABSPATH . '/wp-admin/includes/user.php' );
			}

			// Remove special user role and capabilities
			$roles = get_editable_roles();
	        foreach ($GLOBALS['wp_roles']->role_objects as $key => $role){
	            if (isset($roles[$key]) && $role->has_cap('read_club_only_posts')){ 
	                $role->remove_cap('read_club_only_posts');
	            }
	            if (isset($roles[$key]) && $role->has_cap('post_to_club')){ 
	                $role->remove_cap('post_to_club');
	            }
	        }

			remove_role('member');
		}


		// If Menu plugin hook
		public function club_member_if($conditions) {
			$conditions[] = array(
				'name' => 'User is Club Member',
				'condition' => function($item){
					return current_user_can('read_club_only_posts');
				}
			);

			return $conditions;
		}


		// Scripts and styles
		public function admin_enqueue_scripts() {
			wp_register_script('members_admin', plugins_url("js/admin.js", __FILE__), array('jquery'), NULL, FALSE);
		
			$screen = get_current_screen();
			if($screen->id == 'toplevel_page_members'){ 
				wp_enqueue_media();
				wp_enqueue_script('members_admin');
			}
		}


		// Users columns
		public function user_columns($columns) {
			$columns['username'] = 'Username/Registration email';
			$columns['member'] = 'Member';
			return $columns;
		}
		public function user_columns_content($value, $column_name, $userid) {
			if($column_name == 'member'){
				$member = $this->getMemberWhere('uid', $userid);
				if(false !== $member && count($member)){
					return "<a href='" . admin_url('admin.php?page=members.php') . "#member-row-{$member[0]->mid}'>{$member[0]->firstname} {$member[0]->surname}</a>";
				}
			}
		}

		// Callback - prints notice div with information about Users and Members at the top of 
		function user_admin_notice() {
			global $pagenow;
	    	if ( $pagenow == 'users.php' ) {
				?>
				<div class="notice notice-info" style="background-color:#cef; max-width:65em">
					<p>This is the Users admin page. <strong>Users</strong> are accounts that allow humans to log into the site with a password and so on.
					This is different from <strong>Members</strong>, which represent actual members of Steampunk Götheborg and contain their personal data, but don't help anyone to log in. 
					Members and Users are often connected (when a club member registers an account for themselves, their "Member item" gets connected to their new "User item")
					but there are many members which do not have User accounts, and some Users which are not connected to a Member.</p>

					<ul>You should <strong>only</strong> be on this page to:
						<li style="list-style:disc outside; margin:0 0 0 2em">Change someone's password, say if they've lost it
						<li style="list-style:disc outside; margin:0 0 0 2em">Manually edit someones user permissions
						<li style="list-style:disc outside; margin:0 0 0 2em">Manually remove a user account, say for bad behaviour or abuse
						<li style="list-style:disc outside; margin:0 0 0 2em">or otherwise, <em>if you know what you're doing.</em>
					</ul>

					<p>If you want to <strong>help a member get a user account</strong>, you must first add them to the <a href="admin.php?page=members.php">members list</a> 
					and then either tell them to register using their e-mail at the <a href="/skaffa-konto">account registration page</a>, or register them yourself using that page.<br>
					If you wish to <strong>manually connect a User to a Member</strong>, please edit the <em>"User account"</em> field in the <a href="admin.php?page=members.php">members list</a>.</p>
				</div>
				<?php
			}
		}
		


		/**
		 * A small check function for member-only-pages
		 * @param $cap 		Which capability is necessary to view page; default 'read_club_only_posts'
		 * @param $redir 	Where to redirect visitors without the required capability
		 * @author 			Erika "Masala" Nilsson
		 */
		public function restricted_page_check($cap = 'read_club_only_posts', $redir = ''){
			if($redir == '') {
				$pageurl = $_SERVER["SERVER_NAME"] . $_SERVER["REQUEST_URI"];
				$redir = site_url() . '/wp-login.php?redirect_to='.urlencode('http://'.$pageurl);
			}

			if(!is_user_logged_in()){
				header('Location: ' . $redir);
				die('Do respect my Location headers, please.');
			} 
			elseif(!current_user_can($cap)){
				header('Location: ' . $redir);
				die('Do respect my Location headers, please.');
			}
		}

		/**
		 * See http://php.net/manual/en/function.stripslashes.php. 
		 */
		private function stripslashes_deep( $value ) {
			if ( is_array( $value ) && count( $value ) > 0 ) {
				foreach($value as $i => $val){
					$value[$i] = $this->stripslashes_deep( $val );
				}
			}
			elseif ( is_string( $value ) ) {
				$value = stripslashes( $value );
			}
			elseif ( count( (array) $value ) > 0 ) {
				foreach ( $value as $k => $val ){
					$value->$k = $this->stripslashes_deep( $val );
				}
			} 

			return $value;
		}

		/**
		 * Get all rows from db table as an array of objects. Includes expired members etc.
		 * @param $orderby one of mid, firstname, surname, membersince, paiduntil, privileges or uid to order the query by.
		 */
		public function getAll($orderby = 'mid', $inactiveLast = false){
			global $wpdb;
			switch($orderby){
				case 'mid':
				case 'firstname':
				case 'surname':
				case 'membersince':
				case 'paiduntil':
					$orderby .= " ASC";
					break;
				case 'privileges':
				case 'uid':
					$orderby .= " DESC";
					break;
				default:
					$orderby = 'mid ASC';
			}

			if($inactiveLast === true)
				$orderby = "paiduntil >= NOW() DESC, " . $orderby;

			return $this->stripslashes_deep( $wpdb->get_results("SELECT * FROM $this->table ORDER BY $orderby;") );
		}

		/**
		 * Get one member as a row object
		 * @param $mid the member ID.
		 * Then, some wrappers for the same.
		 */
		public function getMember($mid){
			if($mid > 0){
				global $wpdb;
				$mid = (int) $mid;
				return $this->stripslashes_deep( $wpdb->get_row("SELECT * FROM $this->table WHERE mid = $mid;") );
			}
			return false;
		}
		public function getRow($mid){
			return $this->getMember($mid);
		}
		public function getSingle($mid){
			return $this->getMember($mid);
		}

		/**
		 * Get members as row objects, given some field value
		 * @param $whereField the column to check against.
		 * @param $whereValue the column value to check against.
		 */
		public function getMemberWhere($whereField, $whereValue){
			if(isset($this->columns[$whereField])){
				global $wpdb;

				$type = '%s';
				if($whereField == 'mid' || $whereField == 'uid')
					$type = '%d';

				$sql = $wpdb->prepare("SELECT * FROM $this->table WHERE $whereField = $type;", $whereValue);
				if(false !== $wpdb->query($sql))
					return $this->stripslashes_deep( $wpdb->last_result );
				else
					return false;
			}
		}
		public function getMembersWhere($whereField, $whereValue){
			return $this->getMemberWhere($whereField, $whereValue);
		}

		/**
		 * Calculates and returns privilege field for a given member.
		 * If a member has expired (= its paiduntil was yesterday or earlier), its privileges will be set to NULL; if membership is valid, 
		 * member will keep current privileges if not NULL and get privileges 'member' if privileges are currently NULL.
		 * @param $member member row object or member ID
		 * @param $update Set to false to avoid updating database. Default true.
		 * @param $returnDemoted Whether to return whether a member was demoted to non-member, instead of the new member privileges. 
		 * 		  A member is not considered demoted if it was already a non-member. Default false.
		 */
		public function checkPrivileges($member, $update = true, $returnDemoted = false){
			if(is_numeric($member))
				$member = $this->getMember((int)$member);

			if(strtotime($member->paiduntil) < strtotime('today midnight')) {
				if($member->privileges != 'NULL' && $member->privileges != null && true === $update) {
					$this->Admin->update($member->mid, array('privileges' => 'NULL'));
					if($returnDemoted) { return true; }
				}
				return ($returnDemoted ? false : 'NULL');
			}
			else {
				if(($member->privileges == 'NULL'  || $member->privileges == null) && true === $update) {
					$this->Admin->update($member->mid, array('privileges' => 'member'));
					return ($returnDemoted ? false : 'member');
				}
				elseif (($member->privileges == 'NULL' || $member->privileges == null) && false === $update) {
					return ($returnDemoted ? false : 'member');
				}
				return ($returnDemoted ? false : $member->privileges);
			}
		}



		/**
		 * Check all members against when their membership expires.
		 * Expired memberships are 'inactivated', if they have user accounts they are demoted to Subscribers.
		 * Uses checkPrivileges() which uses update() which LEAVES ADMIN USERS ALONE. (Holy shit we don't want to accidentally demote all admins.)
		 */
		public function demoteExpiredMembers() {
			$start = microtime(true);
			$allmembers = $this->getAll();
			$demoted_pretty = array();

			foreach($allmembers as $member){
				if(true == $this->checkPrivileges($member, true, true)){
					if(isset($member->uid) && !empty($member->uid))
						update_user_meta($member->uid, 'sp_newlydemoted', 'true');

					$demoted_pretty[] = "$member->firstname $member->surname";

					// Unsub member from all member-related mailing lists. Perhaps make a setting for which these should be?? (TODO?)
					// Note that people NOT on these lists will trigger Services_Mailman_Exceptions. Catching those and doing nothing to make PHP shut up. :)
					if(isset($member->email) && !empty($member->email) && class_exists('Mailman_Integration')){
						$mmi_setting = Mailman_Integration::getListSettings();
						if(isset($mmi_setting->{'medlemmar_steampunkgbg.se'})){
							try{ 
								Mailman_Integration::unsubscribeMember($member->email, 'medlemmar_steampunkgbg.se');
							}catch(Services_Mailman_Exception $e){ 
								//.
							}
						}
						if(isset($mmi_setting->{'styrelsen_steampunkgbg.se'})){
							try{
								Mailman_Integration::unsubscribeMember($member->email, 'styrelsen_steampunkgbg.se');
							}catch(Services_Mailman_Exception $e){ 
								//.
							}
						}
						
					}
				}
			}

			// Informs committee of demoted members
			if(count($demoted_pretty) > 0){
				$committee_email = get_option('sp_committee_email');
				if($committee_email && filter_var($committee_email, FILTER_VALIDATE_EMAIL) !== false){
					$subject = "[steampunkgbg.se] Medlemskap som nyligen gått ut";
					$message = "När en automatisk kontroll av medlemsregistret gjordes på steampunkgbg.se, visade det sig att några medlemskap nyligen har gått ut. Medlemmarna har blivit markerade som \"inaktiva\" i registret, och om ca ett år kommer deras uppgifter automatiskt att raderas om de inte blir medlemmar igen innan dess.\n\nFöljande medlemmar har just inaktiverats:\n" . implode(" \n", $demoted_pretty) . "\n\nOm någon av dessa nyligen har betalat sin medlemsavgift, gå snarast in och markera detta i medlemsregistret på steampunkgbg.se - och passa på och tipsa de som inte har betalat att det är dags nu!";
					wp_mail($committee_email, $subject, $message);
					
				}
			}

			$end = microtime(true);
			//echo ('<span style="color:white">' . 1000*($end - $start) . '</span>');
		}

		/**
		 * Delete completely any Members whose membership expired at least one year ago
		 * This does NOT delete their user accounts, but demoteExpiredMembers() above should always be run first
		 * and that should demote them to subscribers.
		 */
		public function deleteOldMembers() {
			$start = microtime(true);
			global $wpdb;

			$q = "DELETE FROM $this->table WHERE paiduntil < curdate() - interval 1 year;";
			$wpdb->query($q);

			$end = microtime(true);
			//echo ('<span style="color:white">' . 1000*($end - $start) . '</span>'); 
		}

		// Shows a demotion message and unsets the demotion flag, unless the current user is an administrator in which case it is ignored.
		public function demotion_message(){
			$u = wp_get_current_user();
			if($u->ID !== 0 && @get_user_meta($u->ID, 'sp_newlydemoted', true) == 'true' && !in_array('administrator', $u->roles)){
				add_action('wp_head', function(){
					?><script type="text/javascript">
						jQuery(document).ready(function(){
							alert("DITT MEDLEMSKAP HAR GÅTT UT.\nDitt konto har blivit degraderat till icke-medlem eftersom ditt medlemskap, enligt våra register, har gått ut.\nOm du nyligen har betalt din medlemsavgift eller detta verkar konstigt av någon annan anledning, hör av dig till styrelsen och fråga vad som har hänt.\n\nDetta meddelande kommer inte att visas igen.");
						});
					</script><?php
				});
				update_user_meta($u->ID, 'sp_newlydemoted', 'false');
			}
		}


	} // END class Members
} // END if(!class_exists('Members'))


if ( class_exists('Members') ) {
	// Installation and uninstallation hooks
	register_activation_hook(__FILE__, array('Members', 'activate'));
	register_deactivation_hook(__FILE__, array('Members', 'deactivate'));
	
	// instantiate the plugin class
	$Members = new Members();

	// Wrap the class method 
	function restricted_page_check($cap = 'read_club_only_posts', $redir = ''){
		global $Members;
		$Members->restricted_page_check($cap, $redir);
	}
}