<?php
/**
 * CLASS MembersAdmin
 * Contains administrative functions, such as updating and deleting members, and initializes admin pages and such.
 * Normally only Editors (= club committee) and upwards should be able to use any of these functions.
 */
if(!class_exists('MembersAdmin'))
{
	class MembersAdmin
	{
        public $adminCapability = 'publish_posts'; // Who can use the admin functions

		/**
		 * Construct the plugin object
		 */
		public function __construct()
		{
			// register actions
        	add_action('admin_menu', array(&$this, 'add_menu'));
		}
        
        /**
         * Add the menu
         */		
        public function add_menu()
        {
        	add_menu_page(
        	    'Members', 
        	    'Members', 
        	    $this->adminCapability, 
        	    'members.php', 
        	    array(&$this, 'members_admin_page_check'),
                'dashicons-groups',
                69
        	);
        } 
    
        /**
         * Menu Callback
         */		
        public function members_admin_page_check()
        {
        	if(!current_user_can($this->adminCapability))
        	{
        		wp_die(__('You do not have sufficient permissions to access this page.'));
        	}
	
        	// Render the settings template
        	include(sprintf("%s/templates/admin-page.php", dirname(__FILE__)));
        }


        /**
         * Takes a member ID as argument and returns the result of $wpdb->delete.
         * When a member is deleted and that member has a user which is not an Administrator, that user is demoted to Subscriber.
         */
        public function delete($mid){
            if(current_user_can($this->adminCapability)) {
                global $wpdb, $Members;
                $mid = (int)$mid;
                $member = $Members->getMember($mid);

                if(isset($member->uid) && !empty($member->uid)){
                    $user = get_users(array('include' => $member->uid, 'role__not_in' => 'administrator'));
                    if(count($user)){
                        wp_update_user(array(
                            'ID' => $user[0]->ID, 
                            'role' => 'subscriber'
                        ));
                    }
                }

                return $wpdb->delete($Members->table, array('mid' => $mid));
            }
            return false;
        }

        /**
         * Takes a member ID and an array of fields and values as arguments and returns the result of $wpdb->update.
         * The array is passed straight on to $wpdb->update.
         *
         * When a member which has a user which is not an Administrator has its privileges updated, that user is promoted/demoted accordingly.
         * When a member which has a user has its e-mail updated, the user's e-mail is updated also.
         */
        public function update($mid, $values){

            // CASE 1: An admin is updating some member
            if(current_user_can($this->adminCapability) && is_array($values)){
                global $wpdb, $Members;
                $mid = (int)$mid;
                $uid = null;

                if(isset($values['uid']) && $values['uid'] == 0){
                    $values['uid'] = NULL;
                }
                else if(isset($values['uid'])){
                    $uid = (int) $values['uid'];
                }

                // Logic to change user capabilities if member privileges are changed
                if(isset($values['privileges']) && !empty($values['privileges'])){
                    if($uid == null)  {
                        $uid = $Members->getSingle($mid)->uid;
                    }

                    if($uid != null && $uid != 'NULL'){
                        $user = get_users(array('include' => $uid, 'role__not_in' => 'administrator'));

                        if(count($user)){
                            switch($values['privileges']){
                                case 'member':
                                    wp_update_user(array(
                                        'ID' => $user[0]->ID, 
                                        'role' => 'member'
                                    ));
                                    break;
                                case 'committee':
                                    wp_update_user(array(
                                        'ID' => $user[0]->ID, 
                                        'role' => 'editor'
                                    ));
                                    break;
                                default:
                                    wp_update_user(array(
                                        'ID' => $user[0]->ID, 
                                        'role' => 'subscriber'
                                    ));
                            }
                        }
                    }
                }

                // Logic to change emails
                if(isset($values['email']) && !filter_var($values['email'], FILTER_VALIDATE_EMAIL)){
                    unset($values['email']);
                }

                if(isset($values['email'])) {
                    if($uid == null)  {
                        $uid = $Members->getSingle($mid)->uid;
                    }

                    if($uid != null && $uid != 'NULL'){
                        wp_update_user(array(
                            'ID' => $uid,
                            'user_email' => $values['email']
                        ));
                    }
                }

                // Update the actual member
                return $wpdb->update($Members->table, $values, array('mid' => $mid));
            }

            // CASE 2: A member is updating themself
            elseif (is_array($values)) {
                $mid = (int)$mid;

                $u = get_current_user();
                if($mid === (int) $u->ID){
                    global $wpdb, $Members;

                    // Unset values that a non-administrative member should not be able to change
                    unset($values['uid']);
                    unset($values['privileges']);
                    unset($values['membersince']);
                    unset($values['paiduntil']);
                    unset($values['comments']);

                    // Logic to change emails
                    if(isset($values['email']) && !filter_var($values['email'], FILTER_VALIDATE_EMAIL)){
                        unset($values['email']);
                    }

                    if(isset($values['email'])) {
                        if($uid == null)  {
                            $uid = $Members->getSingle($mid)->uid;
                        }

                        wp_update_user(array(
                            'ID' => $u->ID,
                            'user_email' => $values['email']
                        ));
                    }

                    // Update the actual member
                    return $wpdb->update($Members->table, $values, array('mid' => $mid));
                }
                return false;
            }

            return false;
        }

        /**
         * Takes an array of fields and values as argument and returns the result of $wpdb->insert.
         * The array is passed straight on to $wpdb->insert.
         * Note that this does not allow members to have a user account specified at time of creation.
         */
        public function create($values){
            if(current_user_can($this->adminCapability)){
                global $wpdb, $Members;

                $cols = $Members->columns;
                unset($cols['mid']);
                unset($cols['uid']);

                foreach ($cols as $key => $col){
                    if( true == $col['notnull'] && !isset($values[$key]) ){ // A required value is missing
                        return false; 
                    }
                }

                $sameEmailMembers = $Members->getMembersWhere('email', $values['email']);
                if(isset($values['email']) && $sameEmailMembers != false) // NB we catch both errors and 0-row results with a loose comparison
                    $values['uid'] = $sameEmailMembers[0]->uid;

                if(isset($values['mid']))
                    unset($values['mid']);

                if($wpdb->insert($Members->table, $values))
                    return $wpdb->insert_id;
            }
            return false;
        }



        /**
         * @param $member A member object, like one gotten from a database SELECT
         * @param $exclude Array of column names to exclude from the listing
         * @param $users_list Array of standard wp User objects. Will speed things up if the 'uid' field is to be shown, but the function will work either way.
         * @return String HTML markup for the row
         */
        public function tableRow($member = false, $exclude = array(), $users_list = array()){
            if(current_user_can($this->adminCapability)){
                global $Members;
                $columns = $Members->columns;
                $privs = explode(",", str_replace("'", "", $Members->privileges));
                $memberNames = array_map(function(){ return ''; }, $columns);

                $row = "<tr style='vertical-align:top'";

                if($member != null){
                    $row .= " data-member-id='$member->mid' id='member-row-$member->mid'"; // Data attribute on TR
                    $row .= "><input type='hidden' name='member[$member->mid][edited]' class='row-edited-check' value='false'";

                    foreach($member as $key => $field){
                        $memberNames[$key] = "name='member[$member->mid][$key]'";
                    }
                }

                $row .= '>'; // Ends the TR opening tag

                // Exclude columns
                if(is_array($exclude) && count($exclude)){
                    foreach($exclude as $key){
                        unset($columns[$key]);
                    }
                }

                // Get list of users for uid select
                // FYI this yields one get_users call for EACH ROW which isn't great.
                // Whenever possible, send along a $users_list array to avoid this!
                $users = array();
                if(isset($columns['uid']) && !is_array($users_list) && count($users_list) < 1){
                    $userquery = array('fields' => array('ID', 'user_nicename'));
                    if(!current_user_can('manage_options'))
                       $userquery['role__not_in'] = 'administrator';
                    $users = get_users($userquery);
                }
                else {
                    $users = $users_list;
                }

                foreach($columns as $key => $column){
                    if(current_user_can($column['caps'])){
                        $row .= "<td>";
                        $required = ($column['notnull'] == true ? 'required' : '');

                        switch($key){
                            case 'privileges':
                                $row .= "<select $required data-name='privileges' style='max-width:100%;' $memberNames[$key]><option value='NULL'>none</option>";
                                foreach ($privs as $p) { 
                                    $row .= "<option value='$p'";

                                    if($member != null && $member->privileges == $p)
                                        $row .= " selected";

                                    $row .= ">$p</option>"; 
                                }
                                $row .= '</select>';
                                break;

                            case 'uid':
                                $row .= "<select $required data-name='uid' style='max-width:100%;' $memberNames[$key]><option value='NULL'> </option>";
                                foreach ($users as $u) { 
                                    $row .= "<option value='$u->ID'";

                                    if($member != null && $member->uid == $u->ID)
                                        $row .= " selected";

                                    $row .= ">$u->user_nicename</option>"; 
                                }
                                $row .= '</select>';
                                break;

                            default:
                                $closingtag = ($column['tag'] == 'textarea' ? '</textarea>' : '');
                                $tagEnd = '>';

                                if ($column['tag'] == 'textarea' && $member != null)
                                    $tagEnd .= $member->$key; 
                                elseif ($member != null)
                                    $tagEnd = "value='{$member->$key}'" . $tagEnd;

                                $row .= "<{$column['tag']} $required data-name='$key' style='width:100%' {$memberNames[$key]} {$tagEnd}{$closingtag}";
                        }

                        $row .= "</td>";
                    }
                }
                
                $row .= "<td><button class='delete-member button'>Delete</button></td></tr>";
                return $row;
            }
        }

    } // END class MembersAdmin
} // END if(!class_exists('MembersAdmin'))