<?php
/**
 * CLASS MembersTemplateTags
 */
if (!class_exists('MembersTemplateTags')) {
	class MembersTemplateTags {
		private $visibleFields;
		private $rowfunc;

		public function __construct()
		{
			// define_visible_fields() uses current_user_can(), which isn't available when this file loads.
			// It's available on after_setup_theme, though. 
			add_action('after_setup_theme', array($this, 'define_visible_fields'));
		}


		/**
		 * Figures out which function to use and what fields to show based on user capabilities.
		 */
		public function define_visible_fields() {
			// Editors and up:
			if(current_user_can('publish_posts')){
				$this->visibleFields = array(
					'name' => array('firstname', 'surname'),
					'mail' => 'email',
					'phone' => 'phone',
					'comments' => 'comments',
					'since' => 'membersince',
					'paid' => 'paiduntil',
					'group' => 'privileges',
				);

				$this->rowfunc = array($this, 'editor_memberRow');

				// Admins:
				if(current_user_can('list_users')){
					$this->visibleFields['admin'] = false;
					//$visibleFields['pw'] = 'false';
					$this->visibleFields['showall'] = false;
					// On purpose these don't have meta fields.

					$this->rowfunc = array($this, 'admin_memberRow');
				}
			}
			// Club members:
			elseif(current_user_can('read_club_only_posts')) {
				$this->visibleFields = array(
					'name' => array('firstname', 'surname'),
					//'phone' => 'phone',
					'mail' => 'email',
					'since' => 'membersince',
				);

				$this->rowfunc = array($this, 'memberRow');
			}
		}


		/**
		 * This function takes a list of meta fields to show and a reference to a member
		 * and generates an array of "table cell contents" from it. 
		 * Used as a helper in the memberRow functions.
		 * 
		 * @param $fields 	Array
		 * @param $member 	Member ID or row object
		 * @param $editable Whether or not the data should allow "magic field" editability
		 *
		 * @return 			Array of strings to write in table cells
		 */
		public function processMetaForMemberRows($fields, $member, $editable = false){
			global $Members;
			$output = array();

			if(is_numeric($member)){
				$member = (int) $member;
				$member = $Members->getSingle($member);
			}

			foreach($fields as $name => $key){
				$output[$name] = '';

				if(is_array($key)){
					$temp = array();
					foreach ($key as $k){
						$text = $member->$k;
						$classes = "";

						if($editable)
							$classes .= 'editable ';
						else
							$classes .= 'non-editable ';

						if(trim($text) == ''){
							$classes .= 'empty ';
							$text = ' ';
						}

						$temp[] = '<div class="'.$classes.'" data-field-id="'.$k.'">'.$text.'</div>';
					}
					
					$output[$name] = implode(' ', $temp);
				}
				elseif($key !== false) {
					$text = $member->$key;
					$classes = "";

					if($editable)
						$classes .= 'editable ';
					else
						$classes .= 'non-editable ';
					
					if(trim($text) == ''){
						$classes .= 'empty ';
						$text = ' ';
					}

					$output[$name] = '<div class="'.$classes.'" data-field-id="'.$key.'">'.$text.'</div>';
				}
			}

			return $output;
		}

		/**
		 * Three functions to generate the data to print in the table.
		 * This is so we won't have to check capabilities for every
		 * single iteration of the foreach loop later.
		 * 
		 * @param $member 	Member row object or ID
		 * @param $fields 	Array of metadata fields to output
		 *
		 * @return 			Array of "output fields"
		 */
		public function memberRow($member, $fields){
			return $this->processMetaForMemberRows($fields, $member);
		}

		public function editor_memberRow($member, $fields){
			$output = $this->processMetaForMemberRows($fields, $member, true);

			global $Members;
			$privNames = array('NULL' => 'Inaktiv', 'member' => 'Medlem', 'committee' => 'Styrelse');
			$privs = explode(",", str_replace("'", "", $Members->privileges));

			foreach($privs as $priv){
				if(!isset($privNames[$priv]))
					$privNames[$priv] = ucfirst($priv);
			}

			$privTitle = (!empty($member->privileges) ? $privNames[$member->privileges] : 'Inaktiv');
			$output['group'] = "<div class='magic-select' data-options-json='" . json_encode($privNames) . "' data-field-id='privileges' data-value='$member->privileges'>$privTitle</div>";

			return $output;
		}

		public function admin_memberRow($member, $fields){
			$output = $this->editor_memberRow($member, $fields);
			$output['admin'] = '';

			// If member has user account, show an admin checkbox.
			if(isset($member->uid) && !empty($member->uid)){

				$ifuser = get_users(array(
					'include' => array($member->uid)
				));

				if($ifuser){
					if(in_array('administrator', $ifuser[0]->roles))
						$checked = 'checked';
					else
						$checked = '';

					$output['admin'] = "<input type='checkbox' class='magic-admin-checkbox' data-field-id='admin' $checked style='vertical-align:top'>";
				}
			}

			//$output['pw'] = '<a href="javascript:void(0)" class="symb js-reset-pw" title="Återställ lösenord">&#128273;</a>';
			$output['showall'] = '<a href="javascript:void(0)" class="symb js-member-data-popup" title="Visa alla fält">&#128269;</a>';

			return $output;
		}

		/**
		 * Wrapper function to generate the row data 
		 * using the relevant one of the memberRow functions
		 */
		public function generateMemberRow($member){
			return call_user_func($this->rowfunc, $member, $this->visibleFields);
		}

		/**
		 * Function to actually make a table row. (Returns, doesn't itself print)
		 * @param $member 	member row object
		 * @param $rowData 	Array of row data (like those from the memberRow functions)
		 *
		 * @return 			String/table row to be printed.
		 */
		public function printMemberRow($member, $rowData = null){
			if(is_numeric($member)){
				global $Members;
				$member = (int)$member;
				$member = $Members->getSingle($member);
			}
			if($rowData == null){
				$rowData = $this->generateMemberRow($member);
			}

			$rowclass = '';
			$paiduntil = $member->paiduntil;
			if($paiduntil == '' || strtotime($paiduntil) < strtotime('today midnight'))
				$rowclass = 'not-paid';

			$print = '<tr id="member-row-' . $member->mid . '" data-member-id="' . $member->mid . '" class="' . $rowclass . '">';
			$print .= '<th scope="row">' . $member->mid . '</th>';

			foreach ($rowData as $name => $content){
				$print .= '<td class="field-' . $name . '">';
				$print .= $content;
				$print .= '</td>';
			}

			$print .= '</tr>';
			return $print;
		}

		// Formats thead and tfoot content depending on fields visible
		public function printTableHeaders(){
			global $Members;

			$fieldNames = array(
				'name' => 'Namn',
				'phone' => 'Telefon',
				'mail' => 'E-post',
				'since' => 'Blev medl.',
				'paid' => 'Betald tom.',
				'comments' => 'Kommentarer',
				'pw' => ' ',
				'group' => 'Användargrupp',
				'admin' => 'Admin',
				'showall' => ' ',
			);

			echo '<tr>';
			echo '<th scope="col" style="width:2em" class="field-nr">#</th>';

			foreach ($this->visibleFields as $field => $metakey){
				if(array_key_exists($field, $fieldNames))
					echo '<th scope="col" class="field-' . $field . '">' . $fieldNames[$field] . '</th>';
			}

			echo '</tr>';
		}

	} // END class
} // END if(!class_exists)