<?php
/**
 * CLASS MembersAjax
 * Contains ajax functions; all static so that the WPToolkit Ajax API can "reach" them.
 */
if(!class_exists('MembersAjax'))
{
	abstract class MembersAjax
	{
		private static $ajaxAPI = false;

		private static function ajaxAPI(){
			if( false === self::$ajaxAPI && class_exists('AjaxAPI') ) {
				self::$ajaxAPI = new AjaxAPI();
			}

			return self::$ajaxAPI;
		}



		public static function ajaxUpdateRowClass($field, $value){
			if($field == 'paiduntil'){
				if($value == '' || strtotime($value) < strtotime('today midnight'))
					return 'not-paid';
				else
					return 'paid';
			}
			else
				return false;
		}

		public static function ajaxMemberSaveField(){
			$AjaxAPI = self::ajaxAPI();
			global $Members;

			$cols = $Members->columns;
			unset($cols['mid']);
			unset($cols['uid']);

			// Check all required fields are present
			if(isset($_REQUEST['old_value']) && isset($_REQUEST['field']) && isset($_REQUEST['value']) && isset($_REQUEST['memberID'])){
				if($_REQUEST['old_value'] == $_REQUEST['value']){
					$AjaxAPI->ReturnJSON(true, 'OK; inga ändringar gjordes');
				}
				// Check the field exists and that user can edit that field
				elseif(isset($cols[$_REQUEST['field']]) && current_user_can($cols[$_REQUEST['field']]['edit_caps'])) {
					$valueAr = array(
						$_REQUEST['field'] => $_REQUEST['value']
					);

					// Make sure to check/change user level if paiduntil is changed, and to send new privileges value along
					$newlevel = '';
					if($_REQUEST['field'] == 'paiduntil'){
						$member = $Members->getSingle((int)$_REQUEST['memberID']);
						$member->paiduntil = $_REQUEST['value'];
						$newlevel = $Members->checkPrivileges($member, false);
						
						if($newlevel != $member->privileges)
							$valueAr['privileges'] = $newlevel;

						// Stop users from changing their own privileges (thereby accidentally demoting themselves etc)
						if($member->uid != null && $member->uid == get_current_user_id())
							$AjaxAPI->ReturnError('Datumet du angav skulle medföra att dina användarprivilegier ändras, och man kan inte ändra sina egna privilegier. Någon annan måste göra den här ändringen.');

					}
					elseif ($_REQUEST['field'] == 'privileges'){
						$member = $Members->getSingle((int)$_REQUEST['memberID']);

						// Stop users from changing their own privileges (thereby accidentally demoting themselves etc)
						if($member->uid != null && $member->uid == get_current_user_id())
							$AjaxAPI->ReturnError('Du kan inte ändra dina egna privilegier. Någon annan måste göra den här ändringen.');
					}

					if ( false !== $Members->Admin->update((int)$_REQUEST['memberID'], $valueAr)){
						$rowclass = self::ajaxUpdateRowClass($_REQUEST['field'], $_REQUEST['value']);
						$AjaxAPI->ReturnJSON(true, array('Fält sparat med ny data', 'rowclass'=>$rowclass, 'newlevel'=>$newlevel));
					}
					else {
						$AjaxAPI->ReturnError('Wordpress lyckades inte spara värdet i databasen.');
					}
				}
				else {
					$AjaxAPI->ReturnError('Du har inte tillåtelse att redigera det här fältet.');
				}
			}
			else{ // This is obviously shite, we don't have any data
				$AjaxAPI->ReturnError('Data som krävs för att spara fältet saknas.');
			}
		}

		public static function ajaxSetAdmin(){
			$AjaxAPI = self::ajaxAPI();
			global $Members;

			if(isset($_REQUEST['mid'])){
				if(current_user_can('list_users')) { // Only admins can do this
					$member = $Members->getMember((int)$_REQUEST['mid']);

					if(!isset($_REQUEST['admin']) || $_REQUEST['admin'] === true || $_REQUEST['admin'] === 'true')
						$role = 'administrator';
					else {
						switch($member->privileges){
							case 'committee':
								$role = 'editor';
								break;
							case 'member':
								$role = 'member';
								break;
							default:
								$role = 'subscriber';
						}
					}

					if($member->uid == get_current_user_id() && $admin == false){
						$AjaxAPI->ReturnError('Du kan inte ta bort administratörsrättigheter från dig själv.');
					}
					elseif(empty($member->uid) || $member->uid == 'NULL') {
						$AjaxAPI->ReturnError('Kunde inte hitta användare.');
					}
					else {
						$success = wp_update_user(array(
                            'ID' => $member->uid, 
                            'role' => $role
                        ));

                        if(is_wp_error($success))
                        	$AjaxAPI->ReturnError('Wordpress lyckades inte spara värdet i databasen.');
                        else
                        	$AjaxAPI->ReturnJSON(true, $member->firstname.' '.$member->surname.' har nu rollen '.$role.'.');
					}
				}
				else {
					$AjaxAPI->ReturnError('Du har inte tillräckliga användarrättigheter för att redigera det här fältet.');
				}
			}
			else {
				$AjaxAPI->ReturnError('Data som krävs för att spara fältet saknas.');
			}
		}

		public static function ajaxDeleteMember(){
			$AjaxAPI = self::ajaxAPI();
			global $Members;

			if(isset($_REQUEST['mid']) && current_user_can($Members->Admin->adminCapability)) {
				if(false !== $Members->Admin->delete((int)$_REQUEST['mid']))
					$AjaxAPI->ReturnJSON(true, "Member deleted");
				else 
					$AjaxAPI->ReturnError("Database error");
			}
		}

		public static function ajaxNewMember(){
			$AjaxAPI = self::ajaxAPI();
			global $Members;

			$cols = $Members->columns;
			unset($cols['mid']);
			unset($cols['uid']);

			// Reformat
			$valuesArray = array();
			foreach ($_REQUEST['fields'] as $field){
				if(isset($cols[$field['name']]))
					$valuesArray[$field['name']] = $field['value'];
			}

			$reqdPresent = true;
			foreach ($cols as $key => $col){
				if( true == $col['notnull'] && !isset($valuesArray[$key]) ){
					$reqdPresent = false;
					break;
				}
			}

			if(!$reqdPresent){
				$AjaxAPI->ReturnError('Ett eller flera obligatoriska fält saknas.');
			}
			elseif (!current_user_can($Members->Admin->adminCapability)){
				$AjaxAPI->ReturnError('Du har inte tillräckliga användarrättigheter för att lägga till medlemmar.');
			}
			else {
				// If privileges are not set and paiduntil indicates this should have member privileges, add them.
				if(!isset($valuesArray['privileges']) && strtotime($valuesArray['paiduntil']) >= strtotime('today midnight'))
					$valuesArray['privileges'] = 'member';

				$mid = $Members->Admin->create($valuesArray);
				if(false !== $mid){
					$tablerow = $Members->TemplateTags->printMemberRow($mid);
					$AjaxAPI->ReturnJSON(true, array('Medlemmen har lagts till.', 'row'=>$tablerow, 'mid'=>$mid));
				}
				else {
					$AjaxAPI->ReturnError('Wordpress kunde inte spara till databasen.');
				}
			}
		}

		public static function ajaxUserSignup(){
			$AjaxAPI = self::ajaxAPI();
			global $Members;

			if(!isset($_REQUEST['email']) || !filter_var($_REQUEST['email'], FILTER_VALIDATE_EMAIL))
				$AjaxAPI->ReturnError('<p>Du måste fylla i en riktig e-postadress.</p>');
			elseif(username_exists($_REQUEST['email']) || email_exists($_REQUEST['email'])){
				$AjaxAPI->ReturnError('<p style="margin-bottom:0.2em">En användare med den e-postadressen finns redan. Vill du få hjälp att återställa ditt lösenord för det här användarkontot?</p><p><button class="button turq" id="pwreset">Ja tack, återställ</button> &nbsp; <button class="button red" id="resetform">Nej tack, jag försöker igen</button></p>');
			}
			else {
				$matching = $Members->getMemberWhere('email', $_REQUEST['email']);

				// Stop non-members from registering
				if(!isset($matching) || count($matching) < 1){
					$AjaxAPI->ReturnError('<p>E-postadressen du angav finns inte i medlemsregistret. Är du medlem i föreningen? <a href="mailto:info@steampunkgbg.se">Hör med styrelsen</a> ifall du är inlagd i registret!</p>');
				}

				// Filter $matching for expired members
				$m2 = $matching;
				foreach ($m2 as $index => $match){
					if($Members->checkPrivileges($match) == 'NULL'){
						unset($matching[$index]);
					}
				}
				unset($m2);

				switch(count($matching)){
					case 1:
						$pw = wp_generate_password();
						$fname = $matching[0]->firstname;
						$lname = $matching[0]->surname;
						$role = 'member';

						if('committee' == $Members->checkPrivileges($matching[0]))
							$role = 'editor';

						$user = wp_insert_user(array(
							'user_login' => $_REQUEST['email'],
							'user_email' => $_REQUEST['email'],
							'user_pass' => $pw,
							'first_name' => $fname,
							'last_name' => $lname,
							'display_name' => $fname . ' ' . $lname,
							'role' => $role,
						));

						if(is_wp_error($user))
							$AjaxAPI->ReturnError('<p>Något gick fel när Wordpress skulle skapa användaren. (Försök igen om du vill.)</p>');
						else {
							$Members->Admin->update($matching[0]->mid, array('uid' => $user));
							wp_new_user_notification($user, $pw);
							$AjaxAPI->ReturnJSON(true, 'Användaren har skapats.');
						}
						break;

					case 0:
					case -1: // Stop expired members from registering
						$AjaxAPI->ReturnError('<p>E-postadressen du angav tillhör inte någon aktiv medlem. Är du medlem i föreningen? <a href="mailto:info@steampunkgbg.se">Påminn styrelsen</a> om att föra in din info i medlemsregistret.</p>');
						break;

					default:
						if(isset($_REQUEST['memberID'])){
							// Pick out the selected member from the collection of matches
							$single = null;
							foreach ($matching as $match){
								if($match->mid == $_REQUEST['memberID']){
									$single = $match;
									break;
								}
							}

							// Someone is messing with us
							if($single == null){
								$AjaxAPI->ReturnError('<p>Medlemsnamnet matchar inte e-postadressen &ndash; försöker du köra med oss? ;)</p>');
								return;
							}

							$pw = wp_generate_password();
							$fname = $single->firstname;
							$lname = $single->surname;

							$role = 'member';
							if('committee' == $Members->checkPrivileges($single))
								$role = 'editor';

							$user = wp_insert_user(array(
								'user_login' => $_REQUEST['email'],
								'user_email' => $_REQUEST['email'],
								'user_pass' => $pw,
								'role' => $role,
								'first_name' => $fname,
								'last_name' => $lname,
								'display_name' => $fname . ' ' . $lname,
							));

							if(is_wp_error($user)) {
								$AjaxAPI->ReturnError('<p>Något gick fel när Wordpress skulle skapa användaren. (Försök igen om du vill.)</p>');
							}
							else {
								$Members->Admin->update($single->mid, array('uid' => $user));
								wp_new_user_notification($user, $pw);
								$AjaxAPI->ReturnJSON(true, 'Användaren har skapats.');
							}
						}
						else {
							$formstart = '<form id="register-step-2"><select id="suggestions" name="suggestions">';
							$formend = '</select><input type="submit" class="button turq" value="Slutför"></form>';

							foreach($matching as $member){
								$temparray[] = '<option value="' . $member->mid . '">' . $member->firstname . ' ' . $member->surname . '</option>';
							}
							$form = $formstart . implode($temparray) . $formend;

							$AjaxAPI->ReturnJSON(true, array('info' => '<p style="margin-bottom:0.2em">E-postadressen matchar flera medlemmar, så du behöver välja vilken av dem kontot ska tillhöra. <br><label for="suggestions">Vad heter du?</label></p>' . $form));
						}
				}
			}
		}


	}
}