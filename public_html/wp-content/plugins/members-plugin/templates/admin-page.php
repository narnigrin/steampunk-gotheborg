<?php
	global $wpdb, $Members;
	$table = $Members->table;

	// Save POST data
	if(isset($_POST['submit'])){
		$news = array(); 

		if(isset($_POST['member']) && count($_POST['member']) > 0){
			foreach ($_POST['member'] as $index => $m){
				if($m['edited'] == true){
					unset($m['edited']);
					$Members->Admin->update($index, $m);
				}
			}
		}
		if(isset($_POST['new']) && count($_POST['new']) > 0){
			foreach ($_POST['new'] as $n){
				$Members->Admin->create($n);
			}
		}
	}

	// Main queries and variables
	$orderby = 'surname';
	if(isset($_GET['orderby'])){
		switch($_GET['orderby']){
			case 'firstname':
			case 'surname':
			case 'membersince':
			case 'paiduntil':
			case 'privileges':
			case 'uid':
				$orderby = $_GET['orderby'];
				break;
		}
	}
	$membersArray = $Members->getAll($orderby);
	$url = admin_url('admin.php?page=members.php');

	// Getting a users array here instead of at each repetition of tableRow() further down
	$userquery = array('fields' => array('ID', 'user_nicename'));
    if(!current_user_can('manage_options'))
       $userquery['role__not_in'] = 'administrator';
    $user_array = get_users($userquery);


	// Download CSV functionality
	function toCSV($oar,$nameskeys) {
		$str=implode(',',array_values($nameskeys))."\n";
		$keys=array_keys($nameskeys);
		$max=count($nameskeys);
		foreach($oar as $row) {
			$str.=$row->{$keys[0]};
			for($i=1;$i<$max;$i++) {
				$str.=','.$row->{$keys[$i]};
			}
			$str.="\n";
		}
		return $str;
	}

	function SP_CSV($oar) {
		global $Members;
		$cols = $Members->columns;
		unset($cols['uid']);
		foreach($cols as $key => $col){
			$cols[$key] = $col['sv'];
		}
		return(toCSV($oar,$cols));
	}

	$csv=SP_CSV( $membersArray );
	$filepath = wp_upload_dir(null, false)['basedir'] . "/members.csv";

	$opts = array(
		'http'=>array(
			'header'=>"Content-Type: text/csv; charset=UTF-8; header=present\r\n".
					  "Content-Length: ".strlen($csv)."\r\n"
		)
	);
	$context = stream_context_create($opts);
	$file = file_put_contents($filepath, $csv, 0, $context);
?>
<div class="wrap">
	<h2>Members</h2>

	<h3 style="margin-bottom:.5ex">There are <?php echo count($membersArray) ?> people in the members table, including unpaid/expired members.</h3>
	<p style="margin-top:0"><a href="<?php echo wp_upload_dir(null, false)['baseurl'] . "/members.csv"; ?>" download="SteampunkGbg-Members.csv"><span class="dashicons dashicons-download" style="text-decoration:none!important;"></span>Download table as CSV</a></p>

	<p><strong>Comments</strong> are only visible to committee and admins. <br>
	<strong>Privileges</strong> have a practical effect only when the member gets an associated user account, and is otherwise for "book-keeping". <br>
	When you change the privileges for a member who has a user account, the user account's permissions also change.</p>

	<?php if(current_user_can('list_users')) : ?>
		<h3>Special info for administrators:</h3>
		<p>To make a user's permissions different from their member privileges, go to the <a href="users.php">Users admin screen</a> and edit the user's Role directly.<br>
		Administrator users will <strong>not</strong> lose their permissions if you change the member here; you must add/remove admin privileges separately.</p>
	<?php endif; ?>
	
	<form method="post">
	<?php submit_button("Save all changes"); ?>

	<table class="wp-list-table widefat fixed striped" id="sp-members-table">
		<thead>
			<tr>
				<th class="manage-column sortable asc" scope="col"><a href="<?php echo "$url&orderby=firstname" ?>"><span>Given name</span> <span class="sorting-indicator"></span></a></th>
				<th class="manage-column sortable asc" scope="col"><a href="<?php echo "$url&orderby=surname" ?>"><span>Surname</span> <span class="sorting-indicator"></span></a></th>
				<th class="manage-column" scope="col">E-mail</th>
				<th class="manage-column" scope="col">Phone</th>
				<th class="manage-column" scope="col" style="width:18%">Comments</th>
				<th class="manage-column sortable asc" scope="col"><a href="<?php echo "$url&orderby=membersince" ?>"><span>Member since</span> <span class="sorting-indicator"></span></a></th>
				<th class="manage-column sortable asc" scope="col"><a href="<?php echo "$url&orderby=paiduntil" ?>"><span>Paid until</span> <span class="sorting-indicator"></span></a></th>
				<th class="manage-column sortable desc" scope="col"><a href="<?php echo "$url&orderby=privileges" ?>"><span>Privileges</span> <span class="sorting-indicator"></span></a></th>
				<?php if(current_user_can($Members->columns['uid']['caps'])) : ?>
					<th class="manage-column sortable desc" scope="col"><a href="<?php echo "$url&orderby=uid" ?>"><span>User account</span> <span class="sorting-indicator"></span></a></th>
				<?php endif; ?>
				<td style="width:4.5em"></td>
			</tr>
		</thead>
		<tbody>
		<?php 
			foreach($membersArray as $m) {
				echo $Members->Admin->tableRow($m, array('mid'), $user_array); 
			} 
		?>
		</tbody>
	</table>

	<table class="wp-list-table widefat fixed striped" id="sp-add-members-table"><tbody></tbody></table>
	<p><a href="javascript:void(0)" class="add-member"><span class="dashicons dashicons-plus-alt" style="text-decoration:none!important;"></span> Add new member</a></p>

	<?php submit_button("Save all changes"); ?>
	</form>
</div>

<script type="text/javascript">
if(typeof window.sp === 'undefined')
	window.sp = {};

window.sp.membersAdminTableRow = "<?php echo $Members->Admin->tableRow(false, array('mid')); ?>";
</script>