<?php
/*
Plugin Name: Steampunk Gallery
Description: A gallery improver for Steampunk Götheborg
Version: 1.0
Author: Erika "Masala" Nilsson
Author URI: http://www.erikanilsson.eu
License: MIT
*/

if(!class_exists('Steampunk_Gallery'))
{
	class Steampunk_Gallery
	{

		public $gallery_type = 'grid';

		/**
		 * Construct the plugin object
		 */
		public function __construct()
		{
			// Add fields to the gallery screen.
			add_action('print_media_templates', array($this, 'add_gallery_fields'));

			// Filter the gallery output
			add_filter('shortcode_atts_gallery', array($this, 'filter_gallery_atts'), 10, 3 );
			add_filter('post_gallery', array($this, 'filter_post_gallery'), 99, 2);
			add_filter('gallery_style', array($this, 'filter_gallery_tag'), 99, 1);

			add_action('after_setup_theme', array($this, 'gallery_shortcode_override'));

		}

		// Add gallery fields
		// Uses http://wordpress.stackexchange.com/questions/182821/add-custom-fields-to-wp-native-gallery-settings
		// and https://wordpress.org/support/topic/how-to-add-fields-to-gallery-settings/
		public function add_gallery_fields(){
			// Todo: Hide the link to field if possible
			?>
				<script type="text/html" id="tmpl-steampunk-gallery-type">
					<label class="setting">
						<span><?php _e('Gallery type'); ?></span>
						<select data-setting="sg_gallery_type">
							<option value="grid"> Grid (Wordpress default) </option>
							<option value="slideshow"> Slideshow </option>
						</select>
					</label>
				</script>

				<script>

					jQuery(document).ready(function(){
						_.extend(wp.media.gallery.defaults, {
							sg_gallery_type: 'grid',
						});

						wp.media.view.Settings.Gallery = wp.media.view.Settings.Gallery.extend({
							template: function(view){
								return wp.media.template('gallery-settings')(view)
								+ wp.media.template('steampunk-gallery-type')(view);
							}
						});

					});

				</script>
			<?php
		}

		// Filters to tweak/overwrite the gallery output
		public function filter_gallery_atts( $output, $pairs, $atts ) {
			switch($this->gallery_type){
				case 'slideshow':
					$output['size'] = 'large'; 
					break;
				case 'grid':
				default:
					$output['size'] = 'thumbnail';
			}

			$output['link'] = 'file';
			return $output;
		}

		public function filter_post_gallery($string, $attr){
			if(isset($attr['sg_gallery_type'])){
				$this->gallery_type = $attr['sg_gallery_type'];
			}
			else {
				$this->gallery_type = 'grid'; // Reset just in case
			}

			return ''; 
		}

		public function filter_gallery_tag($markup){
			// Add data attributes and a class .gallery-sg-[type]
			$markup = str_replace('<div ', '<div data-sg-gallery-type="' . $this->gallery_type . '" ', $markup);
			$markup = str_replace(" class='", " class='gallery-sg-" . $this->gallery_type . " ", $markup);

			return $markup;
		}

		public function gallery_shortcode_override(){
			add_shortcode('gallery', array($this, 'gallery_shortcode_function'));
		}

		// Overriding the gallery shortcode function without actually rewriting all of it
		// The point of this: Filter the image captions to parse URLs correctly. Stupidly, there isn't a nice way to do this.
		// From http://wordpress.stackexchange.com/a/74675
		public function gallery_shortcode_function($attr){
			// Default value in WordPress
			$html5 = current_theme_supports( 'html5', 'gallery' );
			$captiontag = $html5 ? 'figcaption' : 'dd';

			// User value
			isset ( $attr['captiontag'] ) && $captiontag = $attr['captiontag'];

			// Let WordPress create its default gallery ...
			$gallery = gallery_shortcode( $attr );

			// ... and filter the caption
			$gallery = preg_replace_callback(
				'~(<' . $captiontag . '.*>)(.*)(</' . $captiontag . '>)~mUus',
				function($matches){
					return $matches[1] . $this->filter_parse_urls($matches[2]) . $matches[3];
				},
				$gallery
			);

			return $gallery;
		}

		// Receives the caption contents from the shortcode function above, and filters captions for URLs, turning them to links.
		// Uses a regex pattern from https://css-tricks.com/snippets/php/find-urls-in-text-make-links/
		// TODO: This could be used in other filters, like the_content. Food for thought.
		private function filter_parse_urls($text){
			$regex = "~(?:http|https|ftp|ftps)\://[a-zA-Z0-9\-\.]+\.[a-zA-Z]{2,3}(?:/\S*)?~";
			$result = preg_replace($regex, "<a href='$0'>$0</a> ", $text);
			return $result;
		}

	} // END class Steampunk_Gallery
} // END if(!class_exists('Steampunk_Gallery'))
if(class_exists('Steampunk_Gallery'))
{
	// Installation and uninstallation hooks
	register_activation_hook(__FILE__, array('Steampunk_Gallery', 'activate'));
	register_deactivation_hook(__FILE__, array('Steampunk_Gallery', 'deactivate'));
	// instantiate the plugin class
	$wp_plugin_template = new Steampunk_Gallery();
}