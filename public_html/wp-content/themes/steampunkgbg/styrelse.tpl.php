<?php
/*
	Template name: Lista styrelsemedlemmar
*/

	global $post;
	get_header(); 
?>
			
	<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
	<article>
		<h1><?php the_title(); ?></h1>
		<?php if(has_post_thumbnail()): ?><p><?php the_post_thumbnail('post_featured'); ?></p><?php endif; ?>
		<?php the_content(); ?>

		<?php
			$committee = get_posts(array(
				'posts_per_page' => -1,
				'post_type' => 'committeemember',
				'meta_key' => 'in_committee_value',
				'meta_value' => 'true',
				'orderby' => 'menu_order',
			));

			$outside = get_posts(array(
				'posts_per_page' => -1,
				'post_type' => 'committeemember',
				'meta_key' => 'in_committee_value',
				'meta_value' => 'false',
				'orderby' => 'menu_order',
			));

			if ($committee) :
				$i = 1;
		?>

		<!--h2>Styrelsemedlemmar</h2-->
		<ul class="committee-list">
			<?php 
				foreach ($committee as $cm) : 
					$member = get_posts(array(
						'connected_type' => 'committee_to_member',
						'connected_items' => $cm,
						'nopaging' => true,
						'suppress_filters' => false
					));
			?>

			<li <?php if($i%2 == 0){ echo 'class="even"'; } ?>>
				<?php 
					if(has_post_thumbnail($cm->ID)) {
						echo get_the_post_thumbnail($cm->ID, 'committee_member_photo');
					}
				?>
				<h3>
				<?php 
					echo apply_filters('the_title', $cm->post_title);

					$member_email = $member ? @get_post_meta($member[0]->ID, 'email_value', true) : '';
					if($member_email){
						echo '&nbsp;&nbsp;<span class="email symb"><a href="mailto:' . $member_email . '" title="Skicka mail">&#9993;</a></span>';
					} 
				?>
				</h3>
				<p class="positions">
				<?php 
					$positions = wp_get_post_terms($cm->ID, 'committee_position');
					$pos = array();
					foreach ($positions as $position){
						$pos[] = $position->name;
					}

					echo ucfirst(strtolower(implode(', ', $pos)));
				?>
				</p>
				<?php echo apply_filters('the_content', $cm->post_content); ?>
			</li>

			<?php $i++; endforeach; ?>
		</ul>
		<?php 
			endif; 
			if ($outside) : 
		?>

		<h2>Övriga</h2>
		<ul class="other-posts-list">
			<?php 
				foreach ($outside as $m) : 
					$member = get_posts(array(
						'connected_type' => 'committee_to_member',
						'connected_items' => $m,
						'nopaging' => true,
						'suppress_filters' => false
					));
			?>
			<li>
				<?php 
					$member_email = $member ? @get_post_meta($member[0]->ID, 'email_value', true) : '';
					if($member_email){
						echo '<a href="mailto:' . $member_email . '">';
					}

					echo '<strong>' . apply_filters('the_title', $m->post_title) . '</strong>';

					if($member_email){
						echo '</a>';
					}

					$positions = wp_get_post_terms($m->ID, 'committee_position');
					$pos = array();
					foreach ($positions as $position){
						$pos[] = $position->name;
					}

					echo '&mdash;' . ucfirst(strtolower(implode(', ', $pos)));
				?>
			</li>
			<?php endforeach; ?>
		</ul>
		<?php endif; ?>

	</article>
	<?php endwhile; endif; ?>
			
<?php get_footer(); ?>