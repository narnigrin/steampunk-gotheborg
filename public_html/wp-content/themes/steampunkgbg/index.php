<?php 
	global $post;
	get_header(); 
?>

	<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
	<article>
		<h1><?php the_title(); ?></h1>

		<?php if(is_single()): ?><p class="postmeta head"><?php printPostMeta(false); ?></p><?php endif; ?>
		
		<?php if(has_post_thumbnail()): ?><p><?php the_post_thumbnail('post_featured'); ?></p><?php endif; ?>
		<?php the_content(); ?>
	</article>

	<?php if(is_single()): comments_template(); endif; ?>
	<?php endwhile; endif; ?>
			
<?php get_footer(); ?>