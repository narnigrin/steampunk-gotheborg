<!DOCTYPE html>
<html lang="sv-SE">
<head>
	<meta http-equiv="content-type" content="text/html; charset=UTF-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width,initial-scale=1" />
	<meta name="msapplication-config" content="none"/><!-- Because otherwise MSIE gives me 2563 404's for browserconfig.xml -->

	<title><?php wp_title('•', true, 'right'); bloginfo('name'); ?></title>

	<link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png">
	<link rel="icon" type="image/png" href="/favicon-32x32.png" sizes="32x32">
	<link rel="icon" type="image/png" href="/favicon-16x16.png" sizes="16x16">
	<link rel="manifest" href="/manifest.json">
	<link rel="mask-icon" href="/safari-pinned-tab.svg" color="#91643b">
	<meta name="theme-color" content="#f1e0b4">

	<link rel="stylesheet" type="text/css" media="screen" href="<?php echo get_stylesheet_uri(); ?>" />
	<!--[if lt IE 9]>
	<script type="text/javascript">
		//Teaches oldish IEs to believe these elements exist
		document.createElement('header');
		document.createElement('nav');
		document.createElement('section');
		document.createElement('article');
		document.createElement('aside');
		document.createElement('footer');
	</script>
	<link rel="stylesheet" type="text/css" href="<?php echo THEME_URL ?>/ieize-css.php?d=<?php echo get_stylesheet_uri(); ?>" />
	<![endif]-->

	<!--[if lte IE 7]>
	<style type="text/css">
		/* IE<8 don't understand box-sizing:border-box, so they'll add padding to the 'outside' of elements anyway. Thus, recalculate some widths. */
		#inner { width: 910px; }
		.sidebar { width: 233px; }
		article { width: 541px; }
	</style>
	<![endif]-->

	<?php wp_head(); ?>
	
	<?php if(isset($page['styles'])) { ?>
	<style type="text/css" media="screen">
		@import "css/<?php echo $page['styles']; ?>";
	</style>
	<?php } ?>

	<?php global $bodyclass; ?>
	
</head>
<body <?php body_class($bodyclass); ?>>

<div id="container"><div id="bgdiv"><!-- Both of these are for placing background images in ... -->
	<header><a href="<?php echo site_url() ?>" title="Steampunk Götheborg - Hem"><h1 id="mainheader">Steampunk Götheborg</h1></a></header>
	<div id="outer">
		<p id="menulink">
			<a href="#big-main-menu">Navigering</a> <span class="symb">&#59171;</span>
		</p>
		<p id="breadcrumb">
			<span class="symb">&#59172;</span>
			<?php printBreadcrumb($post); ?>
		</p>
		<div id="inner">

			<nav id="big-main-menu"><ul>
			<?php
				global $big_top_menus;
				foreach ($big_top_menus as $id => $name){
					printTopMenu($id);
				}
			?>
			</ul></nav>

			<section id="content-area">

			<?php if(defined('SPGBG_ERRORMESSAGE')): ?>
			<p class="error main-msg"><?php echo SPGBG_ERRORMESSAGE ?></p>
			<?php endif; ?>

			<?php if(defined('SPGBG_SUCCESSMESSAGE')): ?>
			<p class="main-msg"><?php echo SPGBG_SUCCESSMESSAGE ?></p>
			<?php endif; ?>