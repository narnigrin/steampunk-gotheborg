<?php
	new PostType(
		'Committee Member', 
		'Committee',
		array(
			'supports' => array('title', 'editor', 'thumbnail'),
			'taxonomies' => array('committee_position'),
			'menu_position' => 22,
			'menu_icon' => 'dashicons-groups',
		)
	);

	new MetaBox(array(
		'committeemember' => array(
			'Other settings' => array(
				'in_committee' => array(
					'type' => 'boolean',
					'label' => 'In club committee: ',
					'description' => 'Auditors (revisorer) and members of the nominating committee (valberedningen) do not count as members of the club committee proper. <strong>Uncheck this box</strong> if the person should be listed separately from the committee.',
					'default' => 'true',
				),
			),
		),
	));

	// Thumbnail sizes
	add_image_size('committee_member_photo', 150, 220, true);


	// Show custom columns in admin
	add_filter( 'manage_committeemember_posts_columns', 'sp_edit_committee_columns' ) ;
	add_action('manage_committeemember_posts_custom_column', 'sp_columns_content_committee', 10, 2);

	function sp_edit_committee_columns( $columns ) {
		$columns['title'] = __('Name');
		$columns['incomm'] = __('In Committee');
		return $columns;
	}
	function sp_columns_content_committee($column_name, $post_ID) {
		if($column_name == 'incomm'){
			$val = @get_post_meta($post_ID, 'in_committee_value', true);
			if($val === 'true'){
				echo '✓';
			}
			else{
				echo '&ndash;';
			}
		}
	}

	add_action('init', 'committee_reg_taxo');
	function committee_reg_taxo(){
		register_taxonomy( 'committee_position', 'committeemember', array(
			'label' => 'Position',
			'public' => false,
			'show_ui' => true,
			'show_in_nav_menus' => false,
			'show_admin_column' => true,
			'hierarchical' => true,
			'rewrite' => false,
			'capabilities' => array(
				'manage_terms' => 'edit_theme_options',
				'edit_terms' => 'edit_theme_options',
				'delete_terms' => 'edit_theme_options',
			),
		));
	}

	function committee2member(){
		p2p_register_connection_type( array(
		    'name' => 'committee_to_member',
		    'from' => 'committeemember',
		    'to' => 'member',
		    'cardinality' => 'one-to-one',
		    'admin_column' => 'from',
		    'admin_box' => array(
	            'show' => 'from'
	        ),
		));
	}
	add_action( 'p2p_init', 'committee2member' );

?>