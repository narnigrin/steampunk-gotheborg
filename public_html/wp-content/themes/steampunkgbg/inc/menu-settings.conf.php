<?php
	/*
	 * Originally from a mini-plugin by Erika Nilsson @ The Farm (licensed MIT, http://opensource.org/licenses/MIT) <erika@thefarm.se>.
	 * Inspired and helped hugely by http://ottopress.com/2009/wordpress-settings-api-tutorial/
	 * Modified for Steampunk Götheborg by Erika "Masala" Nilsson <erika@erikanilsson.eu> (yes, same person, different capacity)
	 */

	/*
	 * Add settings page
	 */
	add_action('admin_menu', 'spgbg_settings_add');
	function spgbg_settings_add() {
		add_submenu_page( 'themes.php', 'Steampunk Gbg menu settings', 'Steampunk menus', 'edit_theme_options', 'spgbg-theme-settings', 'spgbg_settings_admin_page');
		add_action( 'admin_init', 'spgbg_register_settings' );
	}

	/*
	 * Register all settings. Doing this in a for loop to ensure we cover all menus/menu positions. 
	 */
	function spgbg_register_settings() {

		// I shouldn't need both of these but
		global $big_top_menus;
		$registeredMenus = $big_top_menus;

		foreach ($registeredMenus as $id => $name){
			// Define actual settings
			register_setting( 'spgbg_options', 'spgbg_' . $id . '_menu_bg', 'spgbg_validate' );
			register_setting( 'spgbg_options', 'spgbg_' . $id . '_menu_position', 'spgbg_validate' );

			// Define setting page section
			$menu_name = getMenuNameByLocationId($id);
			if (!$menu_name)
				$menu_name = "<em>not assigned</em>";
			add_settings_section( 'spgbg_settings_main', $name . ' <span style="font-weight:normal">(' . $menu_name . ')</span>', NULL, 'spgbg_' . $id . '_settings');

			// Define settings fields
			add_settings_field( 'spgbg_' . $id . '_menu_bg', 'Background image', 'spgbg_menu_bg_settings_output', 'spgbg_' . $id . '_settings', 'spgbg_settings_main', array($id, $name));
			add_settings_field( 'spgbg_' . $id . '_menu_position', 'Background position (CSS)', 'spgbg_menu_position_settings_output', 'spgbg_' . $id . '_settings', 'spgbg_settings_main', array($id, $name));
		}
	}
	
	/*
	 * Output callback - admin page and wrappers
	 */
	function spgbg_settings_admin_page() {
		if ( !current_user_can( 'edit_pages' ) )  {
			wp_die( __( 'You do not have sufficient permissions to access this page.' ) );
		} 
		?>
		<div class="wrap">
			<h1>Steampunk Götheborg menu settings</h1>
			<p>This page is for uploading images to use as backgrounds for the menu "boxes" (i.e. the top menus when not folded-out). The images will not be tiled, so pick a reasonably big one. :)</p>
			<p><strong>"Background position (CSS)"</strong> is a value for how to position the background image against the edges of the box. 
				It uses the same syntax as the values of the CSS background-position property&mdash;see <a href="#bgpos-hints">below</a> for some hints, 
				or <a href="http://www.w3schools.com/cssref/pr_background-position.asp" target="_blank">this external link</a> for more details on the syntax.
				If you're uncomfortable with CSS or don't know what to write, leave the field/fields empty and the background image will be centered.</p>

			<form method="post" action="options.php"> 
				<?php 
					submit_button(); 
					settings_fields( 'spgbg_options' );

					global $big_top_menus;
					$registeredMenus = $big_top_menus;
					foreach ($registeredMenus as $id => $name){
						do_settings_sections( 'spgbg_' . $id . '_settings' );
					}

					submit_button(); 
				?>
			</form>

			<h2 id="bgpos-hints">Background-position hints/examples</h2>
			<table style="text-align:left!important; vertical-align:top">
				<tr>
					<th scope="row" style="width:100px">left top</th>
					<td>The top left corner of the image will be at the top left corner of the box. The order of "left" and "top" is irrelevant.</td>
				</tr>
				<tr>
					<th scope="row">right top</th>
					<td>The top <em>right</em> corner of the image will be at the top <em>right</em> corner of the box.</td>
				</tr>
				<tr>
					<th scope="row">left bottom</th>
					<td>The <em>bottom</em> left corner of the image will be at the <em>bottom</em> left corner of the box.</td>
				</tr>
				<tr>
					<th scope="row">center bottom</th>
					<td>The bottom edge of the image will be at the bottom edge of the box, and the image is centered horizontally.</td>
				</tr>
				<tr>
					<th scope="row">center center</th>
					<td>The image is centered both horizontally and vertically (default setting).</td>
				</tr>
				<tr>
					<th scope="row">12% 64%</th>
					<td>Given two percentages (the first for the horizontal dimension, the second for the vertical&mdash;this time, order <strong>is</strong> important), 
						the image is positioned from the top left corner (sort of) using those percentages and some magic.<br>
						<a href="http://www.sitepoint.com/css-using-percentages-in-background-image/" target="_blank">This is a good explanation</a> of how percentage-based background positions work,
						but honestly the best way to figure out the perfect numbers is by trial and error.
					</td>
				</tr>
			</table>

			<style type="text/css">
				h1 {
					color: #603913;
					text-shadow: 1px 1px 0 #fff;
				}
				h3 {
					color: rgb(241, 241, 241);
					padding: 1ex;
					margin: 1ex 0 0;
					background-color: #754c24;
					opacity: 0.5;
					border-top-left-radius: 10px;
					border-top-right-radius: 10px;
				}

				.form-table th {
					padding-top: 10px;
					padding-bottom: 0;
				}
				.form-table td {
					padding-top: 5px;
					padding-bottom: 0;
				}
				.form-table {
					margin-bottom: 2em;
				}
			</style>
		</div>
		<?php 
	}
	
	/*
	 * Output callback - background image setting fields
	 */
	function spgbg_menu_bg_settings_output($args) {
		?>
		<input type="hidden" id="spgbg_<?php echo $args[0] ?>_menu_bg" class="spgbg_menu_bg" name="spgbg_<?php echo $args[0] ?>_menu_bg" value="<?php echo esc_attr( get_option('spgbg_' . $args[0] . '_menu_bg', '') ) ?>" style="width:99%" />
		<span class="spgbg_image_container" style="float:left; ">
		</span>
		<a class="button spgbg_image_upload">Attach image</a><br><a class="spgbg_remove_image">Remove image</a>
		<?php
	}
	/*
	 * Output callback - background position setting fields
	 */
	function spgbg_menu_position_settings_output($args) {
		?>
		<input type="text" id="spgbg_<?php echo $args[0] ?>_menu_position" name="spgbg_<?php echo $args[0] ?>_menu_position" value="<?php echo esc_attr( get_option('spgbg_' . $args[0] . '_menu_position', '') ) ?>" style="width:99%" />
		<?php
	}
	
	/*
	 * Validation callback. 
	 * At the moment, this is just very rudimentary security. Could probably be improved upon (majorly).
	 */
	function spgbg_validate($input) {
		return trim(strip_tags($input));
	}

?>