<?php
	/*
	 * Actual calendar communication bits.
	 * 
	 * Required parameters:
	 * $timeMin 		Date in Y-M-d format
	 * $timeMaxExcl 	Date in Y-M-d format
	 * 
	 * Optional parameters:
	 * $maxNum			Integer
	 * 
	 * Return: $allevents, an array of events present
	 */

date_default_timezone_set("Europe/Stockholm");

function cal_connect_data($timeMin, $timeMaxExcl, $maxNum=null){
	//Set up request content nicely
	$apikey = get_option('spgbg_cal_apikey');
	$calendar = get_option('spgbg_cal_calendar');

	$timeMin = "{$timeMin}T00:00:00+01:00";
	$timeMaxExcl = "{$timeMaxExcl}T00:00:00+01:00";
	$orderBy = "startTime&singleEvents=true";
	$maxNum = (isset($maxNum)) ? "&maxResults={$maxNum}" : "";

	//See https://developers.google.com/google-apps/calendar/v3/reference/events/list#try-it
	$fields = array ( 
		//"colorId",
		"description",
		"end",
		//"htmlLink",
		"id",
		"location",
		//"recurringEventId",
		"start",
		"summary",
		//"transparency"
	);
	$fields_string = implode(",",$fields);
	
	//Format request
	$get_request = "https://www.googleapis.com/calendar/v3/calendars/".urlencode($calendar).
		       "/events?orderBy=".$orderBy.
		       "&timeMax=".urlencode($timeMaxExcl)."&timeMin=".urlencode($timeMin).
		       "&fields=items(".urlencode($fields_string).")".
		       "&key={$apikey}".
		       $maxNum.
		       "&timeZone=Europe%2FStockholm";
	
	$GCal_JSON = @file_get_contents($get_request);
	$eventslist = json_decode($GCal_JSON,true); // 2nd arg sez return associative array
	
	//Error check (we got no data => go home)
	if($eventslist === NULL) {
		$headers = @get_headers($get_request);
		$error = "<strong style=\"color:red\">Oh snap!<br>The server's response was ".(empty($headers[0])?"empty (which is probably weird).":$headers[0])."</strong>";
		return $error;
	}
	
	$eventslist = $eventslist['items'];
	
	//################## REFORMAT ARRAY #####################
	//addEvent() just slightly simplifies adding events to the $allevents array in the foreach below. :)
	$allevents = array();
	function addEvent($id,$title,$desc,$loc,$type,$date,$start,$end) {
		return array(
			'gcalEventId'=>$id,
			'title'=>$title,
			'description'=>$desc,
			'location'=>$loc,
			
			'type'=>$type,
			'date'=>$date,
			'start'=>$start,
			'end'=>$end
		);
	}
	
	$i = 0;
	foreach ($eventslist as $thisevent) {
		$id=$thisevent['id'];
		$title=(isset($thisevent['summary'])?$thisevent['summary']:'(Namnlös händelse)');
		$description=(isset($thisevent['description'])?$thisevent['description']:'');
		$location=(isset($thisevent['location'])?$thisevent['location']:'');
		
		//Un-timed event
		if(isset($thisevent['start']['date'])) {
			$start = "";
			$end = "";
			
			//Counts as a one-day, all-day event
			if($thisevent['end']['date'] == date("Y-m-d",strtotime($thisevent['start']['date']." +1 day"))) {
				$type = "allday";
				$date = $thisevent['start']['date'];
				$allevents[$i] = addEvent($id,$title,$description,$location,$type,$date,$start,$end);
				$i++;
			}
			//Counts as a multi-day, all-day event
			else {
				$daysLength = (int) (strtotime($thisevent['end']['date'])-strtotime($thisevent['start']['date']))/86400 - 1; //86400s=24h. Leap seconds are effectively irrelevant and get truncated.
				
				//Split event into several distinct "day events"
				for($j=0; $j<=$daysLength; $j++) {
					$type = "allday multi";
					switch($j) {
						case 0:
							$type .= " first";
							$date = $thisevent['start']['date'];
							break;
						case $daysLength:
							$type .= " last";
						default:
							$date = date("Y-m-d", strtotime($thisevent['start']['date']." +".$j." day".($j==1?"":"s")));
					}
					
					$allevents[$i] = addEvent($id,$title,$description,$location,$type,$date,$start,$end);
					$i++;
				}
			}
		}
		//Timed event starting and ending on different days
		elseif(isset($thisevent['start']['dateTime']) && substr($thisevent['start']['dateTime'],0,10)!=substr($thisevent['end']['dateTime'],0,10)) {
			$startDate = substr($thisevent['start']['dateTime'],0,10);
			$endDate = substr($thisevent['end']['dateTime'],0,10);
		
			//Counts as "same-day"
			if($endDate==date("Y-m-d",strtotime($startDate." +1 day")) && date("H:i",strtotime($thisevent['end']['dateTime']))=="00:00") { 
				$type = 'normal';
				$date = $startDate;
				$start = date("H:i",strtotime($thisevent['start']['dateTime']));
				$end = date("H:i",strtotime($thisevent['end']['dateTime']));
				
				$allevents[$i] = addEvent($id,$title,$description,$location,$type,$date,$start,$end);
				$i++;
			}
			else {
				$daysLength = (int) (strtotime($endDate)-strtotime($startDate))/86400; //86400s=24h. Leap seconds are effectively irrelevant and get truncated.
				if(date("H:i",strtotime($thisevent['end']['dateTime']))=="00:00")
					$daysLength--; //00:00 counts as the day before
				
				//Split event into several distinct "day events"
				for($j=0; $j<=$daysLength; $j++) {
					$start = "";
					$end = "";
					$type = "multi";
					switch($j) {
						case 0:
							$type .= " first";
							$start = date("H:i",strtotime($thisevent['start']['dateTime']));
							$date = $startDate;
							break;
						case $daysLength:
							$type .= " last";
							$end = date("H:i",strtotime($thisevent['end']['dateTime']));
						default:
							$date = date("Y-m-d", strtotime($startDate." +".$j." day".($j==1?"":"s")));
					}
					
					$allevents[$i] = addEvent($id,$title,$description,$location,$type,$date,$start,$end);
					$i++;
				}
			}
			
		}
		//Timed event starting and ending on the same day
		else { 
			$type = 'normal';
			$date = substr($thisevent['start']['dateTime'],0,10);
			$start = date("H:i",strtotime($thisevent['start']['dateTime']));
			$end = date("H:i",strtotime($thisevent['end']['dateTime']));
			
			$allevents[$i] = addEvent($id,$title,$description,$location,$type,$date,$start,$end);
			$i++;
		}
	}
	unset($i);
	unset($id,$title,$description,$location,$type,$date,$start,$end,$startDate,$endDate);
	return $allevents;
}
?>