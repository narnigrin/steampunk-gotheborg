<?php
	/**
	 * AJAX loading function for big calendar. Imitates the similar one for calendar widget.
	 * Uses Westman's AjaxAPI (WPToolkit)
	 */
	function ajaxLoadBigCal(){
		// Messages 
		$monthNamesSwe = array(1=>"januari","februari","mars","april","maj","juni","juli","augusti","september","oktober","november","december");
		$calEmptyMsg = "<em>Kalendern är tom den här månaden.<br>Hitta på någonting själv!</em>";

		global $AjaxAPI;

		// Set up 
		$ymNow = (isset($_REQUEST['m'])&&!empty($_REQUEST['m']))?$_REQUEST['m']:date("Y-m");
		$ymNext = date("Y-m",strtotime($ymNow."-01 +1 month"));
		$ymPrev = date("Y-m",strtotime($ymNow."-15 -1 month"));
		$timeMin = "{$ymNow}-01";
		$timeMaxExcl = "{$ymNext}-01";

		// ################ CALENDAR COMMUNICATION HERE --v ################
		require('cal_connect_data.php'); 
		$allevents = cal_connect_data($timeMin, $timeMaxExcl);


		//######################### OUTPUT #################################
		//Gets a weekday number and reformats it from Sunday-Saturday to Monday-Sunday
		function dayNum($date) {
			$w = date("w",strtotime($date));
			return ($w+6)%7;
		}

		$daysInMonth = date("t",strtotime($ymNow)); //Use $ymNow instead of time() to allow for showing other months than the current, later on
		$monthDayOffset = dayNum($ymNow."-01");

		// Save prev and next months in accessible-by-jQuery format
		$op = '<input type="hidden" id="prevMonth" value="' . $ymPrev . '"><input type="hidden" id="nextMonth" value="' . $ymNext . '">';
		$op .= "<h1 class=\"calmonth\">" . ucfirst($monthNamesSwe[date("n",strtotime($ymNow))]) . " " . substr($ymNow,0,4);
		//$op .= "<a class=\"symb\" id=\"openCalLinks\">&#9662;</a>";
		$op .= "</h1>";
		
		// --------------- Small-screen table starts ------------------
		$op .= "<!-- ### Small-screen calendar ### -->\r\n<table class=\"calendar cal-small\">\r\n";

		function sortByDate($a, $b) {
			$dateA = str_replace("-", "", $a['date']);
			$dateB = str_replace("-", "", $b['date']);
		
			if($dateA == $dateB) { // If dates are equal, sort by event type
				if(stristr($a['type'],"multi")!==false && stristr($b['type'],"multi")===false) : return -1; endif; // Multi-day first
				if(stristr($a['type'],"allday")!==false && stristr($b['type'],"allday")===false) : return -1; endif; // All-day second
				
				// Type is equal: Sort by start time, then end time, then alphabetically by title. 
				// (NB: strnatcasecmp() might treat non-latin-1 characters funny)
				$startA = str_replace(":", "", $a['start']);
				$startB = str_replace(":", "", $b['start']);
				if($startA == $startB) {
					$endA = str_replace(":", "", $a['end']);
					$endB = str_replace(":", "", $b['end']);
					if($endA == $endB) {
						return strnatcasecmp($a['title'], $b['title']); 
					}
					return ($endA < $endB) ? -1 : 1;
				}
				return ($startA < $startB) ? -1 : 1;
			}
			return ($dateA < $dateB) ? -1 : 1;
		}

		if(is_array($allevents)){
			usort($allevents, "sortByDate");
		
			$prevDateWritten = "";
			foreach ($allevents as $event) {
				//If we've accidentally caught an event which starts the previous month, remove it
				if(date("Ym",strtotime($event['date'])) < str_replace("-", "", $ymNow)) {
					unset($event);
					continue;
				}
			
				$op .= "<tr class=\"event-row";
				if (stristr($event['type'],"allday")!==false) : $op .= " allday"; endif;
				if (stristr($event['type'],"multi")!==false) : $op .= " multi-day"; endif;
				if (stristr($event['type'],"first")!==false) : $op .= " first"; endif;
				
				$op .= "\"><td class=\"calendar-daynum\">";
				// Write each day number at most once
				if ($prevDateWritten != $event['date']) {
					$op .= date("d", strtotime($event['date']));
					$prevDateWritten = $event['date'];
				}
				$op .= "</td>";
				
				$op .= "<td class=\"event-time\"><p><span>" . $event['start'] . "&ndash;</span>" . $event['end'] . "</p></td>";
				$op .= "<td><h3>" . $event['title'] . "</h3>";
				$op .= (!empty($event['location']) ? "<p><em>" . $event['location'] . "</em></p>" : "");
				$op .= "</td></tr>";
			}
		}
		
		if (count($allevents) < 1)
			$op .= "<tr><td><p style=\"text-align: center\">" . $calEmptyMsg . "</p></td></tr>";

		$op .= "\r\n</table>\r\n";
		// ------------ Small-screen table ends -----------


		// ------------ Desktop-size table starts ------------
		$op .= "<!-- ### Desktop-size calendar ### -->\r\n<table class=\"calendar cal-large\">\r\n<tr><th>mån</th> <th>tis</th> <th>ons</th> <th>tor</th> <th>fre</th> <th>lör</th> <th>sön</th>";

		for($i=0;$i<=$daysInMonth+$monthDayOffset-1;$i++) {
			if($i%7 == 0)
				$op .= "\t\t</tr>\n\t\t<tr>\n";
			
			$iDate = $i-$monthDayOffset+1;
			$iDateYmd = $ymNow."-".($iDate<10?"0":"").$iDate;
			if($iDate < 1) 
				$op .= "\t\t\t<td class=\"empty\"></td>\n";
			else {
				$op .= "\t\t\t<td";
				$op .= ($iDateYmd == date("Y-m-d")?" class=\"today\"":"");
				$op .= "><div class=\"calendar-td-wrapper-outer\">\n";
				$op .= "\t\t\t\t<div class=\"calendar-daynum\">" . $iDate . "</div>\n";
				$op .= "<div class=\"calendar-td-wrapper\">\n";
				
				if(is_array($allevents)){
					foreach($allevents as $key=>$event) {
						if($event['date'] == $iDateYmd) {
							$op .= "\t\t\t\t<div class=\"calendar-event";
							if (stristr($event['type'],"allday")!==false) : $op .= " allday"; endif;
							if (stristr($event['type'],"multi")!==false) : $op .= " multi-day"; endif;
							if (stristr($event['type'],"first")!==false) : $op .= " first"; endif;
							$op .= "\">\n";
							$op .= "\t\t\t\t\t<h3>" . $event['title'] . "</h3>\n";
							$op .= "\t\t\t\t\t<p>" . $event['start'];
							$op .= "&mdash;" . $event['end'] . "</p>\n";
							$op .= "\t\t\t\t\t<p><em>" . $event['location'] . "</em></p>\n";
							$op .= "\t\t\t\t</div>\n";
							
							unset($allevents[$key]);
						}
						else {
							//Nothing to see here.
						}
					}
				}
				
				$op .= "\t\t\t</div></div></td>\n";
			}
		}

		$op .= "</tr>\r\n</table>\r\n";
		// --------------- Desktop-size table ends -------------------

		// Wrap up
		$AjaxAPI->ReturnJSON(true, $op);
	}
?>
	