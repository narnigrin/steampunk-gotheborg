<?php
/*
 * Steampunk calendar widget
 * Shows next N events from a Google calendar (user chooses N)
 * 
 * Heavily uses http://www.wpbeginner.com/wp-tutorials/how-to-create-a-custom-wordpress-widget/
 *
 * @author 	Erika "Masala" Nilsson
 */

// Creating the widget 
class spgbg_cal_widget extends WP_Widget {

	function __construct() {
		parent::__construct(
			// Base ID of your widget
			'spgbg_cal_widget', 

			// Widget name will appear in UI
			__('Steampunk Götheborg calendar widget', 'spgbg_cal_widget_domain'), 

			// Widget description
			array( 'description' => __( 'Shows the next few events from the Google calendar', 'spgbg_cal_widget_domain' ), ) 
		);
	}

	// Creating widget front-end
	// This is where the action happens
	public function widget( $args, $instance ) {
		$title = apply_filters( 'widget_title', $instance['title'] );
		$numevents = $instance['numevents'];

		// Defined elsewhere in theme/core
		echo $args['before_widget'];

		// Actual widget content follows
		if ( ! empty( $title ) )
			echo $args['before_title'] . $title . $args['after_title'];

		if ( empty( $numevents ) )
			$numevents = 3;

		echo '<table class="spgbg_cal_widget_table" data-time-min="'.date('Y-m-d').'" data-time-max="'.date('Y-m-d', strtotime("+ 3 months")).'" data-num-events="'.$numevents.'">';
		echo '<tr class="row-even"><td><em>Laddar ...</em></td></tr>'; // This will be removed when AJAX is complete
		echo '</table>';
		echo '<a class="spgbg_cal_widget_page_link" href="', site_url(), '/kalendarium">Kalendarium &rsaquo;</a>';

		// Defined elsewhere in theme/core
		echo $args['after_widget'];
	}
			
	// Widget Backend 
	public function form( $instance ) {
		if ( isset( $instance[ 'title' ] ) ) {
			$title = $instance[ 'title' ];
		}
		else {
			$title = __( 'New title', 'spgbg_cal_widget_domain' );
		}

		if ( isset( $instance[ 'numevents' ] ) ) {
			$numevents = $instance[ 'numevents' ];
		}
		else {
			$numevents = '3';
		}

		// Widget admin form
		?>
		<p>
			<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:' ); ?></label> 
			<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
		</p>

		<p>
			<label for="<?php echo $this->get_field_id( 'numevents' ); ?>"><?php _e( 'Number of events to show:' ); ?> </label> 
			<input class="" id="<?php echo $this->get_field_id( 'numevents' ); ?>" name="<?php echo $this->get_field_name( 'numevents' ); ?>" type="text" value="<?php echo esc_attr( $numevents ); ?>" />
		</p>
		<?php 
	}
		
	// Updating widget replacing old instances with new
	public function update( $new_instance, $old_instance ) {
		$instance = array();
		$instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
		$instance['numevents'] = ( ! empty( $new_instance['numevents'] ) ) ? strip_tags( $new_instance['numevents'] ) : '';
		return $instance;
	}
} // Class wpb_widget ends here

// Register and load the widget
function spgbg_load_cal_widget() {
	register_widget( 'spgbg_cal_widget' );
}
add_action( 'widgets_init', 'spgbg_load_cal_widget' );




// #####################
// AJAX loading function
	function ajaxLoadCalEvents(){
		global $AjaxAPI;

		$timeMin = isset($_REQUEST['min']) ? $_REQUEST['min'] : date('Y-m-d');
		$timeMaxExcl = isset($_REQUEST['max']) ? $_REQUEST['max'] : date('Y-m-d', strtotime("+ 3 months"));
		$maxNum = isset($_REQUEST['num']) ? (int) $_REQUEST['num'] : 3;
	
		require('cal_connect_data.php');
		$allevents = cal_connect_data($timeMin, $timeMaxExcl, $maxNum);
		$op = "";

		if(!isset($allevents) || count($allevents) < 1 || !is_array($allevents)){
			$op .= $allevents . '<tr class="row-even"><td><em>Kalendern är tom den närmaste tiden. Hitta på något själv!</em></td></tr>';
		}
		else{
			for($i=0; $i<count($allevents); $i++){
				$op .= '<tr class="row-' . (($i%2 == 0) ? 'even' : 'odd') . '">';
				$op .= '<td class="event-name">' . $allevents[$i]['title'] . '</td>';
				$op .= '<td class="event-date">' . strtolower(date('j M', strtotime($allevents[$i]['date']))) . '</td>';
				$op .= "</tr>\r\n";
			}
		}

		$AjaxAPI->ReturnJSON(true, $op);
	}

?>