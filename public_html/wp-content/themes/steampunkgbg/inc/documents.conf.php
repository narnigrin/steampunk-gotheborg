<?php
	new PostType(
		'Document', 
		'Documents', 
		array(
			'supports' => array('title'),
			'taxonomies' => array('document_type'),
			'menu_position' => 23,
			'menu_icon' => 'dashicons-media-document',
		)
	);

	new MetaBox(array(
		'document' => array(
			'Document settings' => array(
				'docfile' => array(
					'type' => 'text',
					'description' => '<div id="spgbg-addfile-name-display" style="margin-top:-3em;margin-bottom:1ex;font-weight:bold;color:grey;height:1.3em">
							<span class="dashicons dashicons-media-document" style="display:none"></span> 
							<span class="name"></span>
						</div>
						<button class="button button-primary spgbg_addfile" style="vertical-align:middle">Upload or select file</button>
						<a class="spgbg_removefile" style="font-size:90%;vertical-align:middle">Remove file</a>',
					'style' => 'display:none;margin-top:-100%',
				),
				'attachment_id' => array(
					'type' => 'text',
					'style' => 'display:none'
				),
				'docdate' => array(
					'type' => 'date',
					'label' => 'Document date',
					'description' => 'The date this document was written, signed or similar, in the format "YYYY-MM-DD". Not necessarily the upload date. :-)',
					'style' => 'width:99%',
					//'placeholder' => date('Y-m-d', time()),
				),
			),
			'Make document private' => array(
				'private' => array(
					'type' => 'boolean',
					'label' => 'Make this document private: ',
					'default' => 'false',
					'description' => '</em></p>
										<p><strong>Private documents, sensitive information and PUL</strong><br>
										Private documents are club members-only&mdash;they are invisible to anyone who is not a logged-in member of Steampunk Götheborg.
										You should make documents private whenever you suspect that there might be a privacy concern in publishing them publically, 
										for example, if a document contains the name of a member who has been sanctioned for bad behaviour, or otherwise says something negative 
										about a person who is mentioned by name. Publishing documents with negative details about a named/identified person publically can sometimes be 
										in breach of PUL, but limiting the audience usually mitigates that risk. If the personal details are of a particularly sensitive nature, 
										you could always redact (censor/black out) the concerned person\'s name and other identifying info before uploading.<br>
										<em><a href="http://www.datainspektionen.se/lagar-och-regler/personuppgiftslagen/publicering-pa-internet/" target="_blank">Read more about the relevant rules here</a>',
				),
			),
		),
	));


	// Show custom columns in admin. Because the default are pretty unhelpful here
	add_filter( 'manage_document_posts_columns', 'sp_edit_document_columns' ) ;
	add_action('manage_document_posts_custom_column', 'sp_columns_content_document', 10, 2);

	function sp_edit_document_columns( $columns ) {
		//$columns['private_doc'] = "Private";

		$columns = array(
			'title' => __('Title'),
			'author' => __('Uploaded by'),
			'taxonomy-document_type' => __('Document type'),
			'private_doc' => __('Private'),
			'docdate' => __('Document created/signed'),
			'date' => __('Upload date'),
		);

		return $columns;
	}
	function sp_columns_content_document($column_name, $post_ID) {
		if($column_name == 'private_doc'){
			$priv = @get_post_meta($post_ID, 'private_value', true);
			if($priv === 'true'){
				echo '✓';
			}
			else{
				echo '&ndash;';
			}
		}
		elseif($column_name = 'docdate'){
			echo @get_post_meta($post_ID, 'docdate_value', true);
		}
	}


	add_action('init', 'documents_reg_taxo');
	function documents_reg_taxo(){
		register_taxonomy( 'document_type', 'document', array(
			'label' => 'Document type',
			'public' => false,
			'show_ui' => true,
			'show_in_nav_menus' => false,
			'show_admin_column' => true,
			'hierarchical' => true,
			'rewrite' => false,
			'capabilities' => array(
				'manage_terms' => 'edit_theme_options',
				'edit_terms' => 'edit_theme_options',
				'delete_terms' => 'edit_theme_options',
			),
		));
	}


	// ######################
	// Template functions

	/**
	 * Takes as argument the "base" category we want to search from. It assumes document categories.
	 * Plugging in "0" makes it look through all categories (still only document categories).
	 */
	function hierarchical_category_tree($cat) {
		// http://wordpress.stackexchange.com/a/41559 - modified
		$next = get_categories('type=document&taxonomy=document_type&orderby=name&order=ASC&parent=' . $cat);
		$cats = array();

		if( $next ) :    
			foreach( $next as $cat ) :
				$temp = array(
					'slug' => $cat->slug,
					'name' => $cat->name,
					'term_id' => $cat->term_id,
					'children' => hierarchical_category_tree($cat->term_id),
				);

				$cats[] = (object)$temp;
			endforeach;    
		endif;

		return $cats;
	}

	/**
	 * Expects an array of categories with at least a name, slug and term_id for each category.
	 * Hierarchical categories are supported via the key 'children'.
	 * $level modifies the header tag (h1, h2 etc) of the category name.
	 */
	function hierarchical_print_documents($doccats, $level = 1){
		if(count($doccats) < 1)
			return; 

		foreach ($doccats as $cat){
			$children = isset($cat->children) ? $cat->children : array();

			$args = array(
				'posts_per_page' => -1,
				'post_type' => 'document',
				'meta_key' => 'docdate_value',
				'orderby' => 'meta_value menu_order',
				'tax_query' => array(array(
					'taxonomy' => 'document_type',
					'field' => 'slug',
					'terms' => $cat->slug,
					'operator' => 'IN',
				)),
			);

			// Attempt to ignore documents that are also in direct descendant categories
			if(count($children) > 0){
				$children_ids = array();
				foreach ($children as $child){
					$children_ids[] = $child->term_id;
				}

				$args['tax_query'][] = array(
					'taxonomy' => 'document_type',
					'field' => 'id',
					'terms' => $children_ids,
					'operator' => 'NOT IN',
				);
				$args['tax_query']['relation'] = 'AND';
			}

			// Remove "private" documents from results if user isn't logged in and a club member
			if (!current_user_can('read_club_only_posts')){
				$args['meta_query'] = array(array(
		            'key' => 'private_value',
		            'value' => 'true',
		            'compare' => '!=',
		        ));
			}

			$docs = get_posts($args);

			// Print documents
			if (count($docs) > 0 || count($children) > 0):
				echo "<h{$level} class='doclist-header'>" . $cat->name . "</h{$level}>"; ?><ul class="doclist doclist-level-<?php echo $level ?>"><?php

				foreach($docs as $document): 

					$attid = get_post_meta($document->ID, 'attachment_id_value', true); 
					$docdate = strtotime(get_post_meta($document->ID, 'docdate_value', true));
					?>
					<li>
						<a href="<?php echo get_post_meta($document->ID, 'docfile_value', true); ?>" target="_blank" title="Öppna/ladda ned <?php echo get_the_title($document->ID); ?>">
							<div class="docdate">
								<span class="doc-day"><?php echo date('d', $docdate); ?></span>
								<span class="doc-month"><?php echo str_replace(array('May', 'Oct'), array('Maj', 'Okt'), date('M', $docdate)); ?></span>
								<span class="doc-year"><?php echo date('Y', $docdate); ?></span>
							</div>
							<div class="doctitle"><?php 
								if (get_post_meta($document->ID, 'private_value', true) === 'true')
									echo '<span class="symb" style="top:0.04em">&#128274;</span> ';
								echo get_the_title($document->ID); 
							?></div>
							<div class="docfile"><?php echo getAttachmentFileNameFromId($attid); ?></div>
							<?php echo printIconFromFileType(get_post_mime_type($attid), 32, 'docicon'); ?>
						</a>
					</li>
				<?php 
				endforeach;

				// Also print child categories
				echo "<li class='descendants'>";
				hierarchical_print_documents($children, $level+1);
				echo "</li>";

				echo '</ul>';
			endif;
		}
	}

	/**
	 * Given a mime type, prints an img tag with a reasonably appropriate icon.
	 */
	function printIconFromFileType($input_type, $size=32, $classes=""){
		$available_icons = array_diff(scandir(THEME_PATH . '/images/file-type-icons'), array('..', '.'));
		$unknown_type = 'unknown';
		$icon_ext = 'png';

		// Type mappings are regular expressions (without delimiters). The function adds delimiters and start and end characters automagically.
		// They are searched from top to bottom, so if you want, say, all audio files except audio/wav to map to one thing and wav to another, you'd define audio/wav first.
		$type_mappings = array(
			// Straightforward ones
			'application/pdf' => 'pdf',
			'application/postscript' => 'ps',
			'text/plain' => 'txt',
			'image/gif' => 'gif',
			'text/html' => 'html',
			'image/[p]?jpeg' => 'jpg',
			'application/zip' => 'zip',
			'application/x-compressed' => 'zip',
			'application/x-rar-compressed' => 'rar',

			// Document formats are somewhat less straightforward
			'text/richtext' => 'rtf',
			'application/[x-]?rtf' => 'rtf',
			'application/msword' => 'doc',
			'application/[vnd.]?[x-]?[ms]?excel' => 'xls',
			'application/[vnd.]?[x-]?[ms]?powerpoint' => 'ppt',
			'application/vnd.openxmlformats-officedocument.wordprocessingml.document' => 'doc', // I wish I was joking
			'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' => 'xls',
			'application/vnd.sun.xml.writer' => 'odt',
			'application/vnd.oasis.opendocument.text' => 'odt',
			'application/vnd.sun.xml.calc' => 'ods',
			'application/vnd.oasis.opendocument.spreadsheet' => 'ods',
			'application/x-iwork-pages-sffpages' => 'pages', // I really really hope no-one ever uses these
			'application/vnd.apple.pages' => 'pages',

			// Groups/classes
			'audio/[a-z.-]+' => 'mp3',
			'video/[a-z.-]+' => 'avi',
		);
		
		$file_type_result = $unknown_type;
		foreach($type_mappings as $mime => $icon){
			if (in_array($icon . '.' . $icon_ext, $available_icons) && preg_match('#^' . $mime . '#', $input_type)){
				$file_type_result = $icon;
				break;
			}
		}

		// Should print something like <img src="http://steampunkgbg.se/wp-content/themes/Steampunk/images/file-type-icons/jpg.png" alt="jpg" width="32" height="32" class="mime-icon">
		echo '<img src="' . THEME_URL . '/images/file-type-icons/' . $file_type_result . '.' . $icon_ext . '" alt="' . strtoupper($file_type_result) . '" width="' . $size . '" height="' . $size . '" class="mime-icon ' . $classes . '">';
	}



?>