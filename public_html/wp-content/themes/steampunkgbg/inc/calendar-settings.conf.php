<?php

	/*
	 * Add settings page
	 */
	add_action('admin_menu', 'spgbg_cal_settings_add');
	function spgbg_cal_settings_add() {
		add_options_page( 'Google calendar', 'Google calendar', 'manage_options', 'spgbg-google-calendar', 'spgbg_cal_settings_admin_page');
		add_action( 'admin_init', 'spgbg_register_cal_settings' );
	}

	/*
	 * Register all settings. Doing this in a for loop to ensure we cover all menus/menu positions. 
	 */
	function spgbg_register_cal_settings() {
		register_setting('spgbg_cal_settings-group', 'spgbg_cal_apikey');
		register_setting('spgbg_cal_settings-group', 'spgbg_cal_calendar');

		add_settings_section('spgbg_cal_options', '', NULL, 'spgbg_cal_settings_admin_page');

		add_settings_field(
			'spgbg_cal_options_field_apikey', 
			'API key', 
			'spgbg_settings_field_input_text',
			'spgbg_cal_settings_admin_page',
			'spgbg_cal_options',
			array(
	            'field' => 'spgbg_cal_apikey',
	            'desc' => 'Enter the API key that you got from Google. The key is case-sensitive and must allow access from the domain this site sits on.'
	        )
		);
		add_settings_field(
			'spgbg_cal_options_field_calendar', 
			'Calendar', 
			'spgbg_settings_field_input_text',
			'spgbg_cal_settings_admin_page',
			'spgbg_cal_options',
			array(
	            'field' => 'spgbg_cal_calendar',
	            'desc' => 'Enter the e-mail address for the calendar, for example, somecalendar@gmail.com',
	            'placeholder' => 'somecalendar@gmail.com'
	        )
		);
	}
	
	/*
	 * Output callback - admin page and wrappers
	 */
	function spgbg_cal_settings_admin_page() {
		if ( !current_user_can( 'manage_options' ) )  {
			wp_die( __( 'You do not have sufficient permissions to access this page.' ) );
		} 
		?>
		<div class="wrap">
			<h2>Google calendar configuration</h2>

			<form method="post" action="options.php"> 
				<?php 
					settings_fields('spgbg_cal_settings-group');
					do_settings_sections('spgbg_cal_settings_admin_page');

					submit_button(); 
				?>
			</form>
		<?php 
	}
	
	function spgbg_settings_field_input_text($args)
    {
        // Get the field name from the $args array
        $field = $args['field'];
        // Get the value of this setting
        $value = get_option($field);

        $ph = isset($args['placeholder']) ? $args['placeholder'] : '';
        $desc = isset($args['desc']) ? $args['desc'] : '';

        // echo a proper input type="text"
        echo '<input type="text" name="'.$field.'" id="'.$field.'" value="'.$value.'" style="width:99%" placeholder="'.$ph.'"><br><em>'.$desc.'</em>';
    }

?>