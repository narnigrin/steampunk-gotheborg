<?php
/*
	Template name: Lista medlemmar
*/

	restricted_page_check('publish_posts');

	$bodyclass = 'widepage';
	$styrelse = current_user_can('publish_posts');

	// If user has already seen the "tip bubble", don't automatically show it.
	// Also update user meta to say s/he's seen the bubble.
	$user_ID = get_current_user_id();
	$bubbleclass = (get_user_meta($user_ID, 'sp_hidebubble', true) == 'true' ? 'seenalready' : '');
	update_user_meta($user_ID, 'sp_hidebubble', 'true');

	global $post;
	get_header();
?>

<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
	<article>
		<h1>
			<?php the_title(); ?> 

			<?php if($styrelse) : ?>
			<aside id="memberlist-tips">
				<a class="symb" href="javascript:void(0)" title="Hur gör man?">&#59140;</a>

				<div id="memberlist-tips-bubble" class="<?php echo $bubbleclass ?>">
					<h3>Hur funkar det här då?</h3>
					<p>Du som ser den här tipsrutan är antingen styrelsemedlem eller administratör
						och kan redigera medlemsregistret.</p>
					<ul>
						<li><strong>Klicka</strong> på ett värde i tabellen för att redigera.
						<li>Tryck <strong>Enter eller Tab</strong>, eller <strong>klicka utanför fältet</strong>,
							för att lämna redigeringsläget och spara ändringarna.
						<li>Tryck <strong>Escape</strong> för att avbryta redigering utan att spara.
					</ul>

					<p style="margin-bottom:0"><a href="#" class="js-close-bubble">Okej, jag fattar</a></p>
				</div>
			</aside>
			<a class="symb" href="#nymedlem" title="Lägg till ny medlem">&#59136;</a>
			<?php endif; ?>
		</h1>

		<?php if(has_post_thumbnail()): ?><p><?php the_post_thumbnail('post_featured'); ?></p><?php endif; ?>
		<?php 
			the_content();
			$members = $Members->getAll('surname', true);
			if(!empty($members)):
		?>

		<table id="members-table">
			<thead><?php $Members->TemplateTags->printTableHeaders(); ?></thead>
			<tfoot><?php $Members->TemplateTags->printTableHeaders(); ?></tfoot>
			<tbody>
			<?php
				foreach ($members as $member){
					if($styrelse || MembersAjax::ajaxUpdateRowClass('paiduntil', $member->paiduntil) == 'paid'){
						echo $Members->TemplateTags->printMemberRow($member);
					}
				}
			?>
			</tbody>
		</table>
		<?php endif; ?>

		<?php if($styrelse): ?>
			<h2 id="nymedlem">Lägg till ny medlem</h2>
			<form id="addmember" method="POST">
				<p>
					<label for="firstname">Förnamn: </label>
					<input type="text" name="firstname" placeholder="Namn" required>
				</p>
				<p>
					<label for="surname">Efternamn: </label>
					<input type="text" name="surname" placeholder="Namnsson" required>
				</p>
				<p>
					<label for="phone">Telefon&shy;nummer: </label>
					<input type="text" name="phone" placeholder="012345...">
				</p>
				<p>
					<label for="email" style="width:auto; vertical-align:middle">E-post: </label>
					<input type="email" name="email" placeholder="namn@namnsson.com">
				</p>
				<p>
					<label for="comments" style="vertical-align:top; padding-top:0.4em">Kommentarer: </label>
					<textarea name="comments" rows="3"></textarea>
					<small><em>Bara styrelsen och administratörer kan läsa kommentarer.</em></small>
				</p>
				<p>
					<label for="membersince">Blev medlem: </label>
					<input type="date" name="membersince" value="<?php echo date('Y-m-d', time()) ?>">
					<small><em>När personen blev medlem från första början.</em></small>
				</p>
				<p>
					<label for="paiduntil">Medlemsavgift betald till: </label>
					<input type="date" name="paiduntil" value="<?php echo date('Y-m-d', strtotime("+1 year")) ?>" required>
					<small><em>Vanligtvis ett år efter att medlemsavgift senast betalades. För medlemmar med gratis medlemskap, ett år efter att medlemskap senast "bekräftades" av medlemmen (man måste ta aktivt initiativ för att bli medlem och kan inte fortsätta vara det automatiskt).<br>När detta datum passerats blir medlemmen grå i tabellen och anses inte vara "aktiv" längre. Datumet kan självklart redigeras av styrelsemedlemmar.</em></small>
				</p>

				<input type="submit" value="Spara" class="button">
			</form>
		<?php endif; ?>

	</article>
	<?php endwhile; endif; ?>

<?php get_footer(); ?>