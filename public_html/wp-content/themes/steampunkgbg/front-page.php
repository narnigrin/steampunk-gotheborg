<?php 
	global $post;
	get_header(); 
?>
	
	<?php if(have_posts()) : while(have_posts()) : the_post(); ?>
	<article id="main-post-content">
		<h1><?php the_title(); ?></h1>
		<?php the_content(); ?>
	</article>
	<?php endwhile; endif; ?>

	<?php 
		$latest = new WP_Query('cat=-10&post_type=post&posts_per_page=5');
		if($latest->have_posts()) : while($latest->have_posts()) : $latest->the_post();
	?>
		<article class="list-post<?php echo (isset($notfirst) ? ' notfirst"' : '" id="list-post-first"'); ?>>
			<h2 class="title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
			<?php 
				if(isset($notfirst)){
					the_excerpt();
				}
				else {
					if(has_post_thumbnail())
						the_post_thumbnail('post_featured');

					the_content(); 
				}
			?>
			<p class="postmeta foot"><?php printPostMeta(true); ?></p>
		</article>
	<?php 
		$notfirst=true; 
		endwhile; 
	?>
		<p style="border-top:1px solid black; padding-top:1ex"><a href="arkiv">Nyhetsarkiv &raquo;</a></p>
	<?php
		endif; 
		wp_reset_postdata(); 
	?>
			
<?php get_footer(); ?>