<?php 
	global $post;
	get_header(); 

	$requestAddrSafe = strip_tags($_SERVER['REQUEST_URI']);
	$requestAddrClean = htmlentities($_SERVER['REQUEST_URI']);
	$refererAddrClean = htmlentities(@$_SERVER['HTTP_REFERER']);
	$adminemail = get_option('admin_email');
	$website = site_url();
	$websiteNoHttp = str_replace("http://", "", $website);

	//Skicka ett mail till admin
	$failuremess = "En sajtbesökare försökte gå till sidan\r\n" . $website . $requestAddrClean 
				 . "\r\noch fick ett 404-fel (sidan hittades inte).\r\n\r\n"
				 . "Hela " . '$_SERVER' . "-variabeln:\r\n"
				 . print_r($_SERVER, true);
	mail(
		$adminemail, 
		"Död länk till " . $requestAddrClean . " på steampunkgbg.se", 
		$failuremess, 
		"From: Steampunk Götheborg <404-noreply@" . $websiteNoHttp . ">"
	);
?>

	<article>
		<h1>404: Hellfire and Damnation!</h1>
		
		<p>Det har kommit grus i vårt maskineri! Vårt väloljade urverk har kuggat ur! <strong>Sidan du söker har gått upp i rök!</strong></p>
		<p>Sidan <span style="background-color:#f1e0b4;color:#956b42;word-break:break-all"><?php echo $website . $requestAddrClean ?></span> går alltså inte att hitta (hur mycket vi än letar). 
			<?php if($refererAddrClean == null): ?>
				Om du kom hit genom att manuellt skriva en adress i adressfältet, kontrollera stavningen och försök igen, eller prova någon av länkarna i navigationsmenyn.
			<?php else: ?>
				Enligt våra uppgifter kom du hit via en länk på <a href="<?php echo $_SERVER['HTTP_REFERER'] ?>"><?php echo $refererAddrClean ?></a>.
				<?php if(strpos($_SERVER['HTTP_REFERER'], $website) === 0) : ?>
					Jag, uh &hellip;</p>
					<p>Öh &hellip; *host*<br>
					<strong>Säg efter mig: <em>Jag &hellip; har &hellip; inte &hellip; sett &hellip; något.</em></strong><br>
					Ahem.</p>
					<p>Nå, det där var pinsamt. Om det hjälper mot skammen eller traumat från upplevelsen, kan du <a href="<?php echo $website ?>/kontakta-oss">höra av dig till oss</a> och klaga.
				<?php else: ?>
					Känner du dig manad, kan du alltid hälsa deras webbmaster att en av deras länkar har dött.
				<?php endif; ?>
			<?php endif; ?>
		
			Du kan också gå till <a href="<?php echo $website ?>">startsidan</a> och klicka dig vidare därifrån. 
			Under tiden har ett mail med teknisk info automatiskt skickats till sajtadministratören.</p>
		
		<p>Vi <strong>ång</strong>rar djupt att du tvingades se detta. Här är en rolig video som plåster på såren.</p>
		<iframe width="560" height="315" src="http://www.youtube-nocookie.com/embed/TFCuE5rHbPA?rel=0" frameborder="0" allowfullscreen></iframe>
	</article>
			
<?php get_footer(); ?>