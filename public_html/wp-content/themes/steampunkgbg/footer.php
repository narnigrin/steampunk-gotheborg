			</section>

			<div class="sidebar">
				<?php if(!is_user_logged_in()): ?>
				<aside class="sidebar-widget" id="bli-medlem-widget">
					<a href="/om-foreningen/bli-medlem" title="Bli medlem!"><img src="<?php echo THEME_URL ?>/images/blimedlem.png" alt="Bli medlem!"></a>
				</aside>
				<?php else: ?>
				<aside class="sidebar-widget" id="user-widget">
					<?php 
						$user = wp_get_current_user();
						echo get_avatar($user->ID, 44);
					?>
					<h2><?php 
						echo (!empty($user->display_name) ? $user->display_name : $user->user_login);
					?></h2>
					<p><a href="#" title="Min sida">Min sida</a> &mdash; <a class="logout" href="?action=logout" title="Logga ut">Logga ut</a></p>
				</aside>
				<?php endif; ?>

				<?php dynamic_sidebar( 'right-sidebar' ); ?>
			</div>

			<div class="clearboth"></div>
		</div>
	</div>
	
<div class="clearboth"></div>
</div><!-- /#bgdiv -->
</div><!-- /#container -->

<footer>
	<div id="footer-inner">
		<a href="<?php echo site_url() ?>"><img src="<?php echo THEME_URL ?>/images/spgbg-footer.png" alt="Steampunk Götheborg" id="spgbg-footer" /></a>
		<p>Design &amp; nörderi <a href="http://erikanilsson.eu" target="_blank">Erika "Masala" Nilsson</a> (<span class="symb">&#128325;</span> <a href="http://creativecommons.org/licenses/by-nc-sa/2.5/se/" target="_blank">by-nc-sa</a>),<br />
		förutom Helena Squat av <a href="http://moorstation.org/typoasis/designers/lloyd/" target="_blank">Paul Lloyd</a>, filtypsikoner från <a href="http://www.flaticon.com/authors/freepik" target="_blank">Freepik</a> (<span class="symb">&#128325;</span> <a href="http://creativecommons.org/licenses/by/3.0/" target="_blank">by</a>) och övriga ikoner från <a href="http://www.entypo.com/" target="_blank">Entypo</a> av Daniel Bruce (<span class="symb">&#128325;</span> <a href="http://creativecommons.org/licenses/by-sa/3.0/" target="_blank">by-sa</a>).<br />
		</p>
	</div>
</footer>

<div id="login-popup" class="common-popup">
	<div id="login-popup-inner" class="common-popup-inner">
		<?php
			$loginpost = get_page_by_path('logga-in');
			echo apply_filters('the_content', $loginpost->post_content);

			echo '<div style="font-size:0.8em; margin-top:2.5em;">' . SPGBG_COOKIE_INFO . '</div>';
		?>

		<a class="close-button" href="#" title="Stäng">&#10060;</a>
	</div>
</div>

<script type="text/javascript">
	//The purpose of this script: Compare computed height of #container to a given number; if not exceeded, hide outer footer backgrounds.
	var height = Math.max(document.getElementById("container").offsetHeight, document.getElementById("container").scrollHeight, document.getElementById("container").clientHeight);
	var bgheight = 0.5*383+888+85; // 383=height of footbg-inner; 888=height of topbg; 85=topbg offset from top. This is the height at which footbg-outer starts to make sense.
	
	if(height<bgheight) {
		document.getElementById("bgdiv").style.backgroundImage="none";
	}
	
	<?php if (isset($page['scripts'])) { echo $page['scripts']; } ?>
</script>

<?php wp_footer(); ?>
</body>
</html>