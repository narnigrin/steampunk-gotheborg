<?php
/*
	Template name: Användarprofil
*/

	restricted_page_check();
	$user = wp_get_current_user();

	if(class_exists('Members')){
		global $Members;
		$member = $Members->getMemberWhere('uid', $user->ID);
		$member = is_array( $member ) ? $member[0] : $member;
	}
	else {
		die("Denna sida fungerar bara när pluginet Steampunk Götheborg Members är aktiverat.");
	}
	
	// Handle POST data - save form
	if(isset($_POST['email'])){
		$err = '';
		$success = true;

		// Other fields
		if(!empty($member) && isset($_POST['firstname']) && isset($_POST['surname'])){
			$member_fields = array(
				'firstname' => wp_kses( $_POST['firstname'], array() ),
				'surname' => wp_kses( $_POST['surname'], array() ),
				'email' => sanitize_email( $_POST['email'] )
			);

			if(isset($_POST['phone']))
				$member_fields['phone'] = wp_kses($_POST['phone'], array());

			if(false === $Members->Admin->update( $member->mid, $member_fields )){
				$success = false;
			}
		}
		elseif(empty($member)){
			if(is_wp_error( wp_update_user( array('ID' => $user->ID, 'user_email' => sanitize_email( $_POST['email'] )) ) )) {
				$success = false;
			}
		}

		// Update display name
		if(isset($_POST['display_name']) && is_wp_error( wp_update_user( array('ID' => $user->ID, 'display_name' => wp_kses( $_POST['display_name'], array() )) ) )) {
			$success = false;
		}

		// Password
		if(!empty($_POST['password']) && !empty($_POST['old_password']) && wp_check_password($_POST['old_password'], $user->user_pass, $user->ID)){
			if(is_wp_error( wp_update_user( array('ID' => $user->ID, 'user_pass' => $_POST['password']) ) )) {
				$err = "<strong>Obs!</strong> Ditt lösenord har <em>inte</em> ändrats.";
				$success = false;
			}
			else {
				wp_mail(
					$user->user_email, 
					'Ändrat lösenord på steampunkgbg.se', 
					"Hej {$user->display_name}!\n\nLösenordet för ditt användarkonto har nyss ändrats på steampunkgbg.se.\nOm det inte var du som ändrade det kan du svara på det här mailet för att kontakta styrelsen, så kan de hjälpa dig nollställa ditt lösenord. Du kan därefter ställa in ett nytt lösenord.\nOm det var du som ändrade lösenordet, kan du bortse från detta meddelande.\n\nMvh,\nSteampunk Götheborg",
					"From:info@steampunkgbg.se\n"
				);
			}
		}
		elseif(!empty($_POST['password'])){
			$err = "Du måste fylla i ditt nuvarande (gamla) lösenord för att kunna spara ett nytt. Det gamla lösenordet var inkorrekt eller saknades, så ditt lösenord har inte ändrats.";
		}

		// Gather the results and give feedback
		if($success === false){
			$err = "Ett fel inträffade och ett eller flera fält sparades inte. (Du kan prova igen.)<br>" . $err;
		}

		if(!empty($err)){
			define('SPGBG_ERRORMESSAGE', $err);
		}
		else {
			define('SPGBG_SUCCESSMESSAGE', "Ändringarna är sparade.");
		}

		// Reload $user and $member to get the new data!
		$user = get_userdata($user->ID);

		if(class_exists('Members')){
			$member = $Members->getMemberWhere('uid', $user->ID);
			$member = is_array( $member ) ? $member[0] : $member;
		}
		else {
			die("Denna sida fungerar bara när pluginet Steampunk Götheborg Members är aktiverat.");
		}
	}


	global $post;
	get_header();
?>

<style type="text/css">
	#user-profile-form {
		position: relative;
	}

	#user-profile-form .avatar {
		width: 215px;
		height: auto;
		max-width: 100%;
		margin: 0 auto 1em;
	}

	fieldset.blocky-checkboxes {
		width: 100%;
		margin-bottom: 2em;
	}

	#user-profile-form h2 {
		margin-bottom:1ex;
	}

	@media screen and (min-width: 560px){
		#user-profile-form .avatar {
			float: left;
			margin-right: 1.5ex;
		}

		fieldset.blocky-checkboxes {
			margin-bottom: 1.2em;
			padding-bottom: 0;
			border-bottom: 0 none;
		}

		#user-profile-form h2 {
			display: block;
			clear:both;
		}
	}
</style>

<article>
	<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
		<?php the_content(); ?>
	<?php endwhile; endif; ?>

	<form method="POST" id="user-profile-form" action="<?php echo $_SERVER['PHP_SELF'] . '?' . $_SERVER['QUERY_STRING'] ?>">
		<?php echo get_avatar( $user->ID, 256 ); ?>
		<p style="text-align:left">
			<?php 
				echo "<strong>" . (! empty( $user->display_name ) ? $user->display_name : $user->nicename) . "</strong>";

				if ( isset( $member ) && ! empty( $member->membersince ) ) {
					echo '<br>Medlem sedan den ' . date( 'j/n&\nb\sp;Y', strtotime( $member->membersince ) );
				}
				if ( isset( $member ) && ! empty( $member->paiduntil ) ) {
					$soon = strtotime( "+1 month" ) > strtotime( $member->paiduntil );
					echo ($soon ? '<span style="color:#CC2230">' : '') . '<br>Medlem&shy;skapet löper ut den ' . date( 'j/n&\nb\sp;Y', strtotime( $member->paiduntil ) ) . ($soon ? '</span>' : '');
				}
			?>
		</p>
		<p style="text-align:left; color:#999"><em>Bilden hämtas från <a href="http://gravatar.com" target="_blank">Gravatar.com</a> och kan ändras&nbsp;där.</em></p>


		<?php
			$names = array( 
				$user->user_email,
				explode("@", $user->user_email)[0],
			);
			if ( isset( $member ) && ! empty( $member->firstname ) && ! empty( $member->surname ) ){
				$names[] = "{$member->firstname} {$member->surname}";
				$names[] = "{$member->surname} {$member->firstname}";
			}
			elseif ( ! empty( $user->display_name ) ){
				$names[] = $user->display_name;
			}
		?>
		<fieldset class="blocky-checkboxes">
			<legend>Visningsnamn</legend>
			<div class="blocky-checkboxes-inner">
				<?php $i=0; foreach ( $names as $name ) : ?>
					<label for="display_name_<?php echo $i ?>" class="<?php echo ( $name == $user->display_name ? 'active' : '' ); ?>">
						<?php echo $name ?>
						<input type="radio" name="display_name" value="<?php echo $name ?>" id="display_name_<?php echo $i ?>" <?php echo ( $name == $user->display_name ? 'checked' : '' ); ?>>
					</label>
				<?php $i++; endforeach; ?>
			</div>
		</fieldset>


		<h2>Medlemsuppgifter</h2>

		<?php if(!empty($member)) : ?>
			<label for="firstname" style="float:left;width:50%">
				Förnamn
				<input required type="text" name="firstname" id="firstname" style="width:95%" placeholder="Förnamn" value="<?php echo (! empty( $member->firstname ) ? $member->firstname : ''); ?>">
			</label>
			<label for="surname" style="float:left;width:50%">
				Efternamn
				<input required type="text" name="surname" id="surname" style="width:100%" placeholder="Efternamn" value="<?php echo (! empty( $member->surname ) ? $member->surname : ''); ?>">
			</label>

			<label for="phone">
				Telefonnummer
				<input type="text" name="phone" id="phone" style="width:100%" placeholder="Telefon" value="<?php echo (! empty( $member->phone ) ? $member->phone : ''); ?>">
			</label>
		<?php endif; ?>

		<label for="email" style="width:auto;display:block">
			E-post
			<input required type="email" name="email" id="email" placeholder="E-post" style="width:100%" value="<?php echo $user->user_email ?>">
		</label><br>

		<label for="password" style="display:inline-block;vertical-align:bottom;width:50%">
			Byt lösenord
			<input type="password" name="password" id="password" placeholder="Nytt lösenord" style="width:95%">
		</label><!--
		--><label for="old_password" style="display:inline-block;vertical-align:bottom;width:50%;font-weight:normal">
			Nuvarande lösenord (identifikation)
			<input type="password" name="old_password" id="old_password" placeholder="Nuvarande lösenord" style="width:100%">
		</label><br>

		<div id="confirmpassword" style="display:none">
			<p style="margin: 1em 0 1ex; color:rgb(0,100,100)"><span class="symb">&#9888;</span> Du är på väg att ändra ditt lösenord. Du kommer att få ett bekräftelsemail om ändringen slår igenom. Nästa gång du loggar in måste du använda det nya lösenordet.</p>
		</div>

		<button class="button" id="submit-button" style="clear:both"><span class="symb">&#10003;</span> <span class="submit-text">Spara ändringar</span></button>
		&nbsp;&nbsp;<button class="button trans" style="display:none" id="cancel-button"><span class="symb">&#10060;</span> Avbryt</button>

	</form>
</article>

<script type="text/javascript">
	(function($){
		$('#submit-button').on('click', function(ev) {
			ev.preventDefault();

			if($('#password').val() != '' && $('#old_password').val() != '' && $(this).data('submit') != 'true'){
				$('#confirmpassword').slideDown();
				$('#cancel-button').fadeIn();
				$('#submit-button').data('submit', 'true').find('.submit-text').text('OK, spara');
				return;
			}
			else {
				$('#user-profile-form').submit();
			}
		});

		$('#cancel-button').on('click', function(ev){
			ev.preventDefault();
			
			$('#confirmpassword').slideUp();
			$('#cancel-button').fadeOut();
			$('#submit-button').data('submit', '').find('.submit-text').text('Spara ändringar');
		});
	})(jQuery);
</script>

<?php get_footer(); ?>