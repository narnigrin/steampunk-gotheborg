<?php 
	global $post;
	get_header(); 

	$requestAddrSafe = strip_tags($_SERVER['REQUEST_URI']);
	$requestAddrClean = htmlentities($_SERVER['REQUEST_URI']);
	$website = site_url();

?>

	<article>
		<h1>403: You need a bigger lockpick.</h1>

		<img src="<?php echo THEME_URL ?>/images/yxtant.jpg" alt="Utelåst?" style="max-width:100%; display:block; height:auto"><br>

		<p>Du har inte tillåtelse att visa sidan eller filen <span style="background-color:#f1e0b4;color:#956b42;word-break:break-all;"><?php echo $website . $requestAddrClean ?></span>.
			Om du försökte nå ett protokoll eller dokument via en direktlänk, testa att gå till <a href="<?php echo $website ?>/om-foreningen/protokoll-och-dokument/">Protokoll och dokument-sidan</a>
			och klicka dig vidare därifrån istället. Kan du inte se dokumentet i listan, kan det vara begränsat till föreningsmedlemmar. Du behöver i så fall logga in.</p>

		<p>Vi ber om ursäkt för besväret och hoppas att du hittar det du söker.</p>

		<p style="opacity:0.5">
		  	(<a href="https://www.flickr.com/photos/adulau/16879431601/" target="_blank" title="Bilden på Flickr">Bild:</a> 
		  	<a href="https://www.flickr.com/photos/adulau/" target="_blank">Alexandre Dulanoy</a>, 
		  	<a href="https://creativecommons.org/licenses/by-sa/2.0/" target="_blank" title="Creative Commons-licens">CC&nbsp;by-sa</a>)
    	</p>
	</article>
			
<?php get_footer(); ?>