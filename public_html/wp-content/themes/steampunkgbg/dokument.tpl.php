<?php
/*
	Template name: Lista dokument
*/

	global $post;
	get_header(); 
?>
			
	<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
	<article>
		<h1><?php the_title(); ?></h1>
		<?php if(has_post_thumbnail()): ?><p><?php the_post_thumbnail('post_featured'); ?></p><?php endif; ?>
		<?php the_content(); ?>

		<?php
			// See inc/documents.conf.php
			$document_categories = hierarchical_category_tree(0);
			hierarchical_print_documents($document_categories, 2);
		?>
	</article>
	<?php endwhile; endif; ?>
			
<?php get_footer(); ?>