<?php
/*
	Template name: Visa Googlekalender
*/

	$bodyclass = 'widepage';

	global $post;
	get_header(); 
?>
			
			<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
			<article>
				<ul id="calendar-links">
					<li><a href="https://www.google.com/calendar/feeds/steampunkgbg%40gmail.com/public/basic" id="xml" title="Prenumerera på kalendern i Atom-format"><span class="symb">&#59194;</span> Prenumerera</a></li>
					<li><a href="https://www.google.com/calendar/ical/steampunkgbg%40gmail.com/public/basic.ics" id="ical" title="Ladda ned kalendern i iCal-format"><span class="symb">&#128190;</span> Spara</a></li>
					<li><a href="https://www.google.com/calendar/embed?src=steampunkgbg%40gmail.com&ctz=Europe/Stockholm" id="html" title="Läs kalendern i Google Calendar"><span class="symb">&#128197;</span> Läs original</a></li>
				</ul>
				<div class="clearboth calendar-clear">&nbsp;</div>

				<a id="btn_prev" class="disabled"><span class="symb">&#59225;</span></a>
				<a id="btn_next" class="disabled"><span class="symb">&#59226;</span></a>
				<div id="calendar-wrapper"><em>Laddar kalendern ...</em></div>
			</article>
			<?php endwhile; endif; ?>
			
<?php get_footer(); ?>