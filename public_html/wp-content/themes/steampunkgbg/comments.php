<section id="comments-section">
<h2 id="kommentarer"><?php comments_number( '', '<span class="symb">&#59168;</span> En kommentar', '<span class="symb">&#59168;</span> % kommentarer' ); ?></h2>

<ol class="commentlist">
<?php
	// Comment list
	wp_list_comments(array(
		'reverse_top_level' => true,
		'reply_text' => 'Svara',
		'type' => 'all',
		'style' => 'ol',
		'avatar_size' => 40,
	));
?>
</ol>
</section>

<?php
	// Comment form below

	$user = wp_get_current_user();
	$user_identity = (!empty($user->display_name) ? $user->display_name : $user->user_login);
	$commenter = wp_get_current_commenter();
	$req = get_option( 'require_name_email' );
	$aria_req = ( $req ? " aria-required='true' required" : '' );

	if(user_can($user->ID, 'list_users'))
		$user_group = 'administratör';
	elseif(user_can($user->ID, 'publish_posts'))
		$user_group = 'styrelsemedlem';
	elseif(user_can($user->ID, 'post_to_club'))
		$user_group = 'medlem';
	else
		$user_group = '';


	$comment_fields = array(
		'author' =>
			'<p><label for="author">Namn ' .
			( $req ? '<span class="required">*</span>' : '' ) .
			'<input placeholder="Namn" style="width:98%" id="author" name="author" type="text" value="' . esc_attr( $commenter['comment_author'] ) .
			'" size="30"' . $aria_req . ' /></label>',
		'email' =>
			'<label for="email">E-post ' .
			( $req ? '<span class="required">*</span>' : '' ) .
			'<input placeholder="namn@adress.com" style="width:99%" id="email" name="email" type="text" value="' . esc_attr(  $commenter['comment_author_email'] ) .
			'" size="30"' . $aria_req . ' /><em style="font-weight:normal">Din e-post publiceras inte.</em></label>',
		'url' =>
			'<label for="url">URL ' .
			'<input placeholder="http://" style="width:99%" id="url" name="url" type="text" value="' . esc_attr( $commenter['comment_author_url'] ) .
			'" size="30" /></label></p>',
	);

	$comment_form_args = array(
		'comment_field' => 
			'<label for="comment">Kommentar <a style="float:right; margin-right:2%; position:relative" href="javascript:void(0)" class="allowed-tags" title="Visa tillåtna HTML-taggar och -attribut">HTML ' .
			'<code>' . allowed_tags() . '</code></a>' .
			'<textarea placeholder="Skriv här" style="width:98%" id="comment" name="comment" cols="45" rows="8" aria-required="true" required></textarea></label>',
		'must_log_in' => 
			'<p class="must-log-in">' . 
			sprintf( 'Du måste vara <a href="%s">inloggad</a> för att kommentera.', wp_login_url( apply_filters( 'the_permalink', get_permalink( ) ) ) ) . 
			'</p>',
		'logged_in_as' => 
			'<p class="logged-in-as">' . 
			sprintf( 'Du kommenterar som <a href="%1$s">%2$s</a> (%3$s). <a href="%4$s" title="Log out of this account">Logga ut?</a>', admin_url( 'profile.php' ), $user_identity, $user_group, wp_logout_url( apply_filters( 'the_permalink', get_permalink( ) ) ) ) . 
			'</p>',
		'comment_notes_before' => 
			'',
		'comment_notes_after' =>
			'',
		'title_reply' => 
			'Delta i samtalet',
		'title_reply_to' => 
			'Svara %s',
		'label_submit' => 
			'Skicka kommentar',
		'fields' => 
			apply_filters( 'comment_form_default_fields', $comment_fields )
	);

	comment_form($comment_form_args);
?>