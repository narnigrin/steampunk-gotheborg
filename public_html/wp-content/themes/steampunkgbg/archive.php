<?php 
	global $post;
	get_header(); 
?>

	<h1 style="margin-bottom:1em"><?php
		$months = array(
			1 => 'januari',
			'februari',
			'mars',
			'april',
			'maj',
			'juni',
			'juli',
			'augusti',
			'september',
			'oktober',
			'november',
			'december'
		);

		if(is_category()):
			single_cat_title("Kategoriarkiv: ");
		elseif(is_tag()):
			single_tag_title("Taggarkiv: ");
		elseif(is_author()):
			$author = get_userdata(get_query_var('author'));
			echo "Nyheter skrivna av " . $author->display_name;
		elseif(is_date()):
			echo "Poster från ";
			if(is_day()):
				echo "den " . get_the_date('j ') . $months[get_the_date('n')] . get_the_date(' Y');
			elseif(is_month()):
				echo $months[get_the_date('n')] . get_the_date(' Y');
			elseif(is_year()):
				echo get_the_date('Y');
			endif;
		else: // Includes "is_home"; the standard blog archive
			echo "Nyhetsarkiv";
		endif;
	?></h1>

	<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
	<article class="list-post">
		<h2 class="title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
		<?php the_excerpt(); ?>

		<p class="postmeta foot"><?php printPostMeta(true); ?></p>
	</article>
	<?php 
		endwhile; 
		else:
	?>
	<article class="list-post">
		<p><em>Det finns inga poster att visa.<br>
			Vill du titta i <a href="../kalendarium">kalendern</a> om det händer något spännande?</em></p>
	</article>
	<?php endif; ?>

	<article class="pagination">
		<?php 
			$args = array(
				'prev_text'          => __('«'),
				'next_text'          => __('»'),
			);
			echo paginate_links($args); 
		?>
	</article>
			
<?php get_footer(); ?>