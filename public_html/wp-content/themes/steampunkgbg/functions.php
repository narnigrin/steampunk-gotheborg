<?php
	add_filter('show_admin_bar', '__return_false');
	add_theme_support('post-thumbnails');
	add_theme_support('html5', array('gallery'));
	remove_action( 'set_comment_cookies', 'wp_set_comment_cookies' ); // Commenters don't need cookies.

	/*add_filter( 'auth_cookie_expiration', 'keep_me_logged_in_for_1_year' );
	function keep_me_logged_in_for_1_year( $expire_in_seconds ) {
	    return 31556926;
	}*/

	/*
	 * Useful constants
	 */
	define('THEME_PATH', get_template_directory());
	define('THEME_URL', get_template_directory_uri());
	define('THEME_TEXTDOMAIN', 'wptoolkit');

	// Cookie info on login forms
	// See https://codex.wordpress.org/Customizing_the_Login_Form for action hooks
	define('SPGBG_COOKIE_INFO', '<h4>Info om cookies</h4>
		<p>Vid inloggning sparas ett antal s.k. cookies (kakor) på din dator. 
		Kakorna krävs för att inloggningen ska fungera.
		Genom att logga in samtycker du till att dessa kakor sparas och används.</p>
		<p>Mer info om kakorna <a href="https://codex.wordpress.org/WordPress_Cookies#WordPress_.3E_3.0" target="_blank">hittar du här</a>.</p>

		<style type="text/css">
			#login h1 a {
				background-image: url(' . THEME_URL . '/images/poseidon80.png);
			}
			#login { padding-top: 4%; }
		</style>');

	add_action('login_message', 'sp_cookie_info_login');
	function sp_cookie_info_login(){
		echo SPGBG_COOKIE_INFO;
	}
	
	/*
	 * Thumbnail sizes
	 */
	//add_image_size('committee_member_photo', 150, 150, true);
	add_image_size('post_featured', 570, 228, true);

	/* 
	 * Use WPToolkit (https://github.com/hanswestman/WPToolkit)
	 */
	function custom_autoload($class){
		$file = THEME_PATH. '/toolkit/' . $class . '.class.php';
		if(file_exists($file)){
			include_once($file);
		}
	}
	spl_autoload_register('custom_autoload');

	$AjaxAPI = new AjaxAPI();

	require_once(THEME_PATH . '/inc/menu-settings.conf.php');
	require_once(THEME_PATH . '/inc/calendar-settings.conf.php');
	require_once(THEME_PATH . '/inc/calendar.widget.php');
	require_once(THEME_PATH . '/inc/calendar_get.php');
	require_once(THEME_PATH . '/inc/documents.conf.php');

	/*
	 * Register menu locations
	 */
	add_action('init', 'register_menu');

	$big_top_menus = array(
		'first-menu' => 'First menu',
		'second-menu' => 'Second menu',
		'third-menu' => 'Third menu',
		'fourth-menu' => 'Fourth menu'
	);
	function register_menu() {
		global $big_top_menus;
		foreach ($big_top_menus as $id => $name){
			register_nav_menu($id, $name);
		}
	}


	/*
	 * Scripts and styles
	 */
	add_action('wp_enqueue_scripts', 'spgbg_enqueue_scripts', 9);
	function spgbg_enqueue_scripts() {
		wp_register_script('jquery', 'http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js', array(), '1.11.0', TRUE);
		wp_register_script('unslider', THEME_URL . '/js/unslider/dist/js/unslider-min.js', array('jquery'), NULL, TRUE);
		wp_register_script('_script', THEME_URL . '/js/script.js', array('jquery', 'unslider'), '1.2', TRUE);
		wp_enqueue_script('_script');

		wp_register_script('kalender', THEME_URL . '/js/kalender.js', array('jquery'), NULL, TRUE);
		wp_register_style('kalender_styles', THEME_URL . '/css/kalender.css', null, null, 'screen');
		
		wp_register_style('members_styles', THEME_URL . '/css/members.css', null, null, 'screen');
		wp_register_script('memberslist', THEME_URL . "/js/memberslist.js", array('jquery'), '0.9.1', TRUE);

		$custom = array(
			'template_url' => THEME_URL,
			'ajax_path' => admin_url( 'admin-ajax.php' ),
		);
		wp_localize_script('_script', 'custom', $custom);

		global $post;

		if($post !== null){
			$tpl = get_page_template_slug($post->ID);
			if($tpl == "kalender.tpl.php") {
				wp_enqueue_script('kalender');
				wp_enqueue_style('kalender_styles');
			}
			elseif($tpl == "members.tpl.php") {
				wp_enqueue_style('members_styles');
				wp_enqueue_script('memberslist');
			}
			elseif($tpl == "signup.tpl.php"){
				wp_enqueue_script('memberslist');
			}
		}
	}

	add_action('admin_enqueue_scripts', 'spgbg_enqueue_admin_scripts');
	function spgbg_enqueue_admin_scripts() {
		wp_register_script('menu_admin', THEME_URL . '/js/menu_admin.js', array('jquery'), NULL, FALSE);
		wp_register_script('document_admin', THEME_URL . '/js/document_admin.js', array('jquery'), '1.1', FALSE);
		
		$screen = get_current_screen();
		if($screen->id == 'appearance_page_spgbg-theme-settings'){ // Our custom settings page (it's under Appearance)
			wp_enqueue_media();
			wp_enqueue_script('menu_admin');
		}
		elseif($screen->id == 'document'){ // New/edit document pages
			wp_enqueue_media();
			wp_enqueue_script('document_admin');
		}
	}

	/*
	 * REMOVE fuckin' emoji scripts!!
	 */
	remove_action('wp_head', 'print_emoji_detection_script', 7);
	remove_action('admin_print_scripts', 'print_emoji_detection_script');
	remove_action('wp_print_styles', 'print_emoji_styles');
	remove_action('admin_print_styles', 'print_emoji_styles');


	/*
	 * Sidebar(s)
	 */
	add_action( 'widgets_init', 'spgbg_sidebar' );
	function spgbg_sidebar() {
		register_sidebar( array(
			'name'          => 'Right sidebar',
			'id'            => 'right-sidebar',
			'before_widget' => '<aside class="sidebar-widget %2$s" id="%1$s">',
			'after_widget'  => '</aside>',
			'before_title'  => '<h2>',
			'after_title'   => '</h2>'
		) );
	}


	/*
	 * Helpers/template tags
	 */
	function getImageUrlFromImageId($image_id, $size = 'full') {
		$image = wp_get_attachment_image_src($image_id, $size);
		return empty($image[0]) ? '' : $image[0];
	}

	function getAttachmentFileNameFromId($attachment_id){
		$att_url = wp_get_attachment_url($attachment_id);
		$filename_parts = explode('/', $att_url);
		return end($filename_parts);
	}

	function getMenuNameByLocationId($id){
		$locs = get_nav_menu_locations();
		
		$menu_obj = get_term( $locs[$id], 'nav_menu' );
		if($menu_obj != NULL && isset($menu_obj->name)){
			return $menu_obj->name;
		}
		else {
			return false;
		}

	}

	function printTopMenu($id) {
		$this_menu_bg = get_option('spgbg_' . $id . '_menu_bg');
		$this_menu_bgpos = get_option('spgbg_' . $id . '_menu_position');
		$menu_name = getMenuNameByLocationId($id);

		?>
		<li class="top-menu-box" style="<?php if($this_menu_bg): ?>background-image:url('<?php echo $this_menu_bg ?>');<?php endif; if($this_menu_bgpos): ?> background-position:<?php echo $this_menu_bgpos; endif; ?>">
			<a href="#submenu-<?php echo $id ?>" class="top-menu-title"><span><?php echo $menu_name ?></span></a>
		<?php
			$count = wp_nav_menu( array(
				'theme_location' => $id,
				'echo' => 0,
			));
			$count = substr_count($count,'class="menu-item ');

			wp_nav_menu( array(
				'theme_location' => $id,
				'container' => false,
				'menu_class' => 'top-sub-menu',
				'items_wrap' => '<ul id="submenu-' . $id . '" class="%2$s" data-items-number="' . $count . '" data-top-menu-title="' . $menu_name . '">%3$s</ul>',
			));
		?>
		</li>
		<?php
	}

	function printBreadcrumb($post) {
		if(is_front_page()): ?>
			<a class="current" href="<?php echo site_url() ?>">Hem</a>
		<?php else: ?>
			<a href="<?php echo site_url() ?>">Hem</a> &raquo;

			<?php if(is_single() && $post->post_type=='post'): ?>
					<a href="<?php echo get_permalink( get_option( 'page_for_posts' ) ) ?>">Nyheter</a> &raquo;
					<a href="<?php echo get_permalink($post->ID) ?>" class="current"><?php echo get_the_title($post->ID) ?></a>
			<?php elseif(is_single() && $post->post_type==''): /* Something else */ ?>
					<!-- Link to appropriate archive here. Still TODO -->
					<a href="<?php echo get_permalink( get_option( 'page_for_posts' ) ) ?>">Nyheter</a> &raquo;
					<a href="<?php echo get_permalink($post->ID) ?>" class="current"><?php echo get_the_title($post->ID) ?></a>
			<?php elseif(is_archive()): ?>
					<a href="<?php echo $_SERVER['REQUEST_URI']; ?>" class="current">Arkiv &ndash; <?php wp_title('') ?></a>
			<?php elseif(is_home()): ?>
					<a href="<?php echo $_SERVER['REQUEST_URI']; ?>" class="current">Nyheter</a>
			<?php elseif(is_page()): 
					$ancestors = get_post_ancestors($post);
					if(is_array($ancestors)):
						$ancestors = array_reverse($ancestors);
						foreach ($ancestors as $ancestor):
					?>
					<a href="<?php echo get_permalink($ancestor) ?>"><?php echo get_the_title($ancestor) ?></a> &raquo;
					<?php endforeach; endif; ?>
					<a href="<?php echo get_permalink($post->ID) ?>" class="current"><?php echo get_the_title($post->ID) ?></a>
			<?php elseif(is_404()): ?>
					<a class="current">404 (sidan hittades inte)</a>

		<?php endif; endif;
	}

	/**
	 * Checks for login attempt before user is authenticated. Auth happens between after_setup_theme 
	 * and init somewhere, and after_setup_theme is in fact the earliest hook we can reach from here.
	 */
	add_action('after_setup_theme', 'checkAuthAction');
	function checkAuthAction(){
		if(isset($_GET['action'])){
			switch($_GET['action']){
				case 'logout':
					wp_logout();
					
					$redir = (isset($_GET['redir']) ? $_GET['redir'] : site_url());
					wp_redirect($redir);
					exit;

					break;
				case 'login':
					if(isset($_POST['log']) && isset($_POST['pwd']) && isset($_POST['rememberme'])){
						$result = wp_signon();

						if(is_wp_error($result)){
							//print_r($result);
							//print_r($result->get_error_code());
							//print_r($result->get_error_message());

							define('SPGBG_ERRORMESSAGE', $result->get_error_message());
						}
						else {
							$redir = (isset($_GET['redir']) ? $_GET['redir'] : site_url());
							wp_redirect($redir);
							exit;
						}
					}
					else {
						// Don't do anything. The $_GET variable might just be a mistake
					}
					break;
			}
		}
	}

	// Use within The Loop ONLY. Requires post data!
	function printPostMeta($comments = false){ 
		$year = get_the_time('Y');
		$month = get_the_time('m');
		$day = get_the_time('d');

		?><span><span class="symb">&#128197;</span> <?php 
			$dateunit = '<a href="' . site_url() . '/%s" title="Datumarkiv">%s</a>';

			echo sprintf($dateunit, $year, $year) . '-' . 
				 sprintf($dateunit, $year.'/'.$month, $month) . '-' . 
				 sprintf($dateunit, $year.'/'.$month.'/'.$day, $day); 
		?></span> | 
		<?php the_category(', '); ?> | 
		<span><span class="symb">&#128100;</span> <a href="mailto:<?php the_author_meta('user_email'); ?>"><?php the_author(); ?></a></span> 
		<?php 
			if($comments){
				echo '<span>';
				comments_number( '', '| <span class="symb">&#59160;</span> <a href="' . get_the_permalink() . '#kommentarer">1</a>', '| <span class="symb">&#59168;</span> <a href="' . get_the_permalink() . '#kommentarer">%</a>' );
				echo '</span>';
			}
	}


	// Fix comment classes - add classes for user groups
	add_filter( 'comment_class' , 'comment_class_user_group', 10, 4 );
	function comment_class_user_group( $classes, $class, $comment_id, $post_id ) {
	    $comment = get_comment( $comment_id );

	    // Stop studying the user if there is none
	    if ( $comment->user_id == 0 ) {
		    return $classes;
		}

	    if ( user_can( $comment->user_id, 'list_users' ) ) {
	        $classes[] = 'byadmin';
	    } 
	    if ( user_can( $comment->user_id, 'publish_posts' ) ) {
	        $classes[] = 'byeditor';
	    } 
	    if ( user_can( $comment->user_id, 'post_to_club' ) ) {
	        $classes[] = 'bymember';
	    }

	    return $classes;
	}


	/* Begin code for custom Error 403 - Forbidden */
	function custom_error_pages( ) {
	   global $wp_query;

	   if ( isset( $_REQUEST[ 'status' ] ) && $_REQUEST[ 'status' ] == 403 ) {
	      $wp_query->is_404 = FALSE;
	      //$wp_query->is_page = TRUE;
	      //$wp_query->is_singular = TRUE;
	      $wp_query->is_page = FALSE;
	      $wp_query->is_singular = FALSE;
	      $wp_query->is_single = FALSE;
	      $wp_query->is_home = FALSE;
	      $wp_query->is_archive = FALSE;
	      $wp_query->is_category = FALSE;
	      add_filter( 'wp_title', 'custom_error_title', 65000, 2 );
	      //add_filter( 'body_class', 'custom_error_class' );
	      status_header( 403 );
	      get_template_part( '403' );
	      exit;
	   }
	}

	function custom_error_title( $title='', $sep='' ) {
	   if ( isset( $_REQUEST[ 'status' ] ) && $_REQUEST[ 'status' ] == 403 ) {
	      return "403 Forbidden ".$sep." ";
	   }
	}

	add_action( 'wp', 'custom_error_pages' );
	/* End code for custom Error 403 - Forbidden */


	// ###########################################
	// ### Add a setting to Settings - General ###
	// ###########################################

	add_action( 'admin_init', 'reg_setting_committee_email' );
	function reg_setting_committee_email() {
	    register_setting(
	        'general',
	        'sp_committee_email',
	        'trim'
	    );
	    add_settings_field(
	        'sp_committee_email',
	        'Committee e-mail',
	        'sp_committee_email_field',
	        'general',
	        'default',
	        array ( 'label_for' => 'sp_committee_email' )
	    );
	}
	function sp_committee_email_field($args) {
	    $data = esc_attr( get_option( 'sp_committee_email', '' ) );

	    printf(
	        '<input type="text" name="sp_committee_email" value="%1$s" id="%2$s" />',
	        $data,
	        $args['label_for']
	    );
	}
?>