<?php
/*
	Template name: Registrera medlemsinlogg
*/

	global $post;
	get_header();
?>

<style type="text/css">
	#register-form label {
		display: inline-block;
		*display: inline;
		zoom: 1;
		width: 49%;
		font-weight: bold;
	}
	#register-step-2 label {
		max-width: 200px;
		display: block;
		float: left;
		margin-right: 0.5em;
		width: 50%;
	}
	@media screen and (max-width:350px){
		#register-step-2 label {
			width: 60%;
		}
	}

	#register-form label input, #register-step-2 label input {
		display: block;
		width: 98%;
		border: 1px solid #603913;
		padding: 0.5em;
	}

	#register-form input.button {
		display: inline-block;
		*display: inline;
		zoom: 1;
		max-width: 49%;
		vertical-align: bottom;
	}

	#response {
		margin-top: 0.5em;
		color: #080;
	}
	#response .symb {
		float: left; #603913
		display: block; 
		margin: 0.1em 0.2em 0 0;
		font-size: 3em;
		color: #0c0;
	}
	#response.error {
		color: #a00;
	}
	#response.error .symb {
		color: #e00;
	}
	#response.info {
		color: #06c;
	}
	#response.info .symb {
		color: #0ad;
	}

	#suggestions {
		border: 1px solid #603913;
		height: 2.35em;
		float: left;
		margin-right: 1em;
	}
</style>

<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
	<article>
		<h1><?php the_title(); ?></h1>
		<?php if(has_post_thumbnail()): ?><p><?php the_post_thumbnail('post_featured'); ?></p><?php endif; ?>
		<?php the_content(); ?>

		<form method="POST" id="register-form">
			<label for="email">
				Din e-post:
				<input required type="email" name="email" id="email" placeholder="namn@domän.com">
			</label>

			<input type="submit" value="Registrera" class="button">
		</form>
		<div id="response"></div>
	</article>
	<?php endwhile; endif; ?>

<?php get_footer(); ?>