(function($){
	$(document).ready(function(){
		var calwrap = $('#calendar-wrapper');
		var prevBtn = $('#btn_prev');
		var nextBtn = $('#btn_next');
		$.ajaxSetup ({cache: false});
		
		function getCal(month) {
			prevBtn.addClass('disabled');
			nextBtn.addClass('disabled');

			$.ajax({
				url: window.wptoolkit.ajaxUrl,
				type: 'GET',
				data: { 
					action: 'AjaxAPI',
					command: 'ajaxLoadBigCal',
					m : month,
				}
			})
			.done(function(res) {
				if(res.status == true && res.data){
					calwrap.html(res.data);
					prevBtn.removeClass('disabled');
					nextBtn.removeClass('disabled');
				}
				else {
					// TODO: Error handling
				}
			});
		}
		
		getCal("");
		
		prevBtn.click(function(){
			var prev = $('#prevMonth').val();

			if($(window).innerWidth() >= 690)
				$('#calendar-links').slideUp();
		
			getCal(prev);
		});
		nextBtn.click(function(){
			var next = $('#nextMonth').val();

			if($(window).innerWidth() >= 690)
				$('#calendar-links').slideUp();

			getCal(next);
		});

		$('#calendar-wrapper').on('click', '#openCalLinks', function(){ // PM: binds click event handler to #openCalLinks IN THE FUTURE
			var $article = $(this).closest('article');
			var thisPos = $(this).offset().left - $article.offset().left;
			var thisWidth = $(this).outerWidth();

			$('#calendar-links').css('right', $article.innerWidth() - (thisPos + thisWidth + 10));
			$('#calendar-links').slideToggle();
		});

		$(window).resize(function(){
			if($(window).innerWidth() < 690){
				$('#calendar-links').show();
			}
		});
	});
})(jQuery);