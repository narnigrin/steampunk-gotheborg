(function($) {
	$(document).ready(function(){
		
		/**
		 * The markup for image elements
		 */
		function image_elt(img) {
			return '<img src="' + img + '" style="max-height:90px; display:block; margin-bottom:5px; margin-right:5px;">';
		}
		
		/**
		 * Show image at load if present 
		 */
		$('.spgbg_menu_bg').each(function(){
			if($(this).attr('value') != '') {
				$(this).siblings('.spgbg_image_upload').html('Change image');
				$(this).siblings('.spgbg_image_container').html(image_elt($(this).val()));
			}
			else {
				$(this).siblings('.spgbg_remove_image').hide();
			}
		});
		
		/**
		 * Media library functionality taken mostly from http://codestag.com/how-to-use-wordpress-3-5-media-uploader-in-theme-options/ 
		 */
		var _custom_media = true,
		    _orig_send_attachment = wp.media.editor.send.attachment;
		$('.spgbg_image_upload').click(function(ev) {
			ev.preventDefault();
			var button = $(this);
			
			var send_attachment_bkp = wp.media.editor.send.attachment;
			_custom_media = true;
			
			wp.media.editor.send.attachment = function(props, attachment){
				if ( _custom_media ) {
					button.siblings('.spgbg_menu_bg').attr('value', attachment.url);
					
					button.siblings('img').remove();
					button.siblings('.spgbg_image_container').html(image_elt(attachment.url));
					button.siblings('.spgbg_remove_image').show();
					button.html('Change image');
				} else {
					return _orig_send_attachment.apply( this, [props, attachment] );
				};
			}

			wp.media.editor.open(button);
		});
		$('.add_media').on('click', function(){
			_custom_media = false;
		});
		

		/** 
		 * Remove image link click event
		 */
		$('.spgbg_remove_image').click( function(ev){
			ev.preventDefault();
			$(this).siblings('.spgbg_menu_bg').attr('value', '');
			$(this).siblings('.spgbg_image_container').find('img').remove()
			$(this).siblings('.spgbg_image_upload').html('Attach image');
			$(this).hide();
		});
		
	}); /* END document ready */
})(jQuery);