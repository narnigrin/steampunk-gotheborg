(function($){
	
	$(document).ready(function(){
		/* Media library functionality taken mostly from http://codestag.com/how-to-use-wordpress-3-5-media-uploader-in-theme-options/ */
		var _custom_media = true,
		    _orig_send_attachment = wp.media.editor.send.attachment;
		$('.spgbg_addfile').click(function(ev) {
			ev.preventDefault();
			var button = $(this);
			
			var send_attachment_bkp = wp.media.editor.send.attachment;
			_custom_media = true;
			
			wp.media.editor.send.attachment = function(props, attachment){
				if ( _custom_media ) {
					$('#document_Document_settings_docfile').attr('value', attachment.url);
					$('#document_Document_settings_attachment_id').attr('value', attachment.id);
					$('#spgbg-addfile-name-display span').show().filter('.name').text(attachment.filename);
				} else {
					return _orig_send_attachment.apply( this, [props, attachment] );
				};
			}

			wp.media.editor.open(button);
		});
		$('.add_media').on('click', function(){
			_custom_media = false;
		});
		
		$('.spgbg_removefile').click( function(ev){
			ev.preventDefault();
			$('#document_Document_settings_docfile, #document_Document_settings_attachment_id').attr('value', '');
			$('#spgbg-addfile-name-display span').empty().hide();
		});


		// On load ...
		var filepath = $('#document_Document_settings_docfile').val();
		var filename = (filepath != '' ? filepath.split('/').pop() : '')
		$('#spgbg-addfile-name-display span.name').text(filename);
		if(filename != '') { $('#spgbg-addfile-name-display span').show(); }
	});
})(jQuery);