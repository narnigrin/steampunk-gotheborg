(function($) {
	$.fn.center = function() {
		this.css("position", "absolute");
		this.css("top", ($(window).height() - this.height()) / 2 + $(window).scrollTop() + "px");
		this.css("left", ($(window).width() - this.width()) / 2 + $(window).scrollLeft() + "px");
		this.css('z-index', 99999);
		return this;
	};

	// Ajax loading symbol
	function showProgress() {
		if ($('#progress').length === 0) {
			$('body').append('<div id="progress"><img src="' + custom.template_url + '/images/239.GIF" alt="Laddar..." style="background: #FFF; padding: 3px; border-radius: 50%; box-shadow: 0px 10px 40px rgba(0,0,0,0.3)" /></div>');
		}else{
			$('#progress').show();
		}
		
		$('#progress').center();
	}
	function hideProgress() {
		$('#progress').hide();
	}

	$(document).ajaxStart(function() {
		showProgress();
	}).ajaxStop(function() {
		hideProgress();
	});


	$(document).ready(function(){

		// MENU BEHAVIOUR
		// Menu and login box popups
		$('a[href="#big-main-menu"]').click(function(ev){
			$('#big-main-menu').fadeIn();
		});
		$('a[href="#login-popup"]').click(function(ev){
			$('#login-popup').fadeIn();
		});

		// Control back button behaviour for the menu
		$(window).bind('hashchange', function() {
			var hash = top.location.hash;
			if (hash == '' && $(window).innerWidth() < 640) {
				$('#big-main-menu, .top-sub-menu, .common-popup').fadeOut();
			}
			else if (hash == '') {
				$('.common-popup').fadeOut();
			}
			else if (hash == '#big-main-menu' && $(window).innerWidth() < 640) {
				$('#big-main-menu').fadeIn();
				$('.top-sub-menu').fadeOut();
			}
		});

		// More menu behaviour for mobile
		$('.top-menu-title').click(function(ev){
			if($(window).innerWidth() < 640)
				$(this).siblings('.top-sub-menu').fadeIn();
			else
				ev.preventDefault();
		});
		$('#big-main-menu').on('click', function(ev){
			if($(window).innerWidth() < 640 && $('.top-menu-box').has($(ev.target)).length < 1){
				window.location.hash = '';
			}
		});

		// Menu behaviour for desktop (mainly)
		$('.top-menu-box').hover(function(){
			if ($(window).innerWidth() >= 640) {
				var mh = $(this).find('.top-sub-menu').attr('data-items-number') * 26.5 + 120;
				mh = Math.max(mh, 130);

				$(this).find('.top-sub-menu').css('max-height', mh + 'px');
			}
		}, function(){
			if ($(window).innerWidth() >= 640) {
				$(this).find('.top-sub-menu').css('max-height', 0);
			}
		});

		$(window).on('resize', function(){
			if($(window).innerWidth() < 640)
				$('.top-sub-menu').css('max-height', 'auto');
		});



		// Show/hide the allowed tags bubble in comment form
		$('a.allowed-tags').on('click', function(){
			$(this).find('code').fadeToggle('fast');
		});



		// CALENDAR
		// Load the calendar widget content
		$.ajaxSetup ({cache: false});
		var $calwidget = $('.spgbg_cal_widget_table');
		if($calwidget.length > 0){
			$calwidget.each(function(){
				var _this = $(this),
					time_min = _this.data('time-min'),
					time_max = _this.data('time-max'),
					num_events = _this.data('num-events');
				
				$.ajax({
					url: window.wptoolkit.ajaxUrl,
					type: 'GET',
					data: { 
						action: 'AjaxAPI',
						command: 'ajaxLoadCalEvents',
						min : time_min,
						max : time_max, 
						num : num_events
					}
				})
				.done(function(res) {
					if(res.status == true && res.data){
						_this.fadeOut(100, function(){
							_this.html(res.data).fadeIn(200);
						});
					}
					else if(res.message){
						_this.fadeOut(100, function(){
							_this.html(res.message).fadeIn(200);
						});
					}
				});
			});
		}


		// DOCUMENTS
		// Accordions
		$.fn.documentAccordion = function(){
			$(this).each(function(){
				var _this = $(this),
					doclist = _this.next('.doclist');

				_this.addClass('doclist-accordion');

				if (doclist.find('li').length > 6){ // Nothing special about this number, it just seemed good
					_this.addClass('doclist-collapsed');
					doclist.addClass('doclist-collapsed');
				}
				else {
					_this.addClass('doclist-expanded');
					doclist.addClass('doclist-expanded');
				}

				_this.on('click', function(){
					_this.toggleClass('doclist-expanded doclist-collapsed');
					doclist.slideToggle('slow', function(){
						doclist.toggleClass('doclist-collapsed doclist-expanded');
					});
				});
			});
		};
		var $doclists = $('.doclist-header');
		if ($doclists.length)
			$doclists.documentAccordion();


		// GALLERIES
		// Initiate slideshows
		$.fn.initSlideshow = function(){
			var _this = $(this),
				_figures = _this.find('figure'),
				numberSlides = _figures.length;

			_this.on('unslider.ready', function(){
				_this.addClass('.sg-slideshow-initiated');
			});

			_this.wrapInner('<div class="sg-slideshow-wrapper"></div>');
			_figures.each(function(index){
				var captionNumbering = '<strong><em>' + (index+1) + '/' + numberSlides + ':</em></strong> ';

				if($(this).find('figcaption').length)
					$(this).find('figcaption').prepend(captionNumbering);
				else
					$(this).append('<figcaption>' + captionNumbering.replace(':', '') + '</figcaption>');
			});

			// Had to hack the max-heights for the images to actually accept it. Damn CSS.
			function adjustImageMaxHeights(){
				_figures.find('.gallery-icon img').each(function(){
					$(this).css({
						maxHeight: $(this).closest('div.gallery-icon').height() + 'px',
					});
				});
			}
			adjustImageMaxHeights();
			$(window).on('resize', function(){
				adjustImageMaxHeights();
			});

			_this.unslider({
				autoplay:  false,
				arrows:    {
					prev:      '<a class="unslider-arrow prev" title="Föregående bild"></a>',
					next:      '<a class="unslider-arrow next" title="Nästa bild"></a>',
				},
				animation: 'horizontal',
				selectors: {
					container: '.sg-slideshow-wrapper',
					slides:    'figure',
				},
				infinite:  true,
				speed:     500,
			});
		};
		var slideshows = $('.gallery.gallery-sg-slideshow');
		if(slideshows.length)
			slideshows.initSlideshow();

		// Gallery image zoom - for all galleries
		$.fn.imageZoom = function(){
			$(this).each(function(){
				var _this = $(this),
					imageUrl = _this.find('div.gallery-icon a').attr('href'),
					imagePopup = '<div id="imagePopup" class="common-popup"><div class="common-popup-inner"></div></div>',
					closeButton = '<a class="close-button" href="javascript:history.go(-1)" title="Stäng">&#10060;</a>';

				_this.find('div.gallery-icon a').on('click', function(ev){
					ev.preventDefault();
					showProgress(); 
					window.location.hash = 'imagepopup';

					if($('#imagePopup').length < 1)
						$('body').append(imagePopup);

					$('#imagePopup .common-popup-inner').empty();

					var popupContent = _this.clone();

					// Tidy some things
					popupContent.find('div.gallery-icon img').attr('src', imageUrl).attr('style', '');

					$('#imagePopup .common-popup-inner').append(popupContent).append(closeButton);
					$('#imagePopup').fadeIn();

					$('#imagePopup figure div.gallery-icon img').on('load', function(){
						hideProgress();
					});
				});
			});
		};
		var gallery_images = $('.gallery figure');
		if(gallery_images.length)
			gallery_images.imageZoom();


		// BLOCKY CHECKBOXES
		$('body').on('change', '.blocky-checkboxes input[type="checkbox"], .blocky-checkboxes input[type="radio"]', function(){
			var $this = $(this),
				$checkboxes = $this.closest('.blocky-checkboxes').find('input[type="checkbox"], input[type="radio"]'),
				$active = $checkboxes.filter(':checked');

			$checkboxes.each(function(){
				$(this).closest('label').removeClass('active');
			});
			$active.each(function(){
				$(this).closest('label').addClass('active');
			})
		});

	}); /* END document ready */
})(jQuery);