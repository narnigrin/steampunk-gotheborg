/* js for members list */

(function($){
	function errorOutput(attempted, message){
		alert("Ett fel uppstod och " + attempted + ". Felmeddelandet som returnerades var: \n----------\n\n" + message + " \n\n----------\nOm problemet kvarstår, kontakta webbmastern.");
	}

	$(document).ready(function(){
		// Auto-show the "tip bubble" if user hasn't been here before
		$('#memberlist-tips-bubble').not('.seenalready').delay(300).fadeIn('slow');

		// Close "tips bubble"
		$('.js-close-bubble').click(function(ev){
			ev.preventDefault();
			$('#memberlist-tips-bubble').fadeOut(function(){
				$('#memberlist-tips-bubble a.js-close-bubble').text('Okej, NU fattar jag (kanske)');
			});
		});
		$('#memberlist-tips a.symb').click(function(ev){
			ev.preventDefault();
			$('#memberlist-tips-bubble').fadeToggle();
		});


		// #########################################
		// Magic to turn table fields into "magic inputs/magic selects" on click
		// #########################################
		$(document).on('click', 'div.editable', function(){
			var _self = $(this),
				text_before = _self.text().replace('"', '\"'),
				field_id = _self.data('field-id'),
				input_format;

			// Privileges should not be editable on inactive members
			if (field_id == 'privileges' && text_before == 'Inaktiv')
				return;

			if(field_id == 'comments')
				input_format = '<textarea class="magic-field" name="FNAME" id="FNAME" value="FVAL" style="height:' + _self.outerHeight() + 'px">FVAL</textarea>'; // The value attribute isn't proper HTML, but simplifies the JS
			else
				input_format = '<input type="text" class="magic-field" name="FNAME" value="FVAL" id="FNAME">';

			var input = (input_format.replace(/FNAME/g, field_id)).replace(/FVAL/g, text_before);
			_self.replaceWith(input);

			$('#' + field_id).focus();
		});
		$(document).on('click', 'div.magic-select', function(){
			var _self = $(this),
				value_before = _self.data('value'),
				title_before = _self.text(),
				field_id = _self.data('field-id'),
				options = _self.data('options-json');

			// Privileges should not be editable on inactive members
			if (field_id == 'privileges' && title_before == 'Inaktiv')
				return;

			var select = '<select id="'+field_id+'" class="magic-field" data-preselected="'+value_before+'" data-pretitle="'+title_before+'" data-options-json=\''+JSON.stringify(options)+'\'>OPTIONS</select>',
				options_html = '';

			$.each(options, function(key, title){
				options_html += '<option value="' + key + '"';
				if(key == value_before){
					options_html += ' selected="selected"';
				}
				options_html += '>' + title + '</option>';
			});

			select = select.replace('OPTIONS', options_html);
			_self.replaceWith(select);
			$('#role').focus();
		});


		// #######################################
		// Magic to turn "magic inputs" back into divs and save the data
		// #######################################
		function saveMemberField(fieldname, newvalue, oldvalue, mid, $self, div, selString){
			$self = $($self);
			var $parent = $self.closest('td');

			$.ajax({
				url: window.wptoolkit.ajaxUrl,
				type: 'POST',
				data: {
					action: 'AjaxAPI',
					command_class: 'MembersAjax',
					command: 'ajaxMemberSaveField',
					memberID: mid,
					field: fieldname,
					value: newvalue,
					old_value: oldvalue,
				},
				success: function(response){
					//console.log(response);

					if(response.status == true && response.data){
						$self.replaceWith(div);
						$parent.find(selString).addClass('success');

						if(response.data.rowclass)
							$parent.closest('tr').removeClass('not-paid').addClass(response.data.rowclass);

						if(response.data.newlevel && response.data.newlevel != null){
							var $level = $parent.closest('tr').find("[data-field-id='privileges']"),
								json = $level.data('options-json');

							$level.data('value', response.data.newlevel).text(json[response.data.newlevel]);
						}

						setTimeout(function(){
							$parent.find(selString).removeClass('success');
						}, 400);
					}
					else {
						errorOutput('fältet kunde inte sparas', response.message);
						
						$self.replaceWith(div);
						$parent.find(selString).addClass('error');
					}
				},
				error: function(jqXHR, status, error){
					//console.log(status, error, jqXHR);
					errorOutput('fältet kunde inte sparas', status + "\n" + error);
						
					$self.replaceWith(div);
					$parent.find(selString).addClass('error');
				},
			});
		}

		$(document).on('keyup', '.magic-field', function(ev){
			if(ev.which == 13){ // Enter
				$(this).blur();
			}
			else if(ev.which == 27){ // Esc
				var _self = $(this);

				if(_self.is('input') || _self.is('textarea')){
					var orig_value = _self[0].getAttribute('value'),
						field_name = _self.attr('name');

					var div_format = '<div class="editable ' + (orig_value == '' ? 'empty' : '') + '" data-field-id="FNAME">FVAL</div>';
					var elt = (div_format.replace(/FNAME/g, field_name)).replace(/FVAL/g, orig_value);
				}
				else { // It's a select
					var orig_value = _self.data('preselected'),
						options = _self.data('options-json'),
						orig_title = _self.data('pretitle');
					var elt = '<div class="magic-select" data-field-id="'+_self.attr('id')+'" data-options-json=\''+options+'\' data-value="'+orig_value+'">' + orig_title + '</div>';
				}

				_self.replaceWith(elt);
			}
		});
		$(document).on('blur', 'input.magic-field, textarea.magic-field', function(){
			var _self = $(this),
				field_value = _self.val(),
				field_name = _self.attr('name'),
				$parent = _self.closest('td');

			var div_format = '<div class="editable ' + (field_value == '' ? 'empty' : '') + '" data-field-id="FNAME">FVAL</div>';
			var div = (div_format.replace(/FNAME/g, field_name)).replace(/FVAL/g, field_value);

			// ##### Attempt to save data #####
			saveMemberField(field_name, field_value, _self[0].getAttribute('value'), _self.closest('tr').data('member-id'), _self, div, 'div.editable[data-field-id='+field_name+']');
		});

		$(document).on('change', 'select.magic-field', function(){
			$(this).trigger('blur');
		}).on('blur', 'select.magic-field', function(){
			var _self = $(this),
				value = _self.val(),
				value_title = _self.find('option[value=' + value + ']').text(),
				options = _self.data('options-json'),
				$parent = _self.closest('td');

			var div = '<div class="magic-select" data-field-id="'+_self.attr('id')+'" data-options-json=\''+JSON.stringify(options)+'\' data-value="'+value+'">' + value_title + '</div>';

			// Attempt to save data
			saveMemberField(_self.attr('id'), value, _self.data('preselected'), _self.closest('tr').data('member-id'), _self, div, 'div.magic-select');
		});
		

		// Saving of admin checkboxes immediately on change
		$('.magic-admin-checkbox').on('change', function(){
			var _self = $(this),
				rowId = _self.closest('tr').data('member-id'),
				newValue = _self.prop('checked');

			$.ajax({
				url: window.wptoolkit.ajaxUrl,
				type: 'POST',
				data: {
					action: 'AjaxAPI',
					command_class: 'MembersAjax',
					command: 'ajaxSetAdmin',
					mid: rowId,
					admin: newValue
				},
				success: function(response){
					//console.log(response);
					if(response.status == true && response.data){
						// We're fine
					}
					else {
						errorOutput('fältet kunde inte sparas', response.message);
						_self.prop('checked', !newValue);
					}
				},
				error: function(jqXHR, status, error){
					//console.log(jqXHR);
					errorOutput('fältet kunde inte sparas', status + "\n" + error);
					_self.prop('checked', !newValue);
				},
			});
		});


		// Add member form
		$('#addmember').on('submit', function(ev){
			ev.preventDefault();
			var _form = $(this);

			// Attempt to save
			$.ajax({
				url: window.wptoolkit.ajaxUrl,
				type: 'POST',
				data: {
					action: 'AjaxAPI',
					command_class: 'MembersAjax',
					command: 'ajaxNewMember',
					fields: $(this).serializeArray(),
				},
				success: function(response){
					if(response.status == true && response.data){
						//console.log(response.data);
						_form[0].reset();

						if(response.data.row && response.data.mid){
							$('#members-table tbody').append(response.data.row);
							//location.hash = "#member-row-" + response.data.mid;
							
							var $row = $('#members-table tr#member-row-' + response.data.mid + '');
							$row.addClass('new-flash');
							setTimeout(function(){
								$row.removeClass('new-flash');
							}, 700);
						}
					}
					else {
						errorOutput('medlemmen kunde inte skapas', response.message);
					}
				},
				error: function(jqXHR, status, error){
					errorOutput('medlemmen kunde inte skapas', status + "\n" + error);
				},
			});
		});


		// #####################
		// User signup functions
		// #####################

		// Helper function for the signup forms (submit events below)
		function registerMemberAccountAjax(data){
			$response = $('#response');

			$.ajax({
				url: window.wptoolkit.ajaxUrl,
				type: 'POST',
				data: $.extend({
					action: 'AjaxAPI',
					command_class: 'MembersAjax',
					command: 'ajaxUserSignup'
				}, data),
				success: function(response){
					//console.log(response);

					if(response.status == true && response.data){
						if(response.data.info){
							$response.fadeOut(function(){
								$(this).html('<span class="symb">&#9873;</span>' + response.data.info).removeClass('error').addClass('info').fadeIn();
							});
						}
						else {
							$response.fadeOut(function(){
								$(this).html('<span class="symb">&#10003;</span><p>Användaren har skapats. Ett mail med lösenordet har skickats till din e-postadress. I mailet finns instruktioner för hur du loggar in. (Om du inte hittar mailet, titta i skräpposten!)</p><p><strong>Välkommen!</strong></p>').removeClass('error info').fadeIn();
								$('#register-form')[0].reset();
							});
						}
					}
					else {
						$response.fadeOut(function(){
							$(this).html('<span class="symb">&#10060;</span>' + response.message).removeClass('info').addClass('error').fadeIn();
						});
					}
				},
				error: function(jqXHR, status, error){
					//console.log(jqXHR.responseText);
					$response.fadeOut(function(){
						$(this).html('').show();
					});
					errorOutput('kontot kunde inte registreras', status + "\n" + error);
				},
			});
		}

		// "Step one" (generic step) of register member account form
		$('#register-form').on('submit', function(ev){
			ev.preventDefault();
			var _form = $(this);

			var data = {
				email: _form.find('#email').val(),
			};

			registerMemberAccountAjax(data);
		});

		// "Step two" (choose displayname) of register member account form
		$(document).on('submit', '#register-step-2', function(ev){
			ev.preventDefault();
			var _form = $('#register-form');
				_form2 = $(this);

			var data = {
				email: _form.find('#email').val(),
				memberID: _form2.find('#suggestions').val(),
			};

			registerMemberAccountAjax(data);
		});

		// Reset password & hide #response buttons actions
		$(document).on('click', '#pwreset', function(){
			window.location = '/wp-login.php?action=lostpassword'; // Only works live
		}).on('click', '#resetform', function(){
			$('#response').fadeOut(function(){
				$(this).html('').show();
				$('#register-form')[0].reset();
			});
		});
	});
})(jQuery);