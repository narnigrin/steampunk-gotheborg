# Web site for Steampunk Götheborg.

Runs on Wordpress 4.*.     
_Last tested on Wordpress 4.6.1._

URL: http://www.steampunkgbg.se 

## Required plugins:

* [custom-taxonomy-order-ne](http://wordpress.org/plugins/custom-taxonomy-order-ne)
* [if-menu](http://wordpress.org/plugins/if-menu)
* [taxonomy-terms-order](http://wordpress.org/plugins/taxonomy-terms-order)

You should compile `public_html/wp-content/themes/steampunkgbg/css/less/style.less` to `public_html/wp-content/themes/steampunkgbg/style.css`.

### Other requirements:

* The environment must have the PEAR packages **HTTP_Request2** and **Services_Mailman** installed in order for the Mailman integration to work.
  (The Mailman plugin tries to check for the presence of these and will just refuse to work if they aren't present.)

----------

### Note:

A pretty common occurrence in the past is that the Google calendars stop working.
If you investigate the problem, and you eventually run into error messages referencing `file_get_contents` or `fopen`,
it's likely that someone has accidentally turned off `allow_url_fopen` on the server.
To turn it back on, log into CPanel, find "Select PHP version", pick an option that isn't `Native (X.y)`
(it could be the same numeric version just not "native"),
and make sure `allow_url_fopen` (under PHP Options) is set to **On.**

This seems to happen every time Manufrog replaces a server, because for some daft reason they _always_ auto-switch PHP to `native`.
